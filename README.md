# blokbot

Blok game playing robot (robot not yet implemented)

![Blok Game](movies/full_blok_game.mov)

<!-- <img src="movies/full_blok_game.mov"  width="600" > -->

## Instructions

1. Download anaconda from `anaconda.org`

1. Install `blokbot` python environment from `environment.yml`:

```shell
conda env create -f environment.yml
```

If you have already installed the `blokbot` environment, and need to update, run

```shell
conda env update -f environment.yml
```

1. Activate the new `blokbot` environment

```shell
conda activate blokbot
```

1. Run the game `code/main.py` with four humans playing against each other:

```shell
python code/main.py
```

1. Run the duo game `code/main.py` with two humans playing against each other:

```shell
python code/main.py --duo
```

1. Run the game `code/main.py` with two humans playing Players 1 and 3, and Random Bot playing Players 2 and 4:

```shell
python code/main.py -p2 random -p4 random
```

1. Run the game `code/main.py` with a human playing Player 1, Big Bot playing Player 2, Snug Bot playing Player 3, and Block Bot playing Player 4:

```shell
python code/main.py -p2 big -p3 snug -p4 block
```

1. Run the game `code/main.py` with four randoms playing each other:

```shell
python code/main.py -p1 random -p2 random -p3 random -p4 random
```

1. Run robots playing against each other `code/run_many_bloks.py`:

```shell
python run_many_bloks.py 10 -p1 random -p2 block -p3 big -p4 snug
```
