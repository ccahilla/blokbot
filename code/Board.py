"""Board.py

Board class definition.
"""

import copy
import os
import time

# import deepdish
import h5py
import numpy as np

import blok_library
from BigBotPlayer import BigBotPlayer
from BlockBotPlayer import BlockBotPlayer
from LeakBotPlayer import LeakBotPlayer
from MaxMoveBotPlayer import MaxMoveBotPlayer
from Player import Player
from RandomBotPlayer import RandomBotPlayer
from SmolBotPlayer import SmolBotPlayer
from SnugBotPlayer import SnugBotPlayer


class Board:
    """Blokus board class.
    Holds the information about the board being drawn in public variables.

    Inputs:
    -------
    size: int
        number of squares on the blokus board
    player_number: int
        number of players in the game
    left: float
        window edge
    right: float
        window edge
    down: float
        window edge
    up: float
        window edge
    """

    def __init__(
        self,
        player_number,
        player1,
        player2,
        player3,
        player4,
        size,
        left,
        right,
        down,
        up,
        window_size,
        font,
    ):
        """Constructor for class Board"""
        self.player_number = player_number
        self.size = size
        self.left = left
        self.right = right
        self.down = down
        self.up = up
        self.window_size = window_size
        self.font = font

        ortho_height = self.up - self.down
        # ortho_width = self.right - self.left

        self.board_width = 0.95 * ortho_height
        self.square_size = self.board_width / self.size

        # location of the board lines
        self.vlines = np.array([])
        self.hlines = np.array([])

        # initialize board data
        self.board_data = np.zeros(shape=(self.size, self.size), dtype=int)
        self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)

        # initialize colors
        color_index = 0
        self.colors = np.array(
            [
                "#000000",
                "#1f77b4",
                "#ffe12b",
                "#d62728",
                "#2ca02c",
                "#bfbfbf",
                "#9400d3",
                "#f88379",
                "#17becf",
                "#f97306",
                "#a63300",
                "#8c564b",
            ]
        )
        # hold index of colors used
        self.used_colors = []

        # Define players
        self.players = []
        self.player_index = 0
        self.player_types = np.array([player1, player2, player3, player4])

        for ii in range(self.player_number):
            order = ii + 1
            color_index += 1

            name = f"Player {order}"
            color = self.colors[color_index]

            if self.player_types[ii] == "random":
                temp_player = RandomBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "big":
                temp_player = BigBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "smol":
                temp_player = SmolBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "snug":
                temp_player = SnugBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "block":
                temp_player = BlockBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "leak":
                temp_player = LeakBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "maxmove":
                temp_player = MaxMoveBotPlayer(order, name, color, color_index, size)
            else:
                temp_player = Player(order, name, color, color_index, size)

            self.players.append(temp_player)

            self.used_colors.append(color_index)

        # cycle through all players, setting their first legal square
        for ii in range(self.player_number):
            player = self.get_current_player()
            # set starting "corner" depending on number of players
            if self.player_number == 2:
                if player.order == 1:
                    x_required = 4
                    y_required = 4
                elif player.order == 2:
                    x_required = 9
                    y_required = 9

            if self.player_number == 4:
                if player.order == 1:
                    x_required = 0
                    y_required = 0
                elif player.order == 2:
                    x_required = 0
                    y_required = 19
                elif player.order == 3:
                    x_required = 19
                    y_required = 19
                elif player.order == 4:
                    x_required = 19
                    y_required = 0

            # set the first legal square for movement, aka "corner"
            player.corner_board[x_required, y_required] = 1

            # populate potential_moves
            player.calculate_potential_moves(self.board_data)

            # move on to next player
            self.increment_player_index()

        # game history dict
        # keys will be the move number,
        # values will be the board_state and some (not all) player variables
        self.total_move_number = 0
        self.move_number = 0
        self.game_history = {}
        self.game_history["info"] = {}
        for player in self.players:
            self.game_history["info"][f"player{player.order}"] = {}
            self.game_history["info"][f"player{player.order}"]["name"] = player.name

        # winners, set by Board.determine_winners() at end of game
        self.winners = None
        self.are_winners_determined = False

        ## board flags
        # current player a robot?
        self.is_current_player_robot = None
        self.update_is_current_player_robot()  # update self.is_current_player_robot straight away

        # current move legality boolean
        self.is_move_legal = False

        # edge and corner help
        self.are_edges_visible = False
        self.are_corners_visible = True

        # score shown
        self.is_score_shown = True

        # show potential moves
        self.is_potential_move_shown = False

        # automatically toggles help after everyone's first move
        self.auto_help_toggle = False

        # help message toggle
        self.is_help_shown = True

        # game over bool
        self.game_over = False

        # game over message bool
        self.game_over_message = False

        # game reviewing bool
        self.is_game_review_started = False
        self.is_game_reviewing = False

        # game saved bool
        self.is_game_saved = False


        return

    def update_window_size(self, left, right, down, up, window_size):
        """If window gets resized, update the self.window_size variable,
        and other associated variables
        """
        self.left = left
        self.right = right
        self.down = down
        self.up = up
        self.window_size = window_size

        ortho_height = self.up - self.down
        # ortho_width = self.right - self.left

        self.board_width = 0.95 * ortho_height
        self.square_size = self.board_width / self.size

        return

    def toggle_edge_help(self):
        """Toggles the edge viewer on and off"""
        self.are_edges_visible = not self.are_edges_visible
        return

    def toggle_corner_help(self):
        """Toggles the corner viewer on and off"""
        self.are_corners_visible = not self.are_corners_visible
        return

    def toggle_score(self):
        """Toggles the score viewer on and off"""
        self.is_score_shown = not self.is_score_shown
        return

    def toggle_potential_move_shown(self):
        """Toggles the potential move viewer on and off"""
        self.is_potential_move_shown = not self.is_potential_move_shown
        return

    def toggle_help(self):
        """Toggles the help message on and off"""
        self.is_help_shown = not self.is_help_shown
        return

    def toggle_game_review(self):
        """Toggles whether the game is being reviewed after game over"""
        self.is_game_reviewing = not self.is_game_reviewing
        return

    def increment_potential_move(self):
        """changes whose turn it is by incrementing potential_move"""
        player = self.get_current_player()
        player.potential_move_index += 1
        if player.potential_move_index not in player.potential_moves:
            player.potential_move_index = 1

        return

    def decrement_potential_move(self):
        """changes whose turn it is by decrement potential_move"""
        player = self.get_current_player()
        player.potential_move_index -= 1
        if player.potential_move_index not in player.potential_moves:
            player.potential_move_index = len(player.potential_moves)

        return

    def increment_player_index(self):
        """changes whose turn it is by incrementing player_index"""
        self.player_index += 1
        if self.player_index == self.player_number:
            self.player_index = 0

        return

    # def increment_color_index(self):
    #     """changes the color of the mouse clicks
    #     """
    #     self.color_index += 1
    #     if self.color_index > len(self.colors) - 1:
    #         self.color_index = 1

    #     return

    def increment_player_color(self):
        """increments current player's color to next unused color,
        and also changes the board data
        """
        player = self.get_current_player()
        color_index = player.color_index

        while color_index in self.used_colors:
            color_index += 1
            if color_index > len(self.colors) - 1:
                color_index = 1

        # next unused color found, pop the old color off the list of used colors
        self.used_colors.remove(player.color_index)

        # change piece colors
        for piece in player.pieces:
            piece.color = self.colors[color_index]

        # change the board data from old index to new index
        self.board_data[self.board_data == player.color_index] = color_index

        # change the player color
        player.color_index = color_index
        player.color = self.colors[color_index]

        # add new color to the list of used_colors
        self.used_colors.append(color_index)

        return

    def increment_piece_index(self):
        """change the piece the current player is holding, by adding one to that index"""
        player = self.get_current_player()
        player.increment_piece_index()
        return

    def decrement_piece_index(self):
        """change the piece the current player is holding, by subtracting one from that index"""
        player = self.get_current_player()
        player.decrement_piece_index()
        return

    def get_ortho_coordinates(self, window_x, window_y):
        """Transfers window (x, y) coordinates to ortho game (x, y) coordinates,
        Ortho coords have (0, 0) in the center.
        """
        # First, calculate the "normalized" mouse coordinates by dividing by window_size
        mouse_norm_x = window_x / self.window_size[0]
        mouse_norm_y = window_y / self.window_size[1]

        # Map those coordinates to your orthographic projection range
        ortho_x = (mouse_norm_x * (self.right - self.left)) + self.left
        ortho_y = (mouse_norm_y * (self.down - self.up)) + self.up

        return ortho_x, ortho_y

    def get_window_coordinates(self, ortho_x, ortho_y):
        """Transfers ortho game (x, y) coordinates to window (x, y) coordinates.
        Window coords have (0, 0) in the bottom left.
        """
        window_x = (
            self.window_size[0] * (ortho_x - self.left) / (self.right - self.left)
        )
        window_y = self.window_size[1] * (ortho_y - self.up) / (self.down - self.up)

        return window_x, window_y

    def get_square_mouse_over(self, mouse_pos):
        """gets the square the mouse is currently over.
        If over a square, returns indicies of that square (ii, jj)
        If not over a square, returns (None, None)
        """
        # First, calculate the "normalized" mouse coordinates by dividing by window_size
        mouse_norm_x = mouse_pos[0] / self.window_size[0]
        mouse_norm_y = mouse_pos[1] / self.window_size[1]

        # Map those coordinates to your orthographic projection range
        ortho_x = (mouse_norm_x * (self.right - self.left)) + self.left
        ortho_y = (mouse_norm_y * (self.down - self.up)) + self.up

        # Check if x- and y-coordinates are within some square's range
        self.x_index = None
        for ii, vline in enumerate(self.vlines):
            if vline < ortho_x and self.vlines[-1] > ortho_x:
                if ii < self.size:
                    self.x_index = ii

        self.y_index = None
        for jj, hline in enumerate(self.hlines):
            if hline < ortho_y and self.hlines[-1] > ortho_y:
                if jj < self.size:
                    self.y_index = jj

        return

    def get_current_player(self):
        """Gets the current player object"""
        player = self.players[self.player_index]
        return player

    def get_current_player_color(self):
        """Gets the current piece object being held by the current player."""
        player = self.get_current_player()
        return player.color

    def get_current_player_piece(self):
        """Gets the current piece object being held by the current player."""
        player = self.get_current_player()
        piece = player.pieces[player.piece_index]
        return piece

    def rotate_piece(self):
        """Rotates piece currently held by player"""
        piece = self.get_current_player_piece()
        piece.rotate_piece()
        return

    def rotate_piece_counterclockwise(self):
        """Rotates piece currently held by player"""
        piece = self.get_current_player_piece()
        piece.rotate_piece_counterclockwise()
        return

    def flip_piece(self):
        """Flips piece currently held by player on x-axis"""
        piece = self.get_current_player_piece()
        piece.flip_piece()
        return

    def invert_piece(self):
        """Flips piece currently held by player on y-axis"""
        piece = self.get_current_player_piece()
        piece.invert_piece()
        return

    def update_is_current_player_robot(self):
        """Updates the is_current_player_robot flag"""
        player = self.get_current_player()
        self.is_current_player_robot = player.is_player_robot
        return

    def skip_turn(self):
        """If player pressed the 'p' button,
        skips their turn by incrementing player index.
        Calculates next player's possible moves as well,
        not doing so breaks the game.
        """
        self.increment_player_index()

        # calculate the next potential moves for the next player
        # have to do it after all others have played,
        # since some moves will become impossible after opponent moves
        next_player = self.get_current_player()
        if next_player.are_potential_moves_calculated:
            next_player.calculate_potential_moves(self.board_data)

        # update whether next_player is a robot
        self.update_is_current_player_robot()

        # check if the game is over
        self.check_each_player_finished()

        return

    def check_each_player_moved_once(self):
        """Checks if each player has moved at least once."""
        all_moved_once = True
        for player in self.players:
            if player.moves == 0:
                all_moved_once = False
                break
        return all_moved_once

    def check_each_player_finished(self):
        """Checks if each player has finished the game."""
        finished_players = 0
        for player in self.players:
            if player.finished:
                finished_players += 1

        if finished_players == len(self.players):
            self.game_over = True
        # if at least one player is totally done
        elif finished_players > 0:
            # check the number of moves available to each player
            # as it's possible that move ended the game for all
            for player in self.players:
                player.calculate_potential_moves(self.board_data)
                if len(player.potential_moves) == 0:
                    player.finished = True

            # now check again
            finished_players = 0
            for player in self.players:
                if player.finished:
                    finished_players += 1

            if finished_players == len(self.players):
                self.game_over = True

        return

    def highlight_hover(self):
        """highlights the square we're hovered over,
        zeros everything else

        Inputs:
        -------
        x_index: int
            x-coordinate of the square to highlight
        y_index: int
            y-coordinate of the square to highlight
        """
        self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)

        if self.x_index is not None and self.y_index is not None:

            player = self.get_current_player()
            piece = self.get_current_player_piece()
            temp_piece_coordinates = piece.piece_coordinates

            # populate whether move is legal
            self.is_move_legal = player.check_move_legality(
                self.x_index, self.y_index, piece, self.board_data
            )

            # populate the hover_board
            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + self.x_index
                temp_y = coordinate[1] + self.y_index
                if (
                    0 <= temp_x
                    and temp_x < self.size
                    and 0 <= temp_y
                    and temp_y < self.size
                ):
                    self.hover_board[temp_x, temp_y] = 1
        return

    def update_all_players_boards(self, old_board_data, new_board_data):
        """Updates all players corner and edge boards based on the latest piece placement,
        in a move-agnostic way."""

        # compare current board and old board
        move_board = np.subtract(new_board_data, old_board_data)

        # get all nonzero coordinates and cycle through them
        xs, ys = move_board.nonzero()

        # remove squares from edge and corner boards
        for temp_x, temp_y in zip(xs, ys):
            for temp_player in self.players:
                temp_player.edge_board[temp_x, temp_y] = 0
                temp_player.corner_board[temp_x, temp_y] = 0

        return

    def save_last_move(self):
        """Saves the latest move and the piece played"""

        scores = np.array([], dtype=int)
        for player in self.players:
            scores = np.append(scores, player.score)

        player = self.get_current_player()

        self.total_move_number += 1
        self.move_number += 1
        move_str = f"move{self.move_number}"

        self.game_history[move_str] = {}
        self.game_history[move_str]["board"] = copy.deepcopy(self.board_data)
        self.game_history[move_str]["player_order"] = player.order
        self.game_history[move_str]["piece_id"] = player.last_played_piece_id
        self.game_history[move_str]["scores"] = scores
        self.game_history[move_str]["number_potential_moves"] = len(
            player.potential_moves
        )

        return

    def make_move(self):
        """Places a piece of the current player's color, if possible.
        Also repopulates the edge_board and corner_board

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        player = self.get_current_player()
        if not self.is_current_player_robot:
            piece = self.get_current_player_piece()

            # populate whether move is legal
            self.is_move_legal = player.check_move_legality(
                self.x_index, self.y_index, piece, self.board_data
            )

        # if the move is legal, or player is robot,
        # then place the piece, and take it out of the running
        if self.is_move_legal or self.is_current_player_robot:

            # save the old board briefly
            old_board_data = copy.deepcopy(self.board_data)

            # make the move, human or robot
            if not self.is_current_player_robot:
                piece = self.get_current_player_piece()
                self.board_data = player.make_move(
                    self.x_index, self.y_index, piece, self.board_data
                )
            else:
                self.board_data = player.make_robot_move(self.board_data, self.players)

            # update all other players edge and corner boards
            self.update_all_players_boards(old_board_data, self.board_data)

            # if a new move has been made, save the move history
            if not np.array_equal(old_board_data, self.board_data):
                self.save_last_move()

            # increment to the next player
            self.increment_player_index()

            # calculate the next potential moves for the next player
            # have to do it after all others have played,
            # since some moves will become impossible after opponent moves
            next_player = self.get_current_player()
            if next_player.are_potential_moves_calculated:
                next_player.calculate_potential_moves(self.board_data)

            # update whether next_player is a robot
            self.update_is_current_player_robot()

            # toggle help off automatically after player's have moved once
            if not self.auto_help_toggle:
                all_moved_once = self.check_each_player_moved_once()
                if all_moved_once:
                    self.auto_help_toggle = True
                    self.is_help_shown = False

            # check if the game is over
            self.check_each_player_finished()

        return

    def print_board(self, board):
        """Prints a board to terminal as it appears on screen"""
        xs = np.arange(0, self.size)
        ys = np.flip(np.arange(0, self.size))

        for yy in ys:
            for xx in xs:
                print(board[xx, yy], end=" ")

            print()
        return

    def determine_winners(self):
        """Figures out who won the game based on score.
        Sets self.winners, a list of the winner(s), in case of a tie.
        """
        best_score = 0
        winners = []
        for player in self.players:
            if best_score < player.score:
                best_score = player.score
                winners = [player.name]
            elif best_score == player.score:
                winners.append(player.name)

        self.winners = winners
        self.are_winners_determined = True

        return

    def save_dict_to_h5(self, filename, dictionary):
        with h5py.File(filename, 'w') as f:
            # Recursively save dictionary to HDF5 file
            def recursively_save_dict(name, obj):
                if isinstance(obj, dict):
                    # Create a group for the dictionary
                    if not name == "/":
                        group = f.create_group(name)

                    for key, value in obj.items():
                        recursively_save_dict(f"{name}/{key}", value)
                else:
                    # Create a dataset for the value (e.g., int, float, etc.)
                    f.create_dataset(name, data=obj)
            
            # Start saving from the root of the dictionary
            recursively_save_dict("/main", dictionary)

        print(f"Saved {filename}")
        return

    def save_game(self):
        """Save the game history and score."""

        # save some final info
        self.game_history["info"]["total_moves"] = self.total_move_number

        for player in self.players:
            self.game_history["info"][f"player{player.order}"][
                "final_score"
            ] = player.score

        self.game_history["info"]["winners"] = self.winners

        # Find the save_dir, in this case the <git_repo>/saved_games/ dir
        code_dir = os.path.dirname(os.path.abspath(__file__))
        save_dir = os.path.join(code_dir, "../saved_games/")
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        # save the game .h5 filename with the timestamp
        date_time_str = time.strftime("%Y_%m_%d_%H%M%S")
        h5_filename = f"game_history_{date_time_str}.h5"
        full_h5_filename = os.path.join(save_dir, h5_filename)

        # Save the game_history dictionary with h5py
        self.save_dict_to_h5(full_h5_filename, self.game_history)

        self.is_game_saved = True
        return

    def review_game(self):
        """If player hits spacebar after game over,
        this function will show a movie of the game automatically
        """
        # decide whether to show the next move
        show_next_move = False
        max_time = 0.4
        if hasattr(self, "timer"):
            time_elapsed = time.time() - self.timer
            if time_elapsed > max_time:
                show_next_move = True
        else:
            self.timer = time.time()

        # if enough time has elapsed to show the next move
        if show_next_move:
            delattr(self, "timer")
            self.move_number += 1
            if self.move_number > self.total_move_number:
                if not self.is_game_review_started:
                    self.move_number = 0
                    self.are_corners_visible = False
                    self.are_edges_visible = False
                    self.is_game_review_started = True

                else:
                    # we've reached the last move, so end the review playback
                    self.move_number = self.total_move_number
                    self.is_game_reviewing = False
                    self.is_game_review_started = False
                    return

            if self.move_number > 0:
                # update the board and set the piece to played
                move_str = f"move{self.move_number}"

                self.board_data = self.game_history[move_str]["board"]
                player_order = self.game_history[move_str]["player_order"]
                piece_id = self.game_history[move_str]["piece_id"]

                for player in self.players:
                    if player_order == player.order:
                        piece = player.pieces[piece_id]
                        break

                piece.set_piece_played()
                self.increment_player_index()

                scores = self.game_history[move_str]["scores"]
                for score, player in zip(scores, self.players):
                    player.score = score
                    

            else:
                # if no moves yet, set board empty
                self.board_data = np.zeros(shape=(self.size, self.size), dtype=int)
                # set all pieces to not played at first
                self.player_index = 0
                for player in self.players:
                    player.score = 0
                    player.finished = False
                    for piece in player.pieces:
                        piece.set_piece_not_played()

        return

    def draw_board(self):
        """Main drawing function, draws the board on the OpenGL screen"""

        # draw vertical lines
        self.vlines = np.array([])
        for ii in range(self.size + 1):
            x0 = ii * self.square_size - self.board_width / 2.0
            y1 = self.board_width / 2.0
            y2 = -self.board_width / 2.0

            point1 = (x0, y1, 0)
            point2 = (x0, y2, 0)
            blok_library.line(point1, point2)

            self.vlines = np.append(self.vlines, x0)

        # draw horizontal lines
        self.hlines = np.array([])
        for jj in range(self.size + 1):
            y0 = jj * self.square_size - self.board_width / 2.0
            x1 = self.board_width / 2.0
            x2 = -self.board_width / 2.0

            point1 = (x1, y0, 0)
            point2 = (x2, y0, 0)
            blok_library.line(point1, point2)

            self.hlines = np.append(self.hlines, y0)

        # calculate board center points
        self.board_center = np.zeros((self.size, self.size, 3))
        for ii in range(self.size):
            for jj in range(self.size):
                x0 = ii * self.square_size + (self.square_size - self.board_width) / 2.0
                y0 = jj * self.square_size + (self.square_size - self.board_width) / 2.0
                point = (x0, y0, 0)

                self.board_center[ii, jj] = point

        # color squares according to self.board_data
        player = self.get_current_player()
        player_color = player.color

        edge_board = player.edge_board
        corner_board = player.corner_board

        if self.is_move_legal:
            alpha = 0.7
        else:
            alpha = 0.4

        for ii in range(self.size):
            for jj in range(self.size):
                point = self.board_center[ii, jj]
                data = self.board_data[ii, jj]
                hover = self.hover_board[ii, jj]
                edge = edge_board[ii, jj]
                corner = corner_board[ii, jj]

                if hover == 1 and data == 0:
                    hex_color = player_color
                    rgb_color = blok_library.hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, alpha

                    blok_library.cquad(point, self.square_size, rgba_color)

                elif hover == 1 and data != 0:
                    hex_color_player = player_color
                    hex_color_board = self.colors[data]
                    rgb_color_player = blok_library.hex_to_rgb(hex_color_player)
                    rgb_color_board = blok_library.hex_to_rgb(hex_color_board)
                    rgba_color_player = *rgb_color_player, alpha
                    rgba_color_board = *rgb_color_board, 1.0

                    blok_library.dquad(
                        point,
                        self.square_size,
                        rgba_color_board,
                        rgba_color_player,
                        stripe_number=6,
                    )
                elif edge and self.are_edges_visible:
                    hex_color_player = player_color
                    hex_color_board = self.colors[data]
                    rgb_color_player = blok_library.hex_to_rgb(hex_color_player)
                    rgb_color_board = blok_library.hex_to_rgb(hex_color_board)
                    rgba_color_player = *rgb_color_player, alpha
                    rgba_color_board = *rgb_color_board, 1.0

                    blok_library.dquad(
                        point,
                        self.square_size,
                        rgba_color_board,
                        rgba_color_player,
                        stripe_number=8,
                    )
                elif corner and self.are_corners_visible:
                    hex_color = player_color
                    rgb_color = blok_library.hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, alpha

                    blok_library.cquad(point, 0.5 * self.square_size, rgba_color)

                elif data != 0:
                    hex_color = self.colors[data]
                    rgb_color = blok_library.hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, 1.0

                    blok_library.cquad(point, self.square_size, rgba_color)

        # draw each player's remaining pieces on the left hand side
        self.draw_remaining_pieces()

        # draw the help message, if toggled on
        if self.is_help_shown:
            self.draw_help_text()

        # draw the player scores, if toggled on
        if self.is_score_shown:
            self.draw_score_text()

        # draw potential move, if toggled on
        if self.is_potential_move_shown and player.are_potential_moves_calculated:
            self.draw_potential_move()

        # draw game over message if game is over
        if self.game_over and not self.is_game_reviewing:
            self.draw_game_over_message()
            self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)

        return

    def draw_remaining_pieces(self):
        """Draws each player's remaining pieces for self.draw_board()"""

        small_square_size = self.board_width / (20 * 5)
        x_center = -self.board_width / 2.0
        y_center = -self.board_width / 2.0
        x_scaler = 6.0

        for ii, player in enumerate(self.players):
            temp_x = x_center - (ii + 1) * x_scaler * small_square_size
            player.draw_remaining_pieces(
                temp_x, y_center, small_square_size, self.player_index
            )
        return

    def draw_score_text(self):
        """Draws each player's score on the bottom right of the screen for self.draw_board()."""
        board_edge = self.board_width / 2.0
        x_score_window, y_score_window = board_edge + 0.2, 3.5

        score_intro = "Score"
        x_score, y_score = self.get_window_coordinates(x_score_window, y_score_window)
        blok_library.draw_text(x_score, y_score, score_intro, self.font)

        text_height = 0.5

        for ii, player in enumerate(self.players):
            index = ii + 1

            hex_color = player.color
            text_color_rgb_255 = blok_library.hex_to_rgb_255(hex_color)
            text_color_rgba_255 = *text_color_rgb_255, 255

            name = player.name
            score = player.score
            score_text = f"{name} : {score}"
            if player.finished:
                score_text += " (no moves)"
            if player.order == self.player_index + 1:
                score_text += " <--"

            temp_x_score = x_score_window
            temp_y_score = y_score_window + index * text_height

            x_score, y_score = self.get_window_coordinates(temp_x_score, temp_y_score)

            blok_library.draw_text(
                x_score, y_score, score_text, self.font, text_color=text_color_rgba_255
            )

        return

    def draw_help_text(self):
        """Draws the help text if called for self.draw_board()"""
        board_edge = self.board_width / 2.0
        x_help_window, y_help_window = board_edge + 0.1, -board_edge + 0.2

        text_height = 0.4

        # intro text
        help_text_intro = f"Help text"
        help_text_intro_color = (255, 255, 0, 255)
        x_help, y_help = self.get_window_coordinates(x_help_window, y_help_window)
        blok_library.draw_text(
            x_help, y_help, help_text_intro, self.font, text_color=help_text_intro_color
        )

        # basic instructions
        help_text_basics = [
            f"Left click to place piece.",
            f"Pieces must touch corner",
            f"of similar colored piece",
            f"after first move, but",
            f"cannot touch the edge",
            f"of a similar colored piece.",
        ]
        for ii, help_text_basic in enumerate(help_text_basics):
            temp_x_help = x_help_window
            temp_y_help = y_help_window + (ii + 1) * text_height
            x_help, y_help = self.get_window_coordinates(temp_x_help, temp_y_help)
            blok_library.draw_text(x_help, y_help, help_text_basic, self.font)

        # commands
        help_text_commands = [
            "r: rotates piece",
            "e: rotates piece",
            "f: flips piece left-right",
            "g: flips piece up-down",
            "up: change piece",
            "down: change piece",
            "p: skips turn",
            "l: change color",
            "x: toggle edge viewer",
            "c: toggle corner viewer",
            "o: toggle score on/off",
            "b: toggle potential move",
            "n: change potential move",
            "m: change potential move",
            "h: toggle help text",
            "esc: quits game",
        ]
        for ii, help_text_command in enumerate(help_text_commands):
            temp_x_help = x_help_window
            temp_y_help = y_help_window + (ii + len(help_text_basics) + 2) * text_height
            x_help, y_help = self.get_window_coordinates(temp_x_help, temp_y_help)
            blok_library.draw_text(x_help, y_help, help_text_command, self.font)

        return

    def draw_potential_move(self):
        """Draws a potential move, based on player.potential_move_index"""
        player = self.get_current_player()
        potential_move_number = len(player.potential_moves)
        move_text2 = ""
        if potential_move_number > 0:
            if player.potential_move_index > potential_move_number:
                player.potential_move_index = potential_move_number

            move_dict = player.potential_moves[player.potential_move_index]

            # compare current board and potential move board
            potential_move_board = np.subtract(move_dict["board"], self.board_data)

            # get all nonzero coordinates and cycle through them
            xs, ys = potential_move_board.nonzero()

            player_color = self.get_current_player_color()
            alpha = 0.3
            for temp_x, temp_y in zip(xs, ys):
                point = self.board_center[temp_x, temp_y]

                hex_color = player_color
                rgb_color = blok_library.hex_to_rgb(hex_color)
                rgba_color = *rgb_color, alpha

                blok_library.cquad(point, self.square_size, rgba_color)

            move_text = (
                f"Potential move {player.potential_move_index}/{potential_move_number}"
            )
            move_text_color = (255, 255, 255, 255)
        else:
            move_text = f"No moves remaining"
            move_text2 = f"Press 'p' to skip turn"
            move_text_color = (255, 255, 0, 255)

        # Draw text to screen
        board_edge = self.board_width / 2.0
        x_move_window, y_move_window = board_edge + 0.2, 6.0

        x_move, y_move = self.get_window_coordinates(x_move_window, y_move_window)
        blok_library.draw_text(
            x_move, y_move, move_text, self.font, text_color=move_text_color
        )

        if move_text2:
            text_height = 0.4
            x_move, y_move = self.get_window_coordinates(
                x_move_window, y_move_window + text_height
            )
            blok_library.draw_text(
                x_move, y_move, move_text2, self.font, text_color=move_text_color
            )

        return

    def draw_game_over_message(self):
        """Draws the game over message if the game is over.
        Also turns off the main help message to free up space on the right.
        """
        self.is_help_shown = False

        text_height = 0.4
        board_edge = self.board_width / 2.0
        x_game_over_window, y_game_over_window = board_edge + 0.2, -2.0
        game_over_text_color = (186, 219, 255, 255)

        # Figure out who won
        # TODO: Don't recalculate this all the time
        max_score = 0
        all_pieces_played = False
        for ii, player in enumerate(self.players):
            score = player.score

            if score > max_score:
                max_score = score

                name = player.name
                hex_color = player.color
                text_color_rgb_255 = blok_library.hex_to_rgb_255(hex_color)
                winner_text_color_rgba_255 = *text_color_rgb_255, 255 

                if score == 89:
                    all_pieces_played = True

        temp_winner_text = f"{name} wins!"
        winner_texts = [
            "*" * len(temp_winner_text),
            temp_winner_text,
            "*" * len(temp_winner_text),
        ]
        if all_pieces_played:
            winner_texts.insert(2, "All pieces were played! Congrats!")

        game_over_texts = [
            f"Game over",
            f"Hit spacebar to play",
            f"a game review movie,",
            f"spacebar again to pause",
            f"Review: move {self.move_number}/{self.total_move_number}",
        ]

        for ii, winner_text in enumerate(winner_texts):
            temp_x_winner = x_game_over_window
            temp_y_winner = y_game_over_window + (ii + 1) * text_height
            x_winner, y_winner = self.get_window_coordinates(
                temp_x_winner, temp_y_winner
            )
            blok_library.draw_text(
                x_winner,
                y_winner,
                winner_text,
                self.font,
                text_color=winner_text_color_rgba_255,
            )
        for ii, game_over_text in enumerate(game_over_texts):
            temp_x_game_over = x_game_over_window
            temp_y_game_over = y_game_over_window + (ii + 1 + len(winner_texts)) * text_height
            x_game_over, y_game_over = self.get_window_coordinates(
                temp_x_game_over, temp_y_game_over
            )
            blok_library.draw_text(
                x_game_over,
                y_game_over,
                game_over_text,
                self.font,
                text_color=game_over_text_color,
            )

        # self.game_over_message = True

        return
