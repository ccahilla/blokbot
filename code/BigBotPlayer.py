"""BigBotPlayer.py

Bot who always plays the biggest piece possible.
"""

import numpy as np

from BotPlayer import BotPlayer
from Piece import Piece


class BigBotPlayer(BotPlayer):
    """Bot who always plays the biggest piece possible"""

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Big Bot {self.order}"

        return

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Big Bot is just the inverse of the number of squares for a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = np.array([])
        for move_key, move_dict in self.potential_moves.items():
            temp_piece = Piece(move_dict["piece_id"], self.color)
            costs = np.append(costs, -temp_piece.squares)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        return move_key
