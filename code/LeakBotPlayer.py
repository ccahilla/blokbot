"""LeakBotPlayer.py

Bot who tries to minimize leaks.
Leaks are connected spaces that only your opponents may play in.
In short, it's just how connected your edge board is.

We will punish leaks by squaring the number of connected squares.
This will make LeakBot not want to play certain pieces,
unless they are on the edge of the board or touching other players.
"""

import numpy as np

from BotPlayer import BotPlayer
from Piece import Piece


class LeakBotPlayer(BotPlayer):
    """Bot who tries to minimize leaks.
    Leaks are connected spaces that only your opponents may play in.
    In short, it's just how connected your edge board is.

    We will punish leaks by squaring the number of connected squares.
    This will make LeakBot not want to play certain pieces,
    unless they are on the edge of the board or touching other players.
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Leak Bot {self.order}"

        self.square_scaler = -1.0  # reward playing big pieces
        self.leak_scaler = 0.2  # punish leaks
        self.leak_exponential = 1.3  # punish leaks hard

        return

    def calculate_costs(self, board_data, players):
        """Leak Bot tries to minimize leaks while playing big pieces.

        Returns a numpy.array of the costs associated with each move.
        """
        # compute the costs for each move
        costs = np.array([])

        # cycle through every potential move
        for move_key, move_dict in self.potential_moves.items():
            # get the new move board
            new_board_data = move_dict["board"]

            # get the piece
            piece_id = move_dict["piece_id"]
            piece = Piece(piece_id, self.color)

            # make list of all ordered pairs (x, y) of board spaces belonging to this player
            new_board_spots = self.get_board_spots(new_board_data, self.order)

            # hold the edges and corners for this player
            new_edge_board = np.zeros(shape=(self.size, self.size), dtype=int)
            new_corner_board = np.zeros(shape=(self.size, self.size), dtype=int)

            # for each spot belonging to this player, make new edge and corner board
            for xx, yy in new_board_spots:

                # edges
                for temp_x, temp_y in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    edge_x = temp_x + xx
                    edge_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= edge_x
                        and edge_x < self.size
                        and 0 <= edge_y
                        and edge_y < self.size
                    ):
                        if new_board_data[edge_x, edge_y] == 0:  # if square is empty
                            new_edge_board[edge_x, edge_y] = 1
                            new_corner_board[edge_x, edge_y] = 0

                # corners
                for temp_x, temp_y in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                    corner_x = temp_x + xx
                    corner_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= corner_x
                        and corner_x < self.size
                        and 0 <= corner_y
                        and corner_y < self.size
                    ):
                        # if square is empty
                        if new_board_data[corner_x, corner_y] == 0:
                            # if square not an edge
                            if new_edge_board[corner_x, corner_y] == 0:
                                new_corner_board[corner_x, corner_y] = 1

            # analyze the edge board, counting squares adjacent to each other
            number_of_islands = 0
            island_sizes = np.array([], dtype=int)
            used_edge_spots = np.array([])

            # first, get the edge board spots (x, y) coordinates
            new_edge_board_spots = self.get_board_spots(new_edge_board, 1)

            for xx, yy in new_edge_board_spots:
                current_edge_spot = np.array([[xx, yy]], dtype=int)
                # if nothing in used_edge_spots
                if not used_edge_spots.size:
                    used_edge_spots = np.array([[xx, yy]], dtype=int)
                else:
                    # check if current edge spot (xx, yy) in used_edge_spots
                    current_edge_spot = np.array([[xx, yy]], dtype=int)
                    has_edge_spot_been_used = (
                        (current_edge_spot == used_edge_spots).all(axis=1).any()
                    )

                    # if it has been used, move on
                    if has_edge_spot_been_used:
                        continue
                    else:
                        used_edge_spots = np.vstack(
                            (used_edge_spots, current_edge_spot)
                        )

                # recursively explore an island
                number_of_islands += 1
                island_size = 1
                island_size, used_edge_spots = self.recursive_island_search(
                    island_size,
                    current_edge_spot,
                    used_edge_spots,
                    new_edge_board_spots,
                )
                island_sizes = np.append(island_sizes, island_size)

            # calculate cost for this move
            temp_cost = self.square_scaler * piece.squares + self.leak_scaler * np.sum(
                island_sizes**self.leak_exponential
            )

            # store the information to check later
            move_dict["piece_squares"] = piece.squares
            move_dict["number_of_islands"] = number_of_islands
            move_dict["island_sizes"] = island_sizes

            costs = np.append(costs, temp_cost)

        return costs

    def recursive_island_search(self, island_size, current_spot, used_spots, all_spots):
        """Recursively searches for islands of edges.
        We have a grid representing edges full of ones and zeros.
        Basically, we want to find how many "islands" of ones there are,
        i.e. how many distinct sets of ones there are,
        and how big they are, i.e. 4 contiguous squares.

        We will do this by investigating each square once,
        here called current_spot,
        and looking around it in four directions.
        If current_spot has another edge next to it in all_spots,
        and has not already been looked at in used_spots,
        then we will add one to the current island size,
        and call recursive_island_search() on that edge as well.
        This will continue until the entire island has been explored.
        """
        xx = current_spot[0][0]
        yy = current_spot[0][1]
        for temp_x, temp_y in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            new_x = temp_x + xx
            new_y = temp_y + yy

            temp_spot = np.array([[new_x, new_y]], dtype=int)
            # if temp_spot (x, y) is in list of edge spots
            is_edge_spot = (temp_spot == all_spots).all(axis=1).any()
            is_used_edge_spot = (temp_spot == used_spots).all(axis=1).any()

            # if we've found an edge spot, and it's not already used
            if is_edge_spot and not is_used_edge_spot:
                # Add temp_spot to used_spots
                used_spots = np.vstack((used_spots, temp_spot))
                # increment size
                island_size += 1
                # run again with temp_spot as current_spot
                island_size, used_spots = self.recursive_island_search(
                    island_size, temp_spot, used_spots, all_spots
                )

        return island_size, used_spots

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Smol Bot is just the number of squares for a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = self.calculate_costs(board_data, players)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move from the lowest cost list
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        # print()
        # print(f"{self.name} potential move {move_key}/{total_potential_moves}")
        # print(f"cost = {costs[min_cost_indices[random_move_index]]}")
        # print(f"cost from piece size = {self.square_scaler * self.potential_moves[move_key]['piece_squares']}")
        # print(f"cost from distance = {self.closeness_scaler / self.potential_moves[move_key]['norm_distance']}")
        # print(f"cost from corners blocked = {self.opponent_corners_blocked_scaler * self.potential_moves[move_key]['number_opponent_corners_blocked']}")

        # print()
        # print(f"piece_squares = {self.potential_moves[move_key]['piece_squares']}")
        # print(f"distance = {self.potential_moves[move_key]['distance']}")
        # print(f"number_opponent_corners_blocked = {self.potential_moves[move_key]['number_opponent_corners_blocked']}" )

        return move_key
