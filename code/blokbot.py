"""main.py

Main script for playing the polymino game.
"""

import argparse
import time

import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *

from Board import Board

FPS_TARGET = 50

### Command line argument parser function
def parse_args():
    """Parse command line arguments.  All should be optional."""
    prog = "main"
    description = """Polymino playing game"""
    epilog = """
    Examples:
    ---------
    1. Play four player game on a 20x20 board:
    python main.py

    2. Play two player game on a 14x14 board:
    python main.py --duo

    3. Play four player game where RandomBot controls Player 2 and Player 4:
    python main.py -p2 random -p4 random

    4. Play four player game where SnugBot controls Player 2, 
       BigBot controls player 3, and Blockbot controls Player 4:
    python main.py -p2 snug -p3 big -p4 block



    List of eligible bot names:
    1. random: RandomBot
    2. big: BigBot
    3. smol: SmolBot
    4. snug: SnugBot
    5. block: BlockBot
    """
    parser = argparse.ArgumentParser(
        prog=prog,
        description=description,
        epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "-p1",
        "--player1",
        type=str,
        help="str. Name of player 1.",
    )
    parser.add_argument(
        "-p2",
        "--player2",
        type=str,
        help="str. Name of player 2.",
    )
    parser.add_argument(
        "-p3",
        "--player3",
        type=str,
        help="str. Name of player 3.",
    )
    parser.add_argument(
        "-p4",
        "--player4",
        type=str,
        help="str. Name of player 4.",
    )
    parser.add_argument(
        "-d",
        "--duo",
        action="store_true",
        help="flag. If set, plays a two-player game.",
    )
    parser.add_argument(
        "-ns",
        "--nosave",
        action="store_true",
        help="flag. If set, do not save the game history and score at the end of the game.",
    )
    parser.add_argument(
        "-q",
        "--quickquit",
        action="store_true",
        help="flag. If set, quit the game immediately at the end of the game. (not recommended for humans)",
    )

    args = parser.parse_args()

    player1 = args.player1
    player2 = args.player2
    player3 = args.player3
    player4 = args.player4
    duo = args.duo
    nosave = args.nosave
    quickquit = args.quickquit

    if player1:
        player1 = player1.lower()
    if player2:
        player2 = player2.lower()
    if player3:
        player3 = player3.lower()
    if player4:
        player4 = player4.lower()

    print()
    print(f"player1 = {player1}")
    print(f"player2 = {player2}")
    print(f"player3 = {player3}")
    print(f"player4 = {player4}")
    print(f"duo = {duo}")
    print(f"nosave = {nosave}")
    print(f"quickquit = {quickquit}")

    return player1, player2, player3, player4, duo, nosave, quickquit


def game_loop(player1, player2, player3, player4, duo, nosave, quickquit):
    """Main pygame and PyOpenGL initialization and loop"""

    # initialize pygame and setup an opengl display
    window_size = 1250, 800
    window_ratio = window_size[1] / window_size[0]
    pygame.init()
    screen = pygame.display.set_mode(window_size, OPENGL | DOUBLEBUF | RESIZABLE)
    glEnable(GL_DEPTH_TEST)  # use our zbuffer

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # setup the camera
    glMatrixMode(GL_PROJECTION)
    # gluPerspective(45.0,1000/1000,0.1,1000.0)  #setup lens

    # Define help font
    font = pygame.font.Font(None, 26)

    # setup ortho coordinate system
    left = -10
    right = 10
    down = window_ratio * left
    up = window_ratio * right
    near = -1
    far = 1
    glOrtho(left, right, down, up, near, far)
    # glOrtho(-10,110,-10,70,-1,1)

    # glTranslatef(0, 0, -100)          # move back
    # glRotatef(-20, 1, 0, 0)           # orbit higher

    # initialize a board
    # Blokus Duo
    if duo:
        player_number = 2
        board_size = 14
    else:
        # Blokus
        player_number = 4
        board_size = 20

    board = Board(
        player_number,
        player1,
        player2,
        player3,
        player4,
        board_size,
        left,
        right,
        down,
        up,
        window_size,
        font,
    )

    # initialize a timer in milliseconds
    nt = int(time.time() * 1000)

    going = True
    while going:
        # add 20 milliseconds to timer
        nt += 1000 // FPS_TARGET

        # if player is a robot, make the move
        if board.is_current_player_robot and not board.game_over:
            board.make_move()

        # check for events
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                going = False

            elif event.type == VIDEORESIZE:
                glLoadIdentity()
                new_window_size = event.w, event.h
                glViewport(0, 0, new_window_size[0], new_window_size[1])

                new_left = left * new_window_size[0] / window_size[0]
                new_right = right * new_window_size[0] / window_size[0]
                new_down = down * new_window_size[1] / window_size[1]
                new_up = up * new_window_size[1] / window_size[1]

                board.update_window_size(
                    new_left, new_right, new_down, new_up, new_window_size
                )

                # glOrtho(1 / left, 1 / right, 1 / down, 1 / up, 1 / near, 1 / far)
                glMatrixMode(GL_PROJECTION)
                glOrtho(new_left, new_right, new_down, new_up, near, far)

            elif event.type == KEYDOWN:
                last_key = event.key

                # if the game is over, only catch some events
                if board.game_over:
                    if last_key == K_SPACE:
                        board.toggle_game_review()
                    continue

                if last_key == K_UP:
                    board.increment_piece_index()

                if last_key == K_DOWN:
                    board.decrement_piece_index()

                if last_key == K_r:
                    board.rotate_piece()

                if last_key == K_e:
                    board.rotate_piece_counterclockwise()

                if last_key == K_f:
                    board.flip_piece()

                if last_key == K_g:
                    board.invert_piece()

                if last_key == K_p:
                    board.skip_turn()

                if last_key == K_h:
                    board.toggle_help()

                if last_key == K_x:
                    board.toggle_edge_help()

                if last_key == K_c:
                    board.toggle_corner_help()

                if last_key == K_o:
                    board.toggle_score()

                if last_key == K_l:
                    board.increment_player_color()

                if last_key == K_b:
                    board.toggle_potential_move_shown()

                if last_key == K_n:
                    board.decrement_potential_move()

                if last_key == K_m:
                    board.increment_potential_move()

                if last_key == K_COMMA:
                    for ii in range(10):
                        board.decrement_potential_move()

                if last_key == K_PERIOD:
                    for ii in range(10):
                        board.increment_potential_move()

            elif event.type == MOUSEWHEEL:
                if event.y > 0:
                    board.increment_piece_index()

                elif event.y < 0:
                    board.decrement_piece_index()

            # if the game is over, don't catch the mouse clicks
            if board.game_over:
                continue

            # get mouse position on window
            mouse_pos = pygame.mouse.get_pos()
            board.get_square_mouse_over(mouse_pos)

            board.highlight_hover()

            if board.x_index is not None and board.y_index is not None:

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    board.make_move()
                    # pygame.event.set_grab(not pygame.event.get_grab())

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
                    # board.increment_color_index()
                    pass
                    # pygame.mouse.set_visible(not pygame.mouse.get_visible())

        if board.game_over:
            if not board.are_winners_determined:
                board.determine_winners()

            if not nosave and not board.is_game_saved:
                board.save_game()

            if board.is_game_reviewing:
                board.review_game()

            if quickquit:
                going = False

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # game action
        board.draw_board()

        # update the full display Surface to the screen
        pygame.display.flip()

        # find how long it took for game action to be calculated and drawn
        ct = int(time.time() * 1000)

        # wait for the difference in expected versus actual time passed, unless it is less than one.
        pygame.time.wait(max(1, nt - ct))

    pygame.display.quit()
    pygame.quit()
    return


if __name__ == "__main__":
    player1, player2, player3, player4, duo, nosave, quickquit = parse_args()
    game_loop(player1, player2, player3, player4, duo, nosave, quickquit)
