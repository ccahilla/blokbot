#!/usr/bin/env python


###############################################################################
# Action Happens Here 50 times per second

def tick(ii):
    """Draws a shape on the screen
    """
    #glRotatef(1, 0, 0, 1)
    #glTranslatef(0, 0, 1)

    # Draw Axis
    axis()

    # Draw Lissajous curve
    amp = 6
    dot_size = 0.2
    base_freq = 0.01
    nx = 3
    ny = 1
    for phi in np.linspace(0, 2*np.pi, 300):
        xx = amp * math.cos(nx * phi + base_freq * ii)
        yy = amp * math.sin(ny * phi + base_freq * ii)

        point = xx, yy, 0
        color = yy, 1.0, xx
        cquad(point, dot_size, color)  #(center, diameter, color)

###############################################################################
# The rest of this is the bones that make it work

import time
import pygame

from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

from OpenGL.arrays import vbo

import math
import numpy as np

FPS_TARGET = 50






def axis():
    glBegin(GL_LINES)

    #x = red
    #y = green
    #z = blue

    glColor3f(1, 0, 0)
    glVertex3fv((0, 0, 0))
    glVertex3fv((1, 0, 0))

    glColor3f(0, 1, 0)
    glVertex3fv((0, 0, 0))
    glVertex3fv((0, 1, 0))

    glColor3f(0, 0, 1)
    glVertex3fv((0, 0, 0))
    glVertex3fv((0, 0, 1))

    glEnd()
    return

def quad(points, color):
    glBegin(GL_QUADS)
    glColor3f(*color)
    for p in points:
        glVertex3fv(p)
    glEnd()
    return

def cquad(point, size, color):
    glBegin(GL_QUADS)
    glColor3f(*color)
    x,y,z = point
    s = size/2.0
    glVertex3fv((x-s,y-s,z))
    glVertex3fv((x+s,y-s,z))
    glVertex3fv((x+s,y+s,z))
    glVertex3fv((x-s,y+s,z))
    glEnd()
    return




def main():

    #initialize pygame and setup an opengl display
    window_size = 1200, 800
    window_ratio = window_size[1]/window_size[0]
    pygame.init()
    pygame.display.set_mode(window_size, OPENGL|DOUBLEBUF)
    glEnable(GL_DEPTH_TEST)    #use our zbuffer

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    #setup the camera
    glMatrixMode(GL_PROJECTION)
    #gluPerspective(45.0,1000/1000,0.1,1000.0)  #setup lens
    
    # setup coordinate system
    left = -10
    right = 10
    down = window_ratio * left
    up = window_ratio * right
    near = -1
    far = 1
    glOrtho(left, right, down, up, near, far)
    # glOrtho(-10,110,-10,70,-1,1)

    #glTranslatef(0, 0, -100)        #move back
    #glRotatef(-20, 1, 0, 0)             #orbit higher

    # initialize a timer in milliseconds
    nt = int(time.time() * 1000)

    for i in range(2**63):
        # add 20 milliseconds to timer
        nt += 1000//FPS_TARGET

        # check for quit'n events
        event = pygame.event.poll()
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            break

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

        # game action
        tick(i)

        # update the full display Surface to the screen
        pygame.display.flip()

        # find how long it took for game action to be calculated and drawn
        ct = int(time.time() * 1000)

        # wait for the difference in expected versus actual time passed, unless it is less than one.
        pygame.time.wait(max(1,nt - ct))

        if i % FPS_TARGET == 0:
            print(nt - ct)




if __name__ == '__main__':
    main()
