"""blokbot.py

Right now, plays a game of blokus with no rules.
Craig Cahillane
Jan 8, 2022
"""
import math
import sys
import time

import numpy as np
import pygame
from OpenGL.arrays import vbo
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *

FPS_TARGET = 50


class Board:
    """Blokus board class.
    Holds the information about the board being drawn in public variables.

    Inputs:
    -------
    size: int
        number of squares on the blokus board
    player_number: int
        number of players in the game
    left: float
        window edge
    right: float
        window edge
    down: float
        window edge
    up: float
        window edge
    """

    def __init__(self, player_number, size, left, right, down, up, window_size):
        """Constructor for class Board
        """
        self.player_number = player_number
        self.size = size
        self.left = left
        self.right = right
        self.down = down
        self.up = up
        self.window_size = window_size

        ortho_height = self.up - self.down
        # ortho_width = self.right - self.left

        self.board_width = 0.95 * ortho_height
        self.square_size = self.board_width / self.size

        # location of the board lines
        self.vlines = np.array([])
        self.hlines = np.array([])

        # initialize board data
        self.board_data = np.zeros(shape=(self.size, self.size), dtype=int)
        self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)

        # initialize colors
        color_index = 4
        self.colors = np.array(
            [
                "#000000",
                "#1f77b4",
                "#ffe12b",
                "#d62728",
                "#2ca02c",
                "#a63300",
                "#9400d3",
                "#f88379",
                "#17becf",
                "#f97306",
                "#bfbfbf",
                "#8c564b",
            ]
        )
        # hold index of colors used
        self.used_colors = []

        # starting piece index
        self.piece_index = 9

        # Define players
        self.players = []
        self.player_index = 0
        # holds the edge squares, 0 is not an edge, 1 is an edge and no square of this player's color can go there
        self.edge_boards = {}

        # holds where the corners currently are, 0 means no corner
        self.corner_boards = {}

        for ii in range(self.player_number):
            index = ii + 1
            color_index += 1

            name = f"Player {index}"
            color = self.colors[color_index]

            temp_player = Player(index, name, color, color_index)
            self.players.append(temp_player)

            self.used_colors.append(color_index)

            self.edge_boards[index] = np.zeros(shape=(self.size, self.size), dtype=int)
            self.corner_boards[index] = np.zeros(
                shape=(self.size, self.size), dtype=int
            )

        # current move legality boolean
        self.is_move_possible = True
        self.is_move_legal = False

        # edge and corner help
        self.are_edges_visible = False
        self.are_corners_visible = True

        # score shown
        self.is_score_shown = True

        # help message toggle
        self.auto_help_toggle = (
            False  # automatically toggles help after everyone's first move
        )
        self.is_help_shown = True

        return

    def update_window_size(self, left, right, down, up, window_size):
        """If window gets resized, update the self.window_size variable,
        and other associated variables
        """
        self.left = left
        self.right = right
        self.down = down
        self.up = up
        self.window_size = window_size

        ortho_height = self.up - self.down
        # ortho_width = self.right - self.left

        self.board_width = 0.95 * ortho_height
        self.square_size = self.board_width / self.size

        return

    def toggle_edge_help(self):
        """Toggles the edge viewer on and off
        """
        self.are_edges_visible = not self.are_edges_visible
        return

    def toggle_corner_help(self):
        """Toggles the corner viewer on and off
        """
        self.are_corners_visible = not self.are_corners_visible
        return

    def toggle_score(self):
        """Toggles the score viewer on and off
        """
        self.is_score_shown = not self.is_score_shown
        return

    def toggle_help(self):
        """Toggles the help message on and off
        """
        self.is_help_shown = not self.is_help_shown
        return

    def increment_player_index(self):
        """changes whose turn it is by incrementing player_index
        """
        self.player_index += 1
        if self.player_index == self.player_number:
            self.player_index = 0

        return

    # def increment_color_index(self):
    #     """changes the color of the mouse clicks
    #     """
    #     self.color_index += 1
    #     if self.color_index > len(self.colors) - 1:
    #         self.color_index = 1

    #     return

    def increment_player_color(self):
        """increments current player's color to next unused color, 
        and also changes the board data
        """
        current_player = self.get_current_player()
        color_index = current_player.color_index

        while color_index in self.used_colors:
            color_index += 1
            if color_index > len(self.colors) - 1:
                color_index = 1

        # next unused color found, pop the old color off the list of used colors
        self.used_colors.remove(current_player.color_index)

        # change piece colors
        for piece in current_player.pieces:
            piece.color = self.colors[color_index]

        # change the board data from old index to new index
        self.board_data[self.board_data == current_player.color_index] = color_index

        # change the player color
        current_player.color_index = color_index
        current_player.color = self.colors[color_index]

        # add new color to the list of used_colors
        self.used_colors.append(color_index)

        return

    def increment_piece_index(self):
        """change the piece the current player is holding, by adding one to that index
        """
        current_player = self.get_current_player()
        current_player.increment_piece_index()
        return

    def decrement_piece_index(self):
        """change the piece the current player is holding, by subtracting one from that index
        """
        current_player = self.get_current_player()
        current_player.decrement_piece_index()
        return

    def get_ortho_coordinates(self, window_x, window_y):
        """Transfers window (x, y) coordinates to ortho game (x, y) coordinates,
        Ortho coords have (0, 0) in the center.
        """
        # First, calculate the "normalized" mouse coordinates by dividing by window_size
        mouse_norm_x = window_x / self.window_size[0]
        mouse_norm_y = window_y / self.window_size[1]

        # Map those coordinates to your orthographic projection range
        ortho_x = (mouse_norm_x * (self.right - self.left)) + self.left
        ortho_y = (mouse_norm_y * (self.down - self.up)) + self.up

        return ortho_x, ortho_y

    def get_window_coordinates(self, ortho_x, ortho_y):
        """Transfers ortho game (x, y) coordinates to window (x, y) coordinates.
        Window coords have (0, 0) in the bottom left.
        """
        window_x = (
            self.window_size[0] * (ortho_x - self.left) / (self.right - self.left)
        )
        window_y = self.window_size[1] * (ortho_y - self.up) / (self.down - self.up)

        return window_x, window_y

    def get_square_mouse_over(self, mouse_pos):
        """gets the square the mouse is currently over.
        If over a square, returns indicies of that square (ii, jj)
        If not over a square, returns (None, None)
        """
        # First, calculate the "normalized" mouse coordinates by dividing by window_size
        mouse_norm_x = mouse_pos[0] / self.window_size[0]
        mouse_norm_y = mouse_pos[1] / self.window_size[1]

        # Map those coordinates to your orthographic projection range
        ortho_x = (mouse_norm_x * (self.right - self.left)) + self.left
        ortho_y = (mouse_norm_y * (self.down - self.up)) + self.up
        # ortho = (ortho_x, ortho_y)
        # print(f"mouse x = {mouse_pos[0]}, mouse y = {mouse_pos[1]}")
        # print(f"ortho x = {ortho_x}, ortho y = {ortho_y}")

        # Check if x- and y-coordinates are within some square's range
        x_index = None
        for ii, vline in enumerate(self.vlines):
            if vline < ortho_x and self.vlines[-1] > ortho_x:
                if ii < self.size:
                    x_index = ii

        y_index = None
        for jj, hline in enumerate(self.hlines):
            if hline < ortho_y and self.hlines[-1] > ortho_y:
                if jj < self.size:
                    y_index = jj

        return x_index, y_index

    def get_current_player(self):
        """Gets the current player object
        """
        current_player = self.players[self.player_index]
        return current_player

    def get_current_player_color(self):
        """Gets the current piece object being held by the current player.
        """
        current_player = self.get_current_player()
        return current_player.color

    def get_current_player_piece(self):
        """Gets the current piece object being held by the current player.
        """
        current_player = self.get_current_player()
        current_piece = current_player.pieces[current_player.piece_index]
        return current_piece

    def rotate_piece(self):
        """Rotates piece currently held by player
        """
        piece = self.get_current_player_piece()
        piece.rotate_piece()
        return

    def rotate_piece_counterclockwise(self):
        """Rotates piece currently held by player
        """
        piece = self.get_current_player_piece()
        piece.rotate_piece_counterclockwise()
        return

    def flip_piece(self):
        """Flips piece currently held by player on x-axis
        """
        piece = self.get_current_player_piece()
        piece.flip_piece()
        return

    def invert_piece(self):
        """Flips piece currently held by player on y-axis
        """
        piece = self.get_current_player_piece()
        piece.invert_piece()
        return

    def skip_turn(self):
        """If current_player pressed the 'p' button,
        skips their turn by incrementing player index
        """
        self.increment_player_index()
        return

    def check_move_possible(self, x_index, y_index):
        """checks the current proposed move is physically possible,
        i.e. can be placed fully on board and not on any other piece,
        then sets self.is_move_possible boolean value.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        # current_player = self.get_current_player()
        current_piece = self.get_current_player_piece()
        temp_piece_coordinates = current_piece.piece_coordinates

        # check the board data to make sure all potential fill spots are empty
        self.is_move_possible = True
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index
            # ensure we're inside the board boundaries
            if (
                0 <= temp_x
                and temp_x < self.size
                and 0 <= temp_y
                and temp_y < self.size
            ):
                # if we aren't, then ensure the board spaces are empty, i.e. == 0
                if self.board_data[temp_x, temp_y]:
                    self.is_move_possible = False
                    break
            else:
                self.is_move_possible = False
                break

        return

    def check_move_legality(self, x_index, y_index):
        """checks the current proposed move is legal according to the rules,
        i.e. can be placed touching the corner of another same colored piece,
        then sets self.is_move_legal boolean value.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        current_player = self.get_current_player()
        current_piece = self.get_current_player_piece()
        temp_piece_coordinates = current_piece.piece_coordinates

        # check the first move is legal
        if current_player.moves == 0:
            if self.player_number == 2:
                self.check_first_move_legality_two_players(x_index, y_index)
            elif self.player_number == 4:
                self.check_first_move_legality_four_players(x_index, y_index)
            else:
                print(f"self.player_number == {self.player_number}")
                print("Not a valid number of players")
                sys.exit(1)

        else:
            edge_hit = False
            corner_hit = False
            self.is_move_legal = False
            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + x_index
                temp_y = coordinate[1] + y_index

                edge = self.edge_boards[current_player.order][temp_x, temp_y]
                corner = self.corner_boards[current_player.order][temp_x, temp_y]

                if edge:
                    edge_hit = True
                    break
                elif corner:
                    corner_hit = True

            if corner_hit and not edge_hit:
                self.is_move_legal = True

        # Toggle help off automatically after player's have moved once
        if not self.auto_help_toggle:
            # print("checking if all players moved once")
            all_moved_once = self.check_each_player_moved_once()
            if all_moved_once:
                self.auto_help_toggle = True
                self.is_help_shown = False

        return

    def check_first_move_legality_two_players(self, x_index, y_index):
        """Checks if the first move is legal for a two player game.
        In this case, the first player must move such that the [4,4] square is covered,
        and the second player must cover the [9,9] square

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        current_player = self.get_current_player()
        current_piece = self.get_current_player_piece()
        temp_piece_coordinates = current_piece.piece_coordinates

        if current_player.order == 1:
            x_required = 4
            y_required = 4
        elif current_player.order == 2:
            x_required = 9
            y_required = 9

        # set the first legal square for movement, aka "corner"
        self.corner_boards[current_player.order][x_required, y_required] = 1

        # move is not legal unless it's over the correct square
        self.is_move_legal = False
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index

            if temp_x == x_required and temp_y == y_required:
                self.is_move_legal = True
                break

        return

    def check_first_move_legality_four_players(self, x_index, y_index):
        """Checks if the first move is legal for a two player game.
        In this case, the first player must move such that the [0,0] square is covered,
        and the second player must cover the [0,19] square,
        the third the [19,19] square,
        and the fourth the [19,0] square.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        current_player = self.get_current_player()
        current_piece = self.get_current_player_piece()
        temp_piece_coordinates = current_piece.piece_coordinates

        if current_player.order == 1:
            x_required = 0
            y_required = 0
        elif current_player.order == 2:
            x_required = 0
            y_required = 19
        elif current_player.order == 3:
            x_required = 19
            y_required = 19
        elif current_player.order == 4:
            x_required = 19
            y_required = 0

        # set the first legal square for movement, aka "corner"
        self.corner_boards[current_player.order][x_required, y_required] = 1

        # move is not legal unless it's over the correct square
        self.is_move_legal = False
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index

            if temp_x == x_required and temp_y == y_required:
                self.is_move_legal = True
                break
        return

    def check_each_player_moved_once(self):
        """Checks if each player has moved at least once.
        """
        all_moved_once = True
        for player in self.players:
            if player.moves == 0:
                all_moved_once = False
                break
        return all_moved_once

    def highlight_hover(self, x_index, y_index):
        """highlights the square we're hovered over,
        zeros everything else

        Inputs:
        -------
        x_index: int
            x-coordinate of the square to highlight
        y_index: int
            y-coordinate of the square to highlight
        """
        current_piece = self.get_current_player_piece()
        temp_piece_coordinates = current_piece.piece_coordinates

        # check the current move is legal
        self.check_move_possible(x_index, y_index)

        # if it is possible, check if it is legal
        if self.is_move_possible:
            self.check_move_legality(x_index, y_index)

        # populate the hover_board
        self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index
            if (
                0 <= temp_x
                and temp_x < self.size
                and 0 <= temp_y
                and temp_y < self.size
            ):
                self.hover_board[temp_x, temp_y] = 1
        return

    def click_color(self, x_index, y_index):
        """Places a piece of the current player's color, if possible.
        Also repopulates the edge_board and corner_board 

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        current_player = self.get_current_player()
        current_piece = self.get_current_player_piece()
        temp_piece_coordinates = current_piece.piece_coordinates

        # check the current move is physically possible
        self.check_move_possible(x_index, y_index)

        # if it is possible, check if it is legal
        if self.is_move_possible:
            self.check_move_legality(x_index, y_index)

        # if the move is legal, then place the piece, and take it out of the running
        if self.is_move_possible and self.is_move_legal:
            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + x_index
                temp_y = coordinate[1] + y_index
                self.board_data[temp_x, temp_y] = current_player.color_index
                for ii in range(self.player_number):
                    index = ii + 1
                    self.edge_boards[index][temp_x, temp_y] = 0
                    self.corner_boards[index][temp_x, temp_y] = 0

            # update the edge board and corner board
            edge_board = self.edge_boards[current_player.order]
            corner_board = self.corner_boards[current_player.order]
            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + x_index
                temp_y = coordinate[1] + y_index

                # edges
                for xx, yy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    edge_x = temp_x + xx
                    edge_y = temp_y + yy
                    if (
                        0 <= edge_x
                        and edge_x < self.size
                        and 0 <= edge_y
                        and edge_y < self.size
                    ):  # square not off edge of board
                        if self.board_data[edge_x, edge_y] == 0:  # if square is empty
                            edge_board[edge_x, edge_y] = 1
                            corner_board[edge_x, edge_y] = 0

                # corners
                for xx, yy in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                    corner_x = temp_x + xx
                    corner_y = temp_y + yy
                    if (
                        0 <= corner_x
                        and corner_x < self.size
                        and 0 <= corner_y
                        and corner_y < self.size
                    ):  # square not off edge of board
                        if (
                            self.board_data[corner_x, corner_y] == 0
                        ):  # if square is empty
                            if (
                                edge_board[corner_x, corner_y] == 0
                            ):  # if square not an edge
                                corner_board[corner_x, corner_y] = 1

                # update the class variables
                self.edge_boards[current_player.order] = edge_board
                self.corner_boards[current_player.order] = corner_board

            # change pieces_played index to True, so it cannot be played again
            current_player.piece_played(current_player.piece_index)

            # increment the piece being held
            current_player.increment_piece_index()

            # increment the player
            self.increment_player_index()

        return

    def draw_board(self):
        """Draws the board on the OpenGL screen
        """
        # draw vertical lines
        self.vlines = np.array([])
        for ii in range(self.size + 1):
            x0 = ii * self.square_size - self.board_width / 2.0
            y1 = self.board_width / 2.0
            y2 = -self.board_width / 2.0

            point1 = (x0, y1, 0)
            point2 = (x0, y2, 0)
            line(point1, point2)

            self.vlines = np.append(self.vlines, x0)

        # draw horizontal lines
        self.hlines = np.array([])
        for jj in range(self.size + 1):
            y0 = jj * self.square_size - self.board_width / 2.0
            x1 = self.board_width / 2.0
            x2 = -self.board_width / 2.0

            point1 = (x1, y0, 0)
            point2 = (x2, y0, 0)
            line(point1, point2)

            self.hlines = np.append(self.hlines, y0)

        # calculate board center points
        self.board_center = np.zeros((self.size, self.size, 3))
        for ii in range(self.size):
            for jj in range(self.size):
                x0 = ii * self.square_size + (self.square_size - self.board_width) / 2.0
                y0 = jj * self.square_size + (self.square_size - self.board_width) / 2.0
                point = (x0, y0, 0)

                self.board_center[ii, jj] = point

        # color squares according to self.board_data
        current_player = self.get_current_player()
        player_color = current_player.color

        edge_board = self.edge_boards[current_player.order]
        corner_board = self.corner_boards[current_player.order]

        if self.is_move_possible and self.is_move_legal:
            alpha = 0.7
        else:
            alpha = 0.4

        for ii in range(self.size):
            for jj in range(self.size):
                point = self.board_center[ii, jj]
                data = self.board_data[ii, jj]
                hover = self.hover_board[ii, jj]
                edge = edge_board[ii, jj]
                corner = corner_board[ii, jj]

                if hover == 1 and data == 0:
                    hex_color = player_color
                    rgb_color = hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, alpha

                    cquad(point, self.square_size, rgba_color)

                elif hover == 1 and data != 0:
                    hex_color_player = player_color
                    hex_color_board = self.colors[data]
                    rgb_color_player = hex_to_rgb(hex_color_player)
                    rgb_color_board = hex_to_rgb(hex_color_board)
                    rgba_color_player = *rgb_color_player, alpha
                    rgba_color_board = *rgb_color_board, 1.0

                    dquad(
                        point,
                        self.square_size,
                        rgba_color_board,
                        rgba_color_player,
                        stripe_number=6,
                    )
                elif edge and self.are_edges_visible:
                    hex_color_player = player_color
                    hex_color_board = self.colors[data]
                    rgb_color_player = hex_to_rgb(hex_color_player)
                    rgb_color_board = hex_to_rgb(hex_color_board)
                    rgba_color_player = *rgb_color_player, alpha
                    rgba_color_board = *rgb_color_board, 1.0

                    dquad(
                        point,
                        self.square_size,
                        rgba_color_board,
                        rgba_color_player,
                        stripe_number=8,
                    )
                elif corner and self.are_corners_visible:
                    hex_color = player_color
                    rgb_color = hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, alpha

                    cquad(point, 0.5 * self.square_size, rgba_color)

                else:
                    hex_color = self.colors[data]
                    rgb_color = hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, 1.0

                    cquad(point, self.square_size, rgba_color)

        # draw each player's remaining pieces on the left hand side
        self.draw_remaining_pieces()

        # draw the help message, if toggled on
        if self.is_help_shown:
            self.draw_help_text()

        # draw the player scores, if toggled on
        if self.is_score_shown:
            self.draw_score_text()

        return

    def draw_remaining_pieces(self):
        """Draws each player's remaining pieces for self.draw_board()
        """

        small_square_size = self.board_width / (20 * 5)
        x_center = -self.board_width / 2.0
        y_center = -self.board_width / 2.0
        x_scaler = 6.0

        for ii, player in enumerate(self.players):
            temp_x = x_center - (ii + 1) * x_scaler * small_square_size
            player.draw_remaining_pieces(temp_x, y_center, small_square_size)
        return

    def draw_score_text(self):
        """Draws each player's score on the bottom right of the screen for self.draw_board().
        """
        board_edge = self.board_width / 2.0
        x_score_window, y_score_window = board_edge + 0.2, 3.0

        score_intro = "Score"
        x_score, y_score = self.get_window_coordinates(x_score_window, y_score_window)
        draw_text(x_score, y_score, score_intro)

        text_height = 0.6

        for ii, player in enumerate(self.players):
            index = ii + 1
            hex_color = player.color
            text_color_rgb_255 = hex_to_rgb_255(hex_color)
            text_color_rgba_255 = *text_color_rgb_255, 255

            name = player.name
            score = player.score
            score_text = f"{name} : {score}"

            temp_x_score = x_score_window
            temp_y_score = y_score_window + index * text_height

            x_score, y_score = self.get_window_coordinates(temp_x_score, temp_y_score)

            draw_text(x_score, y_score, score_text, text_color=text_color_rgba_255)

        return

    def draw_help_text(self):
        """Draws the help text if called for self.draw_board()
        """
        board_edge = self.board_width / 2.0
        x_help_window, y_help_window = board_edge + 0.1, -board_edge + 0.2

        text_height = 0.4

        # intro text
        help_text_intro = f"Help text"
        x_help, y_help = self.get_window_coordinates(x_help_window, y_help_window)
        draw_text(x_help, y_help, help_text_intro)

        # basic instructions
        help_text_basics = [
            f"Left click to place piece.",
            f"Pieces must touch corner",
            f"of similar colored piece",
            f"after first move, but",
            f"cannot touch the edge",
            f"of a similar colored piece.",
        ]
        for ii, help_text_basic in enumerate(help_text_basics):
            temp_x_help = x_help_window
            temp_y_help = y_help_window + (ii + 2) * text_height
            x_help, y_help = self.get_window_coordinates(temp_x_help, temp_y_help)
            draw_text(x_help, y_help, help_text_basic)

        # commands
        help_text_commands = [
            "r: rotates piece",
            "e: rotates piece",
            "f: flips piece left-right",
            "g: flips piece up-down",
            "up: change piece",
            "down: change piece",
            "p: skips turn",
            "l: change color",
            "x: toggle edge viewer",
            "c: toggle edge viewer",
            "o: toggle score on/off",
            "h: toggle help text",
            "esc: quits game",
        ]
        for ii, help_text_command in enumerate(help_text_commands):
            temp_x_help = x_help_window
            temp_y_help = y_help_window + (ii + len(help_text_basics) + 3) * text_height
            x_help, y_help = self.get_window_coordinates(temp_x_help, temp_y_help)
            draw_text(x_help, y_help, help_text_command)

        return


class Player:
    """Class defining a color and group of pieces belonging to a Player.
    """

    def __init__(self, order, name, color, color_index):
        # basic player variables
        self.order = order
        self.name = name
        self.color = color
        self.color_index = color_index

        self.squares_left = 89  # want to minimize this
        self.score = 0

        # define which piece player is holding in their hand
        self.piece_index = 9

        # store every type of piece
        self.pieces = []
        self.pieces_left = 21
        for ii in range(self.pieces_left):
            temp_piece = Piece(ii, color)
            self.pieces.append(temp_piece)

        # store whether a piece has been played already
        self.pieces_played = np.zeros(self.pieces_left, dtype=int)

        # number of moves made
        self.moves = 0

        # store whether the player has played all pieces, or has no available moves
        self.finished = False

        return

    def increment_piece_index(self):
        """change the piece the current player is holding, by going to the next available index
        """
        indicies = np.squeeze(np.argwhere(self.pieces_played == 0))
        if not indicies.size:  # if all pieces are played, so incidies is empty
            print()
            print(f"All pieces played for player {self.order}, congratulations!")
            print()
            self.finished = True
            return
        elif indicies.size == 1:
            indicies = np.array([indicies], dtype=int)

        # check if indicies are higher than current piece indicies
        high_indicies = np.squeeze(np.argwhere(self.piece_index < indicies))

        # if no indicies are above the current piece index
        if not high_indicies.size:
            self.piece_index = indicies[0]
        elif high_indicies.size == 1:
            self.piece_index = indicies[high_indicies]
        else:
            self.piece_index = indicies[high_indicies[0]]

        return

    def decrement_piece_index(self):
        """change the piece the current player is holding, by choosing the next lowest piece available
        """
        indicies = np.squeeze(np.argwhere(self.pieces_played == 0))
        if not indicies.size:  # if all pieces are played, so incidies is empty
            print()
            print(f"All pieces played for player {self.order}, congratulations!")
            print()
            self.finished = True
            return

        # check if indicies are lower than current piece indicies
        low_indicies = np.squeeze(np.argwhere(self.piece_index > indicies))

        # if no indicies are above the current piece index
        if not low_indicies.size:
            self.piece_index = indicies[-1]
        elif low_indicies.size == 1:
            self.piece_index = indicies[low_indicies]
        else:
            self.piece_index = indicies[low_indicies[-1]]

        return

    def piece_played(self, id):
        """If a piece is played, we want to 
        1) decrement pieces_left
        2) set Player.pieces_played[id] == 1
        3) set Piece.played = True
        """
        self.moves += 1
        self.pieces_left -= 1
        self.pieces_played[id] = 1
        current_piece = self.pieces[id]
        current_piece.set_piece_played()

        # Adjust score based on what piece was played
        self.score += current_piece.squares
        self.squares_left -= current_piece.squares

        return

    def draw_remaining_pieces(self, x_center, y_center, square_size):
        """Draw the pieces remaining to the player on the side of the board,
        centered at (x_center, y_center) in ortho units.
        class Board will have to provide the center coordinates to the player.
        small_square_size is the size of the squares in the pieces in ortho units.
        """
        # square_size acts as a scale factor
        ss = square_size

        # factor which gets added to the y-coordinate with every additional piece
        y_addition = -5 * ss

        for ii, piece in enumerate(self.pieces):
            y_addition += 5 * ss
            if piece.played:
                continue

            temp_y = y_center + y_addition
            piece.draw_piece(x_center, temp_y, square_size)

            if ii == self.piece_index:
                box_size = 5 * square_size
                self.draw_box_around_selected_piece(x_center, temp_y, box_size)

        return

    def draw_box_around_selected_piece(self, x_center, y_center, box_size):
        """Draws a box around the selected piece for player
        """
        x0 = x_center
        y0 = y_center
        bs2 = box_size / 2.0

        line((x0 - bs2, y0 - bs2, 0), (x0 - bs2, y0 + bs2, 0))
        line((x0 - bs2, y0 + bs2, 0), (x0 + bs2, y0 + bs2, 0))
        line((x0 + bs2, y0 + bs2, 0), (x0 + bs2, y0 - bs2, 0))
        line((x0 + bs2, y0 - bs2, 0), (x0 - bs2, y0 - bs2, 0))

        return


class Piece:
    """Class which defines each blokus piece.
    Here we'll define one square to be the "center square" of the piece,
    then program the squares around it based on the id number
    The piece_coordinates define the relative coordinates about the center square.
    We should be able to flip and rotate each piece about the center square,
    which will change the piece_coordinates.

    There are a total of 21 pieces:
    12 pentanimoes (5 squares) [ids: 9-20]
    id: 9   10  11  12  13   14   15  16   17  18   19   20
        □□  □□  □□  □□  □□   □□□   □  □□□  □□□ □□□□ □□□□ □□□□□\n
        □   □□   □   □□  □□  □    □□□  □  □□    □   □\n
        □□  □    □□   □  □   □     □   □\n

    5 tetranimoes (4 squares) [ids: 4-8]
    id:  4   5  6   7   8
        □□□  □□ □□ □□□ □□□□\n
        □   □□  □□  □\n

    2 trinimoes (3 squares) [ids: 2-3]
    id:  2  3
        □□ □□□\n
        □\n

    1 duanimoes (2 squares) [id: 1]
    id:  1
         □□\n
    
    1 simple square [id: 0]
    id: 0
        □
    """

    def __init__(self, id, color):
        """
        Inputs:
        -------
        id: int
            number 0-20, defines the blokus piece 
        """
        self.id = id
        self.color = color

        self.played = False  # has piece been played

        if id == 0:
            self.squares = 1
        elif id == 1:
            self.squares = 2
        elif id == 2 or id == 3:
            self.squares = 3
        elif 4 <= id and id <= 8:
            self.squares = 4
        elif 9 <= id and id <= 20:
            self.squares = 5
        else:
            print
            print("Not a valid piece id")
            print(f"id = {id} was given, id must be an int between [0,20]")
            sys.exit(1)

        # center square
        self.piece_name = "small square piece"
        self.piece_coordinates = np.array([[0, 0]], dtype=int)

        # 2 squares
        if id == 1:
            self.piece_name = "two square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))

        # 3 squares
        if id == 2:
            self.piece_name = "three corner piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
        if id == 3:
            self.piece_name = "three line piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))

        # 4 squares
        if id == 4:  # l-piece
            self.piece_name = "l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 5:  # z-piece
            self.piece_name = "z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 6:  # block piece
            self.piece_name = "block piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 7:  # t-piece
            self.piece_name = "t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
        if id == 8:  # 4 line piece
            self.piece_name = "four line piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))

        # 5 squares
        if id == 9:  # big C piece
            self.piece_name = "big c piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 10:  # block plus square piece
            self.piece_name = "block plus square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
        if id == 11:  # block plus square piece
            self.piece_name = "big z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 12:  # big w piece
            self.piece_name = "big w piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 13:  # l plus square piece
            self.piece_name = "l plus square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
        if id == 14:  # big l piece
            self.piece_name = "big l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 2]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-2, 0]))
        if id == 15:  # plus piece
            self.piece_name = "plus piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
        if id == 16:  # big l piece
            self.piece_name = "big t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -2]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
        if id == 17:  # long z piece
            self.piece_name = "long z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 18:  # long t piece
            self.piece_name = "long t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 19:  # long l piece
            self.piece_name = "long l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 20:  # long l piece
            self.piece_name = "long boi"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-2, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))

        return

    def set_piece_played(self):
        """If piece has been played, set self.played = True
        """
        self.played = True
        return

    def rotate_piece(self):
        """Rotates the piece +90 degrees
        """
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = old_y
            new_y = -old_x
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def rotate_piece_counterclockwise(self):
        """Rotates the piece -90 degrees
        """
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = -old_y
            new_y = old_x
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def flip_piece(self):
        """Flip the piece so it is a mirror image of itself along x-axis
        """
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = -old_x
            new_y = old_y
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def invert_piece(self):
        """Flip the piece so it is a mirror image of itself along y-axis
        """
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = old_x
            new_y = -old_y
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def draw_piece(self, x_center, y_center, square_size):
        """Draw the piece in it's current orientation, 
        centered at (x_center, y_center) in ortho units,
        with square_size in ortho units
        """
        # set player color
        rgb_color = hex_to_rgb(self.color)
        alpha = 1.0
        rgba_color = *rgb_color, alpha

        # square_size acts as a scale factor
        ss = square_size

        # cycle through piece coordinates, drawing each square
        for coordinate in self.piece_coordinates:
            temp_x = x_center + ss * coordinate[0]
            temp_y = y_center + ss * coordinate[1]

            point = temp_x, temp_y, 0.0

            cquad(point, ss, rgba_color)

        return


def hex_to_rgb(hex_string):
    r_hex = hex_string[1:3]
    g_hex = hex_string[3:5]
    b_hex = hex_string[5:7]
    r_rgb = int(r_hex, 16) / 255.0
    g_rgb = int(g_hex, 16) / 255.0
    b_rgb = int(b_hex, 16) / 255.0
    return r_rgb, g_rgb, b_rgb


def hex_to_rgb_255(hex_string):
    r_hex = hex_string[1:3]
    g_hex = hex_string[3:5]
    b_hex = hex_string[5:7]
    r_rgb = int(r_hex, 16)
    g_rgb = int(g_hex, 16)
    b_rgb = int(b_hex, 16)
    return r_rgb, g_rgb, b_rgb


def line(point1, point2, color=(1, 1, 1)):
    glBegin(GL_LINES)
    glColor3f(*color)
    glVertex3fv(point1)
    glVertex3fv(point2)
    glEnd()
    return


def quad(points, color):
    glBegin(GL_QUADS)
    glColor3f(*color)
    for p in points:
        glVertex3fv(p)
    glEnd()
    return


def cquad(point, size, color):
    """Color in a square with the same color
    """
    glBegin(GL_QUADS)
    if len(color) == 3:
        color = *color, 1.0
    glColor4f(*color)
    x, y, z = point
    s = size / 2.0
    glVertex3fv((x - s, y - s, z))
    glVertex3fv((x + s, y - s, z))
    glVertex3fv((x + s, y + s, z))
    glVertex3fv((x - s, y + s, z))
    glEnd()
    return


def dquad(point, size, color1, color2, stripe_number=4):
    """Color in a square with stripes of two colors
    """
    glBegin(GL_QUADS)
    if len(color1) == 3:
        color1 = *color1, 1.0
    if len(color2) == 3:
        color2 = *color2, 1.0

    x, y, z = point
    s = size / 2.0

    left = x - s
    right = x + s
    down = y - s
    up = y + s

    stripe_width = size / stripe_number

    for ii in range(stripe_number):

        if ii % 2:
            color = color1
        else:
            color = color2

        glColor4f(*color)

        temp_left = left + ii * stripe_width
        temp_right = left + (ii + 1) * stripe_width

        glVertex3fv((temp_left, down, z))
        glVertex3fv((temp_right, down, z))
        glVertex3fv((temp_right, up, z))
        glVertex3fv((temp_left, up, z))

    glEnd()
    return


def draw_text(x, y, text, text_color=(255, 255, 255, 255), bg_color=(0, 0, 0, 125)):
    text_surface = font.render(text, True, text_color, bg_color)
    text_data = pygame.image.tostring(text_surface, "RGBA", True)
    glWindowPos2d(x, y)
    glDrawPixels(
        text_surface.get_width(),
        text_surface.get_height(),
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        text_data,
    )
    return


def main():

    # initialize pygame and setup an opengl display
    window_size = 1200, 800
    window_ratio = window_size[1] / window_size[0]
    pygame.init()
    screen = pygame.display.set_mode(window_size, OPENGL | DOUBLEBUF | RESIZABLE)
    glEnable(GL_DEPTH_TEST)  # use our zbuffer

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # setup the camera
    glMatrixMode(GL_PROJECTION)
    # gluPerspective(45.0,1000/1000,0.1,1000.0)  #setup lens

    # Define help font
    global font
    font = pygame.font.Font(None, 26)

    # setup ortho coordinate system
    left = -10
    right = 10
    down = window_ratio * left
    up = window_ratio * right
    near = -1
    far = 1
    glOrtho(left, right, down, up, near, far)
    # glOrtho(-10,110,-10,70,-1,1)

    # glTranslatef(0, 0, -100)          # move back
    # glRotatef(-20, 1, 0, 0)           # orbit higher

    # initialize a blokus board
    # Blokus Duo
    player_number = 2
    board_size = 14
    # Blokus
    player_number = 4
    board_size = 20
    board = Board(player_number, board_size, left, right, down, up, window_size)

    # initialize a timer in milliseconds
    nt = int(time.time() * 1000)

    going = True
    while going:
        # add 20 milliseconds to timer
        nt += 1000 // FPS_TARGET

        # check for events
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                going = False

            elif event.type == VIDEORESIZE:
                glLoadIdentity()
                new_window_size = event.w, event.h
                glViewport(0, 0, new_window_size[0], new_window_size[1])
                # print(f"new width, height = {new_window_size[0]}, {new_window_size[1]}")

                new_left = left * new_window_size[0] / window_size[0]
                new_right = right * new_window_size[0] / window_size[0]
                new_down = down * new_window_size[1] / window_size[1]
                new_up = up * new_window_size[1] / window_size[1]

                board.update_window_size(
                    new_left, new_right, new_down, new_up, new_window_size
                )

                # glOrtho(1 / left, 1 / right, 1 / down, 1 / up, 1 / near, 1 / far)
                glMatrixMode(GL_PROJECTION)
                glOrtho(new_left, new_right, new_down, new_up, near, far)

            elif event.type == KEYDOWN:
                global last_key
                last_key = event.key

                if last_key == K_UP:
                    board.increment_piece_index()

                if last_key == K_DOWN:
                    board.decrement_piece_index()

                if last_key == K_r:
                    board.rotate_piece()

                if last_key == K_e:
                    board.rotate_piece_counterclockwise()

                if last_key == K_f:
                    board.flip_piece()

                if last_key == K_g:
                    board.invert_piece()

                if last_key == K_p:
                    board.skip_turn()

                if last_key == K_h:
                    board.toggle_help()

                if last_key == K_x:
                    board.toggle_edge_help()

                if last_key == K_c:
                    board.toggle_corner_help()

                if last_key == K_o:
                    board.toggle_score()

                if last_key == K_l:
                    board.increment_player_color()

            # get mouse position on window
            mouse_pos = pygame.mouse.get_pos()
            x_index, y_index = board.get_square_mouse_over(mouse_pos)
            if x_index is not None and y_index is not None:
                # print(f"x = {x_index}, y = {y_index}")
                board.highlight_hover(x_index, y_index)

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    board.click_color(x_index, y_index)
                    # pygame.event.set_grab(not pygame.event.get_grab())

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
                    # board.increment_color_index()
                    pass
                    # pygame.mouse.set_visible(not pygame.mouse.get_visible())

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # game action
        board.draw_board()

        # update the full display Surface to the screen
        pygame.display.flip()

        # find how long it took for game action to be calculated and drawn
        ct = int(time.time() * 1000)

        # wait for the difference in expected versus actual time passed, unless it is less than one.
        pygame.time.wait(max(1, nt - ct))

        # if i % FPS_TARGET == 0:
        #     print(nt - ct)


if __name__ == "__main__":
    main()
