"""blokbot.py

Right now, plays a game of blokus with no rules.
Craig Cahillane
Jan 8, 2022
"""
import argparse
import copy
import os
import sys
import time

import deepdish
import numpy as np
import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *

FPS_TARGET = 50


class Board:
    """Blokus board class.
    Holds the information about the board being drawn in public variables.

    Inputs:
    -------
    size: int
        number of squares on the blokus board
    player_number: int
        number of players in the game
    left: float
        window edge
    right: float
        window edge
    down: float
        window edge
    up: float
        window edge
    """

    def __init__(
        self,
        player_number,
        player1,
        player2,
        player3,
        player4,
        size,
        left,
        right,
        down,
        up,
        window_size,
    ):
        """Constructor for class Board"""
        self.player_number = player_number
        self.size = size
        self.left = left
        self.right = right
        self.down = down
        self.up = up
        self.window_size = window_size

        ortho_height = self.up - self.down
        # ortho_width = self.right - self.left

        self.board_width = 0.95 * ortho_height
        self.square_size = self.board_width / self.size

        # location of the board lines
        self.vlines = np.array([])
        self.hlines = np.array([])

        # initialize board data
        self.board_data = np.zeros(shape=(self.size, self.size), dtype=int)
        self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)

        # initialize colors
        color_index = 0
        self.colors = np.array(
            [
                "#000000",
                "#1f77b4",
                "#ffe12b",
                "#d62728",
                "#2ca02c",
                "#bfbfbf",
                "#9400d3",
                "#f88379",
                "#17becf",
                "#f97306",
                "#a63300",
                "#8c564b",
            ]
        )
        # hold index of colors used
        self.used_colors = []

        # Define players
        self.players = []
        self.player_index = 0
        self.player_types = np.array([player1, player2, player3, player4])

        for ii in range(self.player_number):
            order = ii + 1
            color_index += 1

            name = f"Player {order}"
            color = self.colors[color_index]

            if self.player_types[ii] == "random":
                temp_player = RandomBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "big":
                temp_player = BigBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "smol":
                temp_player = SmolBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "snug":
                temp_player = SnugBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "block":
                temp_player = BlockBotPlayer(order, name, color, color_index, size)
            elif self.player_types[ii] == "leak":
                temp_player = LeakBotPlayer(order, name, color, color_index, size)
            else:
                temp_player = Player(order, name, color, color_index, size)

            self.players.append(temp_player)

            self.used_colors.append(color_index)

        # cycle through all players, setting their first legal square
        for ii in range(self.player_number):
            player = self.get_current_player()
            # set starting "corner" depending on number of players
            if self.player_number == 2:
                if player.order == 1:
                    x_required = 4
                    y_required = 4
                elif player.order == 2:
                    x_required = 9
                    y_required = 9

            if self.player_number == 4:
                if player.order == 1:
                    x_required = 0
                    y_required = 0
                elif player.order == 2:
                    x_required = 0
                    y_required = 19
                elif player.order == 3:
                    x_required = 19
                    y_required = 19
                elif player.order == 4:
                    x_required = 19
                    y_required = 0

            # set the first legal square for movement, aka "corner"
            player.corner_board[x_required, y_required] = 1

            # populate potential_moves
            player.calculate_potential_moves(self.board_data)

            # move on to next player
            self.increment_player_index()

        # game history dict
        # keys will be the move number,
        # values will be the board_state and some (not all) player variables
        self.total_move_number = 0
        self.move_number = 0
        self.game_history = {}
        self.game_history["info"] = {}
        for player in self.players:
            self.game_history["info"][f"player{player.order}"] = {}
            self.game_history["info"][f"player{player.order}"]["name"] = player.name

        # winners, set by Board.determine_winners() at end of game
        self.winners = None
        self.are_winners_determined = False

        ## board flags
        # current player a robot?
        self.is_current_player_robot = None
        self.update_is_current_player_robot()  # update self.is_current_player_robot straight away

        # current move legality boolean
        self.is_move_legal = False

        # edge and corner help
        self.are_edges_visible = False
        self.are_corners_visible = True

        # score shown
        self.is_score_shown = True

        # show potential moves
        self.is_potential_move_shown = False

        # automatically toggles help after everyone's first move
        self.auto_help_toggle = False

        # help message toggle
        self.is_help_shown = True

        # game over bool
        self.game_over = False

        # game reviewing bool
        self.is_game_review_started = False
        self.is_game_reviewing = False

        # game saved bool
        self.is_game_saved = False

        return

    def update_window_size(self, left, right, down, up, window_size):
        """If window gets resized, update the self.window_size variable,
        and other associated variables
        """
        self.left = left
        self.right = right
        self.down = down
        self.up = up
        self.window_size = window_size

        ortho_height = self.up - self.down
        # ortho_width = self.right - self.left

        self.board_width = 0.95 * ortho_height
        self.square_size = self.board_width / self.size

        return

    def toggle_edge_help(self):
        """Toggles the edge viewer on and off"""
        self.are_edges_visible = not self.are_edges_visible
        return

    def toggle_corner_help(self):
        """Toggles the corner viewer on and off"""
        self.are_corners_visible = not self.are_corners_visible
        return

    def toggle_score(self):
        """Toggles the score viewer on and off"""
        self.is_score_shown = not self.is_score_shown
        return

    def toggle_potential_move_shown(self):
        """Toggles the potential move viewer on and off"""
        self.is_potential_move_shown = not self.is_potential_move_shown
        return

    def toggle_help(self):
        """Toggles the help message on and off"""
        self.is_help_shown = not self.is_help_shown
        return

    def toggle_game_review(self):
        """Toggles whether the game is being reviewed after game over"""
        self.is_game_reviewing = not self.is_game_reviewing
        return

    def increment_potential_move(self):
        """changes whose turn it is by incrementing player_index"""
        player = self.get_current_player()
        player.potential_move_index += 1
        if player.potential_move_index not in player.potential_moves:
            player.potential_move_index = 1

        return

    def decrement_potential_move(self):
        """changes whose turn it is by incrementing player_index"""
        player = self.get_current_player()
        player.potential_move_index -= 1
        if player.potential_move_index not in player.potential_moves:
            player.potential_move_index = len(player.potential_moves)

        return

    def increment_player_index(self):
        """changes whose turn it is by incrementing player_index"""
        self.player_index += 1
        if self.player_index == self.player_number:
            self.player_index = 0

        return

    # def increment_color_index(self):
    #     """changes the color of the mouse clicks
    #     """
    #     self.color_index += 1
    #     if self.color_index > len(self.colors) - 1:
    #         self.color_index = 1

    #     return

    def increment_player_color(self):
        """increments current player's color to next unused color,
        and also changes the board data
        """
        player = self.get_current_player()
        color_index = player.color_index

        while color_index in self.used_colors:
            color_index += 1
            if color_index > len(self.colors) - 1:
                color_index = 1

        # next unused color found, pop the old color off the list of used colors
        self.used_colors.remove(player.color_index)

        # change piece colors
        for piece in player.pieces:
            piece.color = self.colors[color_index]

        # change the board data from old index to new index
        self.board_data[self.board_data == player.color_index] = color_index

        # change the player color
        player.color_index = color_index
        player.color = self.colors[color_index]

        # add new color to the list of used_colors
        self.used_colors.append(color_index)

        return

    def increment_piece_index(self):
        """change the piece the current player is holding, by adding one to that index"""
        player = self.get_current_player()
        player.increment_piece_index()
        return

    def decrement_piece_index(self):
        """change the piece the current player is holding, by subtracting one from that index"""
        player = self.get_current_player()
        player.decrement_piece_index()
        return

    def get_ortho_coordinates(self, window_x, window_y):
        """Transfers window (x, y) coordinates to ortho game (x, y) coordinates,
        Ortho coords have (0, 0) in the center.
        """
        # First, calculate the "normalized" mouse coordinates by dividing by window_size
        mouse_norm_x = window_x / self.window_size[0]
        mouse_norm_y = window_y / self.window_size[1]

        # Map those coordinates to your orthographic projection range
        ortho_x = (mouse_norm_x * (self.right - self.left)) + self.left
        ortho_y = (mouse_norm_y * (self.down - self.up)) + self.up

        return ortho_x, ortho_y

    def get_window_coordinates(self, ortho_x, ortho_y):
        """Transfers ortho game (x, y) coordinates to window (x, y) coordinates.
        Window coords have (0, 0) in the bottom left.
        """
        window_x = (
            self.window_size[0] * (ortho_x - self.left) / (self.right - self.left)
        )
        window_y = self.window_size[1] * (ortho_y - self.up) / (self.down - self.up)

        return window_x, window_y

    def get_square_mouse_over(self, mouse_pos):
        """gets the square the mouse is currently over.
        If over a square, returns indicies of that square (ii, jj)
        If not over a square, returns (None, None)
        """
        # First, calculate the "normalized" mouse coordinates by dividing by window_size
        mouse_norm_x = mouse_pos[0] / self.window_size[0]
        mouse_norm_y = mouse_pos[1] / self.window_size[1]

        # Map those coordinates to your orthographic projection range
        ortho_x = (mouse_norm_x * (self.right - self.left)) + self.left
        ortho_y = (mouse_norm_y * (self.down - self.up)) + self.up

        # Check if x- and y-coordinates are within some square's range
        self.x_index = None
        for ii, vline in enumerate(self.vlines):
            if vline < ortho_x and self.vlines[-1] > ortho_x:
                if ii < self.size:
                    self.x_index = ii

        self.y_index = None
        for jj, hline in enumerate(self.hlines):
            if hline < ortho_y and self.hlines[-1] > ortho_y:
                if jj < self.size:
                    self.y_index = jj

        return

    def get_current_player(self):
        """Gets the current player object"""
        player = self.players[self.player_index]
        return player

    def get_current_player_color(self):
        """Gets the current piece object being held by the current player."""
        player = self.get_current_player()
        return player.color

    def get_current_player_piece(self):
        """Gets the current piece object being held by the current player."""
        player = self.get_current_player()
        piece = player.pieces[player.piece_index]
        return piece

    def rotate_piece(self):
        """Rotates piece currently held by player"""
        piece = self.get_current_player_piece()
        piece.rotate_piece()
        return

    def rotate_piece_counterclockwise(self):
        """Rotates piece currently held by player"""
        piece = self.get_current_player_piece()
        piece.rotate_piece_counterclockwise()
        return

    def flip_piece(self):
        """Flips piece currently held by player on x-axis"""
        piece = self.get_current_player_piece()
        piece.flip_piece()
        return

    def invert_piece(self):
        """Flips piece currently held by player on y-axis"""
        piece = self.get_current_player_piece()
        piece.invert_piece()
        return

    def update_is_current_player_robot(self):
        """Updates the is_current_player_robot flag"""
        player = self.get_current_player()
        self.is_current_player_robot = player.is_player_robot
        return

    def skip_turn(self):
        """If player pressed the 'p' button,
        skips their turn by incrementing player index.
        Calculates next player's possible moves as well,
        not doing so breaks the game.
        """
        self.increment_player_index()

        # calculate the next potential moves for the next player
        # have to do it after all others have played,
        # since some moves will become impossible after opponent moves
        next_player = self.get_current_player()
        if next_player.are_potential_moves_calculated:
            next_player.calculate_potential_moves(self.board_data)

        # update whether next_player is a robot
        self.update_is_current_player_robot()

        # check if the game is over
        self.check_each_player_finished()

        return

    def check_each_player_moved_once(self):
        """Checks if each player has moved at least once."""
        all_moved_once = True
        for player in self.players:
            if player.moves == 0:
                all_moved_once = False
                break
        return all_moved_once

    def check_each_player_finished(self):
        """Checks if each player has finished the game."""
        finished_players = 0
        for player in self.players:
            if player.finished:
                finished_players += 1

        if finished_players == len(self.players):
            self.game_over = True
        # if at least one player is totally done
        elif finished_players > 0:
            # check the number of moves available to each player
            # as it's possible that move ended the game for all
            for player in self.players:
                player.calculate_potential_moves(self.board_data)
                if len(player.potential_moves) == 0:
                    player.finished = True

            # now check again
            finished_players = 0
            for player in self.players:
                if player.finished:
                    finished_players += 1

            if finished_players == len(self.players):
                self.game_over = True

        return

    def highlight_hover(self):
        """highlights the square we're hovered over,
        zeros everything else

        Inputs:
        -------
        x_index: int
            x-coordinate of the square to highlight
        y_index: int
            y-coordinate of the square to highlight
        """
        self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)

        if self.x_index is not None and self.y_index is not None:

            player = self.get_current_player()
            piece = self.get_current_player_piece()
            temp_piece_coordinates = piece.piece_coordinates

            # populate whether move is legal
            self.is_move_legal = player.check_move_legality(
                self.x_index, self.y_index, piece, self.board_data
            )

            # populate the hover_board
            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + self.x_index
                temp_y = coordinate[1] + self.y_index
                if (
                    0 <= temp_x
                    and temp_x < self.size
                    and 0 <= temp_y
                    and temp_y < self.size
                ):
                    self.hover_board[temp_x, temp_y] = 1
        return

    def update_all_players_boards(self, old_board_data, new_board_data):
        """Updates all players corner and edge boards based on the latest piece placement,
        in a move-agnostic way."""

        # compare current board and old board
        move_board = np.subtract(new_board_data, old_board_data)

        # get all nonzero coordinates and cycle through them
        xs, ys = move_board.nonzero()

        # remove squares from edge and corner boards
        for temp_x, temp_y in zip(xs, ys):
            for temp_player in self.players:
                temp_player.edge_board[temp_x, temp_y] = 0
                temp_player.corner_board[temp_x, temp_y] = 0

        return

    def save_last_move(self):
        """Saves the latest move and the piece played"""

        scores = np.array([], dtype=int)
        for player in self.players:
            scores = np.append(scores, player.score)

        player = self.get_current_player()

        self.total_move_number += 1
        self.move_number += 1
        move_str = f"move{self.move_number}"

        self.game_history[move_str] = {}
        self.game_history[move_str]["board"] = copy.deepcopy(self.board_data)
        self.game_history[move_str]["player_order"] = player.order
        self.game_history[move_str]["piece_id"] = player.last_played_piece_id
        self.game_history[move_str]["scores"] = scores
        self.game_history[move_str]["number_potential_moves"] = len(
            player.potential_moves
        )

        return

    def make_move(self):
        """Places a piece of the current player's color, if possible.
        Also repopulates the edge_board and corner_board

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        player = self.get_current_player()
        if not self.is_current_player_robot:
            piece = self.get_current_player_piece()

            # populate whether move is legal
            self.is_move_legal = player.check_move_legality(
                self.x_index, self.y_index, piece, self.board_data
            )

        # if the move is legal, or player is robot,
        # then place the piece, and take it out of the running
        if self.is_move_legal or self.is_current_player_robot:

            # save the old board briefly
            old_board_data = copy.deepcopy(self.board_data)

            # make the move, human or robot
            if not self.is_current_player_robot:
                piece = self.get_current_player_piece()
                self.board_data = player.make_move(
                    self.x_index, self.y_index, piece, self.board_data
                )
            else:
                self.board_data = player.make_robot_move(self.board_data, self.players)

            # update all other players edge and corner boards
            self.update_all_players_boards(old_board_data, self.board_data)

            # if a new move has been made, save the move history
            if not np.array_equal(old_board_data, self.board_data):
                self.save_last_move()

            # increment to the next player
            self.increment_player_index()

            # calculate the next potential moves for the next player
            # have to do it after all others have played,
            # since some moves will become impossible after opponent moves
            next_player = self.get_current_player()
            if next_player.are_potential_moves_calculated:
                next_player.calculate_potential_moves(self.board_data)

            # update whether next_player is a robot
            self.update_is_current_player_robot()

            # toggle help off automatically after player's have moved once
            if not self.auto_help_toggle:
                all_moved_once = self.check_each_player_moved_once()
                if all_moved_once:
                    self.auto_help_toggle = True
                    self.is_help_shown = False

            # check if the game is over
            self.check_each_player_finished()

        return

    def print_board(self, board):
        """Prints a board to terminal as it appears on screen"""
        xs = np.arange(0, self.size)
        ys = np.flip(np.arange(0, self.size))

        for yy in ys:
            for xx in xs:
                print(board[xx, yy], end=" ")

            print()
        return

    def determine_winners(self):
        """Figures out who won the game based on score.
        Sets self.winners, a list of the winner(s), in case of a tie.
        """
        best_score = 0
        winners = []
        for player in self.players:
            if best_score < player.score:
                best_score = player.score
                winners = [player.name]
            elif best_score == player.score:
                winners.append(player.name)

        self.winners = winners
        self.are_winners_determined = True

        return

    def save_game(self):
        """Save the game history and score."""

        # save some final info
        self.game_history["info"]["total_moves"] = self.total_move_number

        for player in self.players:
            self.game_history["info"][f"player{player.order}"][
                "final_score"
            ] = player.score

        self.game_history["info"]["winners"] = self.winners

        # Find the save_dir, in this case the <git_repo>/saved_games/ dir
        code_dir = os.path.dirname(os.path.abspath(__file__))
        save_dir = os.path.join(code_dir, "../saved_games/")
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        # save the game .h5 filename with the timestamp
        date_time_str = time.strftime("%Y_%m_%d_%H%M%S")
        h5_filename = f"game_history_{date_time_str}.h5"
        full_h5_filename = os.path.join(save_dir, h5_filename)

        # Save the game_history dictionary with deepdish
        deepdish.io.save(full_h5_filename, self.game_history)

        self.is_game_saved = True
        return

    def review_game(self):
        """If player hits spacebar after game over,
        this function will replay the game automatically
        """
        # decide whether to show the next move
        show_next_move = False
        max_time = 0.4
        if hasattr(self, "timer"):
            time_elapsed = time.time() - self.timer
            if time_elapsed > max_time:
                show_next_move = True
        else:
            self.timer = time.time()

        # if enough time has elapsed to show the next move
        if show_next_move:
            delattr(self, "timer")
            self.move_number += 1
            if self.move_number > self.total_move_number:
                if not self.is_game_review_started:
                    self.move_number = 0
                    self.are_corners_visible = False
                    self.are_edges_visible = False
                    self.is_game_review_started = True

                else:
                    # we've reached the last move, so end the review playback
                    self.move_number = self.total_move_number
                    self.is_game_reviewing = False
                    self.is_game_review_started = False
                    return

            if self.move_number > 0:
                # update the board and set the piece to played
                move_str = f"move{self.move_number}"

                self.board_data = self.game_history[move_str]["board"]
                player_order = self.game_history[move_str]["player_order"]
                piece_id = self.game_history[move_str]["piece_id"]

                for player in self.players:
                    if player_order == player.order:
                        piece = player.pieces[piece_id]
                        break

                piece.set_piece_played()

                scores = self.game_history[move_str]["scores"]
                for score, player in zip(scores, self.players):
                    player.score = score

            else:
                # if no moves yet, set board empty
                self.board_data = np.zeros(shape=(self.size, self.size), dtype=int)
                # set all pieces to not played at first
                for player in self.players:
                    player.score = 0
                    for piece in player.pieces:
                        piece.set_piece_not_played()

        return

    def draw_board(self):
        """Main drawing function, draws the board on the OpenGL screen"""

        # draw vertical lines
        self.vlines = np.array([])
        for ii in range(self.size + 1):
            x0 = ii * self.square_size - self.board_width / 2.0
            y1 = self.board_width / 2.0
            y2 = -self.board_width / 2.0

            point1 = (x0, y1, 0)
            point2 = (x0, y2, 0)
            line(point1, point2)

            self.vlines = np.append(self.vlines, x0)

        # draw horizontal lines
        self.hlines = np.array([])
        for jj in range(self.size + 1):
            y0 = jj * self.square_size - self.board_width / 2.0
            x1 = self.board_width / 2.0
            x2 = -self.board_width / 2.0

            point1 = (x1, y0, 0)
            point2 = (x2, y0, 0)
            line(point1, point2)

            self.hlines = np.append(self.hlines, y0)

        # calculate board center points
        self.board_center = np.zeros((self.size, self.size, 3))
        for ii in range(self.size):
            for jj in range(self.size):
                x0 = ii * self.square_size + (self.square_size - self.board_width) / 2.0
                y0 = jj * self.square_size + (self.square_size - self.board_width) / 2.0
                point = (x0, y0, 0)

                self.board_center[ii, jj] = point

        # color squares according to self.board_data
        player = self.get_current_player()
        player_color = player.color

        edge_board = player.edge_board
        corner_board = player.corner_board

        if self.is_move_legal:
            alpha = 0.7
        else:
            alpha = 0.4

        for ii in range(self.size):
            for jj in range(self.size):
                point = self.board_center[ii, jj]
                data = self.board_data[ii, jj]
                hover = self.hover_board[ii, jj]
                edge = edge_board[ii, jj]
                corner = corner_board[ii, jj]

                if hover == 1 and data == 0:
                    hex_color = player_color
                    rgb_color = hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, alpha

                    cquad(point, self.square_size, rgba_color)

                elif hover == 1 and data != 0:
                    hex_color_player = player_color
                    hex_color_board = self.colors[data]
                    rgb_color_player = hex_to_rgb(hex_color_player)
                    rgb_color_board = hex_to_rgb(hex_color_board)
                    rgba_color_player = *rgb_color_player, alpha
                    rgba_color_board = *rgb_color_board, 1.0

                    dquad(
                        point,
                        self.square_size,
                        rgba_color_board,
                        rgba_color_player,
                        stripe_number=6,
                    )
                elif edge and self.are_edges_visible:
                    hex_color_player = player_color
                    hex_color_board = self.colors[data]
                    rgb_color_player = hex_to_rgb(hex_color_player)
                    rgb_color_board = hex_to_rgb(hex_color_board)
                    rgba_color_player = *rgb_color_player, alpha
                    rgba_color_board = *rgb_color_board, 1.0

                    dquad(
                        point,
                        self.square_size,
                        rgba_color_board,
                        rgba_color_player,
                        stripe_number=8,
                    )
                elif corner and self.are_corners_visible:
                    hex_color = player_color
                    rgb_color = hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, alpha

                    cquad(point, 0.5 * self.square_size, rgba_color)

                elif data != 0:
                    hex_color = self.colors[data]
                    rgb_color = hex_to_rgb(hex_color)
                    rgba_color = *rgb_color, 1.0

                    cquad(point, self.square_size, rgba_color)

        # draw each player's remaining pieces on the left hand side
        self.draw_remaining_pieces()

        # draw the help message, if toggled on
        if self.is_help_shown:
            self.draw_help_text()

        # draw the player scores, if toggled on
        if self.is_score_shown:
            self.draw_score_text()

        # draw potential move, if toggled on
        if self.is_potential_move_shown and player.are_potential_moves_calculated:
            self.draw_potential_move()

        # draw game over message if game is over
        if self.game_over:
            self.draw_game_over_message()
            self.hover_board = np.zeros(shape=(self.size, self.size), dtype=int)

        return

    def draw_remaining_pieces(self):
        """Draws each player's remaining pieces for self.draw_board()"""

        small_square_size = self.board_width / (20 * 5)
        x_center = -self.board_width / 2.0
        y_center = -self.board_width / 2.0
        x_scaler = 6.0

        for ii, player in enumerate(self.players):
            temp_x = x_center - (ii + 1) * x_scaler * small_square_size
            player.draw_remaining_pieces(
                temp_x, y_center, small_square_size, self.player_index
            )
        return

    def draw_score_text(self):
        """Draws each player's score on the bottom right of the screen for self.draw_board()."""
        board_edge = self.board_width / 2.0
        x_score_window, y_score_window = board_edge + 0.2, 3.5

        score_intro = "Score"
        x_score, y_score = self.get_window_coordinates(x_score_window, y_score_window)
        draw_text(x_score, y_score, score_intro)

        text_height = 0.5

        for ii, player in enumerate(self.players):
            index = ii + 1
            hex_color = player.color
            text_color_rgb_255 = hex_to_rgb_255(hex_color)
            text_color_rgba_255 = *text_color_rgb_255, 255

            name = player.name
            score = player.score
            score_text = f"{name} : {score}"
            if player.finished:
                score_text += " (no moves)"
            if player.order == self.player_index + 1:
                score_text += " <--"

            temp_x_score = x_score_window
            temp_y_score = y_score_window + index * text_height

            x_score, y_score = self.get_window_coordinates(temp_x_score, temp_y_score)

            draw_text(x_score, y_score, score_text, text_color=text_color_rgba_255)

        return

    def draw_help_text(self):
        """Draws the help text if called for self.draw_board()"""
        board_edge = self.board_width / 2.0
        x_help_window, y_help_window = board_edge + 0.1, -board_edge + 0.2

        text_height = 0.4

        # intro text
        help_text_intro = f"Help text"
        help_text_intro_color = (255, 255, 0, 255)
        x_help, y_help = self.get_window_coordinates(x_help_window, y_help_window)
        draw_text(x_help, y_help, help_text_intro, text_color=help_text_intro_color)

        # basic instructions
        help_text_basics = [
            f"Left click to place piece.",
            f"Pieces must touch corner",
            f"of similar colored piece",
            f"after first move, but",
            f"cannot touch the edge",
            f"of a similar colored piece.",
        ]
        for ii, help_text_basic in enumerate(help_text_basics):
            temp_x_help = x_help_window
            temp_y_help = y_help_window + (ii + 1) * text_height
            x_help, y_help = self.get_window_coordinates(temp_x_help, temp_y_help)
            draw_text(x_help, y_help, help_text_basic)

        # commands
        help_text_commands = [
            "r: rotates piece",
            "e: rotates piece",
            "f: flips piece left-right",
            "g: flips piece up-down",
            "up: change piece",
            "down: change piece",
            "p: skips turn",
            "l: change color",
            "x: toggle edge viewer",
            "c: toggle corner viewer",
            "o: toggle score on/off",
            "b: toggle potential move",
            "n: change potential move",
            "m: change potential move",
            "h: toggle help text",
            "esc: quits game",
        ]
        for ii, help_text_command in enumerate(help_text_commands):
            temp_x_help = x_help_window
            temp_y_help = y_help_window + (ii + len(help_text_basics) + 2) * text_height
            x_help, y_help = self.get_window_coordinates(temp_x_help, temp_y_help)
            draw_text(x_help, y_help, help_text_command)

        return

    def draw_potential_move(self):
        """Draws a potential move, based on player.potential_move_index"""
        player = self.get_current_player()
        potential_move_number = len(player.potential_moves)
        move_text2 = ""
        if potential_move_number > 0:
            if player.potential_move_index > potential_move_number:
                player.potential_move_index = potential_move_number

            move_dict = player.potential_moves[player.potential_move_index]

            # compare current board and potential move board
            potential_move_board = np.subtract(move_dict["board"], self.board_data)

            # get all nonzero coordinates and cycle through them
            xs, ys = potential_move_board.nonzero()

            player_color = self.get_current_player_color()
            alpha = 0.3
            for temp_x, temp_y in zip(xs, ys):
                point = self.board_center[temp_x, temp_y]

                hex_color = player_color
                rgb_color = hex_to_rgb(hex_color)
                rgba_color = *rgb_color, alpha

                cquad(point, self.square_size, rgba_color)

            move_text = (
                f"Potential move {player.potential_move_index}/{potential_move_number}"
            )
            move_text_color = (255, 255, 255, 255)
        else:
            move_text = f"No moves remaining"
            move_text2 = f"Press 'p' to skip turn"
            move_text_color = (255, 255, 0, 255)

        # Draw text to screen
        board_edge = self.board_width / 2.0
        x_move_window, y_move_window = board_edge + 0.2, 6.0

        x_move, y_move = self.get_window_coordinates(x_move_window, y_move_window)
        draw_text(x_move, y_move, move_text, text_color=move_text_color)

        if move_text2:
            text_height = 0.4
            x_move, y_move = self.get_window_coordinates(
                x_move_window, y_move_window + text_height
            )
            draw_text(x_move, y_move, move_text2, text_color=move_text_color)

        return

    def draw_game_over_message(self):
        """Draws the game over message if the game is over.
        Also turns off the main help message to free up space on the right.
        """
        self.is_help_shown = False

        text_height = 0.4
        board_edge = self.board_width / 2.0
        x_game_over_window, y_game_over_window = board_edge + 0.2, -2.0
        game_over_text_color = (186, 219, 255, 255)

        game_over_texts = [
            f"Game over",
            f"Hit spacebar to play",
            f"a game review movie,",
            f"spacebar again to pause",
            f"Review: move {self.move_number}/{self.total_move_number}",
        ]

        for ii, game_over_text in enumerate(game_over_texts):
            temp_x_game_over = x_game_over_window
            temp_y_game_over = y_game_over_window + (ii + 1) * text_height
            x_game_over, y_game_over = self.get_window_coordinates(
                temp_x_game_over, temp_y_game_over
            )
            draw_text(
                x_game_over,
                y_game_over,
                game_over_text,
                text_color=game_over_text_color,
            )

        return


class Player:
    """Class defining a color and group of pieces belonging to a Player."""

    def __init__(self, order, name, color, color_index, size):
        # basic player variables
        self.order = order
        self.name = name
        self.color = color
        self.color_index = color_index
        self.size = size

        # score variables
        self.squares_left = 89  # want to minimize this
        self.score = 0

        # define which piece player is holding in their hand
        self.piece_index = 9

        # store every type of piece
        self.pieces = []
        self.pieces_left = 21
        for ii in range(self.pieces_left):
            temp_piece = Piece(ii, color)
            self.pieces.append(temp_piece)

        # hold last Piece played
        self.last_played_piece_id = None

        # store whether a piece has been played already
        self.pieces_played = np.zeros(self.pieces_left, dtype=int)

        # number of moves made
        self.moves = 0

        # store whether the player has played all pieces, or has no available moves
        self.finished = False

        # hold the edges and corners for each player
        self.edge_board = np.zeros(shape=(self.size, self.size), dtype=int)
        self.corner_board = np.zeros(shape=(self.size, self.size), dtype=int)

        # are potential moves calculated
        self.are_potential_moves_calculated = True

        # hold the potential moves for each player
        self.potential_move_index = 1
        self.potential_moves = {}

        # set player to not robot by default
        self.is_player_robot = False

        return

    # get functions
    def get_current_piece(self):
        """Gets the current piece object being held by the current player."""
        piece = self.pieces[self.piece_index]
        return piece

    # Piece management
    def increment_piece_index(self):
        """change the piece the current player is holding, by going to the next available index"""
        indicies = np.squeeze(np.argwhere(self.pieces_played == 0))
        if not indicies.size:  # if all pieces are played, so indicies is empty
            return
        elif indicies.size == 1:
            indicies = np.array([indicies], dtype=int)

        # check if indicies are higher than current piece indicies
        high_indicies = np.squeeze(np.argwhere(self.piece_index < indicies))

        # if no indicies are above the current piece index
        if not high_indicies.size:
            self.piece_index = indicies[0]
        elif high_indicies.size == 1:
            self.piece_index = indicies[high_indicies]
        else:
            self.piece_index = indicies[high_indicies[0]]

        return

    def decrement_piece_index(self):
        """change the piece the current player is holding, by choosing the next lowest piece available"""
        indicies = np.squeeze(np.argwhere(self.pieces_played == 0))
        if not indicies.size:  # if all pieces are played, so incidies is empty
            return

        # check if indicies are lower than current piece indicies
        low_indicies = np.squeeze(np.argwhere(self.piece_index > indicies))

        # if no indicies are above the current piece index
        if not low_indicies.size:
            self.piece_index = indicies[-1]
        elif low_indicies.size == 1:
            self.piece_index = indicies[low_indicies]
        else:
            self.piece_index = indicies[low_indicies[-1]]

        return

    def piece_played(self, id):
        """If a piece is played, we want to
        1) decrement pieces_left
        2) set Player.pieces_played[id] == 1
        3) set Piece.played = True
        """
        self.moves += 1
        self.pieces_left -= 1
        self.pieces_played[id] = 1
        current_piece = self.pieces[id]
        current_piece.set_piece_played()

        # store last piece played by the Player
        self.last_played_piece_id = id

        # Adjust score based on what piece was played
        self.score += current_piece.squares
        self.squares_left -= current_piece.squares

        # if a player is finished, set them to finished
        if self.pieces_left == 0:
            print()
            print(f"All pieces played for player {self.order}, congratulations!")
            print()
            self.finished = True

        return

    # Move check functions
    def check_move_legality(self, x_index, y_index, piece, board_data):
        """checks the current proposed move is legal according to the rules,
        i.e. can be placed touching the corner of another same colored piece,
        then sets self.is_move_legal boolean value.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        temp_piece_coordinates = piece.piece_coordinates

        # check the board data to make sure all potential fill spots are empty
        is_move_possible = True
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index
            # ensure we're inside the board boundaries
            if (
                0 <= temp_x
                and temp_x < self.size
                and 0 <= temp_y
                and temp_y < self.size
            ):
                # if we aren't, then ensure the board spaces are empty, i.e. == 0
                if board_data[temp_x, temp_y]:
                    is_move_possible = False
                    break
            else:
                is_move_possible = False
                break

        is_move_legal = False
        if is_move_possible:
            # check if we hit a corner, and that we do not hit an edge
            edge_hit = False
            corner_hit = False

            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + x_index
                temp_y = coordinate[1] + y_index

                edge = self.edge_board[temp_x, temp_y]
                corner = self.corner_board[temp_x, temp_y]

                if edge:
                    edge_hit = True
                    break
                elif corner:
                    corner_hit = True

            if corner_hit and not edge_hit:
                is_move_legal = True

        return is_move_legal

    # Make move function
    def make_move(self, x_index, y_index, piece, board_data):
        """Places a piece of the current player's color.
        Move legality should already have been checked by Board.make_move()
        Also repopulates the Player edge_board and corner_board.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over

        Output:
        -------
        board_data: 2d numpy array of ints
            new board containing the human player's move
        """
        temp_piece_coordinates = piece.piece_coordinates

        # place piece on selected squares
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index
            board_data[temp_x, temp_y] = self.color_index

        # update the edge board and corner board
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index

            # edges
            for xx, yy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                edge_x = temp_x + xx
                edge_y = temp_y + yy

                # square not off edge of board
                if (
                    0 <= edge_x
                    and edge_x < self.size
                    and 0 <= edge_y
                    and edge_y < self.size
                ):
                    if board_data[edge_x, edge_y] == 0:  # if square is empty
                        self.edge_board[edge_x, edge_y] = 1
                        self.corner_board[edge_x, edge_y] = 0

            # corners
            for xx, yy in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                corner_x = temp_x + xx
                corner_y = temp_y + yy

                # square not off edge of board
                if (
                    0 <= corner_x
                    and corner_x < self.size
                    and 0 <= corner_y
                    and corner_y < self.size
                ):
                    # if square is empty
                    if board_data[corner_x, corner_y] == 0:
                        # if square not an edge
                        if self.edge_board[corner_x, corner_y] == 0:
                            self.corner_board[corner_x, corner_y] = 1

        # change pieces_played index to True, so it cannot be played again
        self.piece_played(piece.id)

        # increment the piece being held
        self.increment_piece_index()

        return board_data

    # Potential moves function
    def calculate_potential_moves(self, board_data):
        """Calculates the current players possible moves.
        Each piece has 8 possible orientations, some of which are degenerate.
        There are also mulitple possible displacements for each orientation,
        i.e. placement of the piece might be on a a couple different corners.
        """
        temp_potential_moves = {}
        moves = 0

        # get all corner coordinates and cycle through them
        xs, ys = self.corner_board.nonzero()

        for x_corner, y_corner in zip(xs, ys):

            # cycle through all remaining player pieces
            for piece in self.pieces:
                # skip pieces already placed
                if piece.played:
                    continue

                # copy the piece so as to not ruin the player's settings,
                # and to always have consistent start orientation
                cpiece = Piece(piece.id, piece.color)

                ## cycle through all orientations and displacements.
                # there are eight possible orientations for each piece
                # orientations will be stored by number of rotations and flips needed

                # two flips
                for ff in range(2):

                    # four rotations
                    for rr in range(4):

                        # apply rotations before flips
                        for ii in range(rr):
                            cpiece.rotate_piece()

                        for ii in range(ff):
                            cpiece.flip_piece()

                        # cycle through squares on the piece (5 maximum)
                        for x_piece, y_piece in cpiece.piece_coordinates:

                            # subtract the piece_coordinates to put that square on the corner
                            x_index = x_corner - x_piece
                            y_index = y_corner - y_piece

                            # try to place the piece on the corner
                            is_place_good = self.check_move_legality(
                                x_index, y_index, cpiece, board_data
                            )

                            # if the move is legal for that piece in that orientation
                            if is_place_good:

                                # form the new_board with the new piece placed
                                new_board = copy.deepcopy(board_data)
                                for coordinate in cpiece.piece_coordinates:
                                    temp_x = coordinate[0] + x_index
                                    temp_y = coordinate[1] + y_index
                                    new_board[temp_x, temp_y] = self.color_index

                                # check if the board is new
                                is_board_new = True
                                for move, move_dict in temp_potential_moves.items():
                                    # if piece is already in the dictionary
                                    if move_dict["piece_id"] == cpiece.id:
                                        if np.array_equal(
                                            move_dict["board"], new_board
                                        ):
                                            is_board_new = False
                                            break

                                # if board is new, add it to list of potential moves
                                if is_board_new:
                                    moves += 1
                                    temp_potential_moves[moves] = {}
                                    temp_potential_moves[moves]["board"] = new_board
                                    temp_potential_moves[moves]["x_corner"] = x_corner
                                    temp_potential_moves[moves]["y_corner"] = y_corner
                                    temp_potential_moves[moves]["piece_id"] = cpiece.id
                                    temp_potential_moves[moves]["flips"] = ff
                                    temp_potential_moves[moves]["rotations"] = rr
                                    temp_potential_moves[moves]["x_index"] = x_index
                                    temp_potential_moves[moves]["y_index"] = y_index

                        # undo rotations and flips
                        for ii in range(ff):
                            cpiece.flip_piece()

                        for ii in range(rr):
                            cpiece.rotate_piece_counterclockwise()

        # set player to finished if they have no moves
        total_potential_moves = len(temp_potential_moves)
        if total_potential_moves == 0:
            self.finished = True

        self.potential_moves = temp_potential_moves

        return

    def get_board_spots(self, board_data, value):
        """Gets all (x, y) pairs equal to value in board_data 2d array"""

        # make list of all ordered pairs (x, y) of board spaces belonging to each player
        board_spots = np.array([])
        for xx in range(self.size):
            for yy in range(self.size):
                if board_data[xx, yy] == value:
                    # if board_spots is None
                    if not board_spots.size:
                        board_spots = np.array([[xx, yy]], dtype=int)
                    else:
                        board_spots = np.vstack((board_spots, [xx, yy]))

        return board_spots

    # Draw functions
    def draw_remaining_pieces(self, x_center, y_center, square_size, player_index):
        """Draw the pieces remaining to the player on the side of the board,
        centered at (x_center, y_center) in ortho units.
        class Board will have to provide the center coordinates to the player.
        small_square_size is the size of the squares in the pieces in ortho units.
        """
        # square_size acts as a scale factor
        ss = square_size

        # factor which gets added to the y-coordinate with every additional piece
        y_addition = -5 * ss

        for ii, piece in enumerate(self.pieces):
            y_addition += 5 * ss
            if piece.played:
                continue

            temp_y = y_center + y_addition
            piece.draw_piece(x_center, temp_y, square_size)

            if ii == self.piece_index:
                box_size = 5 * square_size
                self.draw_box_around_selected_piece(
                    x_center, temp_y, box_size, player_index
                )

        return

    def draw_box_around_selected_piece(
        self, x_center, y_center, box_size, player_index
    ):
        """Draws a box around the selected piece for player"""
        x0 = x_center
        y0 = y_center
        bs2 = box_size / 2.0

        if self.order == player_index + 1:
            color = (1, 1, 0.2)
        else:
            color = (1, 1, 1)

        line((x0 - bs2, y0 - bs2, 0), (x0 - bs2, y0 + bs2, 0), color)
        line((x0 - bs2, y0 + bs2, 0), (x0 + bs2, y0 + bs2, 0), color)
        line((x0 + bs2, y0 + bs2, 0), (x0 + bs2, y0 - bs2, 0), color)
        line((x0 + bs2, y0 - bs2, 0), (x0 - bs2, y0 - bs2, 0), color)

        return


class BotPlayer(Player):
    """Automatic player of blok game"""

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.is_player_robot = True
        return

    def make_robot_move(self, board_data, players):
        """Choose a random move from the self.potential_moves dictionary
        Inputs:
        board_data: 2d matrix
            current board information about placed pieces and empty squares
        players: list of Players
            each player playing the game in a list.
            corner_board and edge_board used for some robots decisions.
        """
        total_potential_moves = len(self.potential_moves)

        if total_potential_moves == 0:
            # if no moves, skip turn
            self.finished = True
            new_board = board_data
        else:
            # choose a move using general self.choose_move() function
            # choose_move should be different for every bot
            move_key = self.choose_move(board_data, players)

            # get the move information
            x_index = self.potential_moves[move_key]["x_index"]
            y_index = self.potential_moves[move_key]["y_index"]
            piece_id = self.potential_moves[move_key]["piece_id"]
            ff = self.potential_moves[move_key]["flips"]
            rr = self.potential_moves[move_key]["rotations"]

            # reconstruct the piece orientation
            piece = Piece(piece_id, self.color)

            for ii in range(rr):
                piece.rotate_piece()
            for ii in range(ff):
                piece.flip_piece()

            new_board = self.make_move(x_index, y_index, piece, board_data)

            # set the piece_index to the piece_id as well,
            # so other functions can know what was played
            self.piece_index = piece_id

        return new_board


# Bot types
class RandomBotPlayer(BotPlayer):
    """Bot who always plays a random available move"""

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Random Bot {self.order}"

        return

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary."""
        total_potential_moves = len(self.potential_moves)

        # choose a random move
        move_key = np.random.randint(total_potential_moves) + 1

        return move_key


class BigBotPlayer(BotPlayer):
    """Bot who always plays the biggest piece possible"""

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Big Bot {self.order}"

        return

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Big Bot is just the inverse of the number of squares for a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = np.array([])
        for move_key, move_dict in self.potential_moves.items():
            temp_piece = Piece(move_dict["piece_id"], self.color)
            costs = np.append(costs, -temp_piece.squares)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        return move_key


class SmolBotPlayer(BotPlayer):
    """Bot who always plays the smallest piece possible"""

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Smol Bot {self.order}"

        return

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Smol Bot is just the number of squares for a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = np.array([])
        for move_key, move_dict in self.potential_moves.items():
            temp_piece = Piece(move_dict["piece_id"], self.color)
            costs = np.append(costs, temp_piece.squares)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        return move_key


class SnugBotPlayer(BotPlayer):
    """Bot who always plays the snuggest possible piece,
    i.e. the piece that borders the most opponent squares
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Snug Bot {self.order}"

        return

    def calculate_costs(self, board_data):
        """Calculates the cost of each move for Snug Bot.
        Each opponent square we border subtracts 1,
        and each square played subtracts 0.1.

        Returns a numpy.array of the costs associated with each move.
        """
        # compute the costs for each move
        costs = np.array([])

        # scalers to control the cost calculation
        square_scaler = -0.1
        opponent_edge_scaler = -1.0
        opponent_corner_scaler = -0.5

        for move_key, move_dict in self.potential_moves.items():
            # get the move information
            x_index = move_dict["x_index"]
            y_index = move_dict["y_index"]
            piece_id = move_dict["piece_id"]
            ff = move_dict["flips"]
            rr = move_dict["rotations"]

            # reconstruct the piece orientation
            piece = Piece(piece_id, self.color)

            for ii in range(rr):
                piece.rotate_piece()
            for ii in range(ff):
                piece.flip_piece()

            # calculate the number of potential new edges and corners
            number_opponent_edges = 0
            number_opponent_corners = 0
            number_off_board_edges = 0
            number_off_board_corners = 0
            for x_piece, y_piece in piece.piece_coordinates:
                temp_x = x_piece + x_index
                temp_y = y_piece + y_index

                # edges
                for xx, yy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    edge_x = temp_x + xx
                    edge_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= edge_x
                        and edge_x < self.size
                        and 0 <= edge_y
                        and edge_y < self.size
                    ):
                        # if square is not empty (should not have to check color)
                        if board_data[edge_x, edge_y] != 0:
                            number_opponent_edges += 1
                    else:
                        number_off_board_edges += 1

                # corners
                for xx, yy in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                    corner_x = temp_x + xx
                    corner_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= corner_x
                        and corner_x < self.size
                        and 0 <= corner_y
                        and corner_y < self.size
                    ):
                        # if square is not empty and not the same color as player
                        if (
                            board_data[corner_x, corner_y] != 0
                            and board_data[corner_x, corner_y] != self.color_index
                        ):
                            number_opponent_corners += 1

                    else:
                        number_off_board_corners += 1

            # calculate cost for this move
            temp_cost = (
                square_scaler * piece.squares
                + opponent_edge_scaler * number_opponent_edges
                + opponent_corner_scaler * number_opponent_corners
            )

            # store the information to check later
            move_dict["number_opponent_edges"] = number_opponent_edges
            move_dict["number_opponent_corners"] = number_opponent_corners
            move_dict["number_off_board_edges"] = number_off_board_edges
            move_dict["number_off_board_corners"] = number_off_board_corners

            costs = np.append(costs, temp_cost)

        return costs

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Snug Bot is the number of opponent squares near to a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = self.calculate_costs(board_data)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move from the lowest cost list
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        # print()
        # print(f"{self.name} potential move {move_key}/{total_potential_moves}")
        # print(f"cost = {costs[min_cost_indices[random_move_index]]}")
        # print(f"total_minimum_cost_moves = {total_minimum_cost_moves}")
        # print(
        #     f"number_opponent_edges = {self.potential_moves[move_key]['number_opponent_edges']}"
        # )
        # print(
        #     f"number_opponent_corners = {self.potential_moves[move_key]['number_opponent_corners']}"
        # )
        # print(
        #     f"number_off_board_edges = {self.potential_moves[move_key]['number_off_board_edges']}"
        # )
        # print(
        #     f"number_off_board_corners = {self.potential_moves[move_key]['number_off_board_corners']}"
        # )

        return move_key


class BlockBotPlayer(BotPlayer):
    """Bot who tries to take away corners from opponents,
    i.e. tries to block you.
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Block Bot {self.order}"

        self.square_scaler = -0.25  # reward playing big pieces
        self.closeness_scaler = (
            0.25  # slightly punish playing pieces close to your other pieces
        )
        self.opponent_corners_blocked_scaler = -1.0  # reward blocking opponents

        return

    def calculate_costs(self, board_data, players):
        """Block Bot tries to block the other players by playing pieces which
        take away the most amount of corners possible from opponents.

        Returns a numpy.array of the costs associated with each move.
        """
        # compute the costs for each move
        costs = np.array([])

        # make list of all ordered pairs (x, y) of board spaces belonging to each player
        board_spots = self.get_board_spots(board_data, self.order)

        # cycle through every potential move
        for move_key, move_dict in self.potential_moves.items():

            # get the move information
            x_index = move_dict["x_index"]
            y_index = move_dict["y_index"]
            piece_id = move_dict["piece_id"]
            ff = move_dict["flips"]
            rr = move_dict["rotations"]

            # reconstruct the piece orientation
            piece = Piece(piece_id, self.color)

            for ii in range(rr):
                piece.rotate_piece()
            for ii in range(ff):
                piece.flip_piece()

            # distance between squares and number of corners taken away
            distance = 0
            number_opponent_corners_blocked = 0

            # cycle through each player
            for player in players:

                # cycle through each piece coordinate
                for x_piece, y_piece in piece.piece_coordinates:
                    temp_x = x_piece + x_index
                    temp_y = y_piece + y_index

                    # if the player is yourself
                    if player.order == self.order:
                        ### calculate distance between current pieces and the potential move

                        # if self.score == 0, meaning no pieces have yet been played
                        if not self.score:
                            norm_distance = np.inf
                            continue

                        # for each spot belonging to this player, add to the distance
                        for xx, yy in board_spots:
                            temp_dist = np.abs(xx - temp_x) + np.abs(yy - temp_y)
                            distance += temp_dist

                        # normalize the distance by the number of squares
                        norm_distance = distance / self.score

                        # do not count toward the corners blocked total,
                        # move on to the next player
                        continue

                    temp_corner_board = player.corner_board
                    if temp_corner_board[temp_x, temp_y]:
                        number_opponent_corners_blocked += 1

            # calculate cost for this move
            temp_cost = (
                self.square_scaler * piece.squares
                + self.closeness_scaler / norm_distance
                + self.opponent_corners_blocked_scaler * number_opponent_corners_blocked
            )

            # store the information to check later
            move_dict["piece_squares"] = piece.squares
            move_dict["distance"] = distance
            move_dict["norm_distance"] = norm_distance
            move_dict[
                "number_opponent_corners_blocked"
            ] = number_opponent_corners_blocked

            costs = np.append(costs, temp_cost)

        return costs

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Smol Bot is just the number of squares for a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = self.calculate_costs(board_data, players)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move from the lowest cost list
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        # print()
        # print(f"{self.name} potential move {move_key}/{total_potential_moves}")
        # print(f"cost = {costs[min_cost_indices[random_move_index]]}")
        # print(f"cost from piece size = {self.square_scaler * self.potential_moves[move_key]['piece_squares']}")
        # print(f"cost from distance = {self.closeness_scaler / self.potential_moves[move_key]['norm_distance']}")
        # print(f"cost from corners blocked = {self.opponent_corners_blocked_scaler * self.potential_moves[move_key]['number_opponent_corners_blocked']}")

        # print()
        # print(f"piece_squares = {self.potential_moves[move_key]['piece_squares']}")
        # print(f"distance = {self.potential_moves[move_key]['distance']}")
        # print(f"number_opponent_corners_blocked = {self.potential_moves[move_key]['number_opponent_corners_blocked']}" )

        return move_key


class LeakBotPlayer(BotPlayer):
    """Bot who tries to minimize leaks.
    Leaks are connected spaces that only your opponents may play in.
    In short, it's just how connected your edge board is.

    We will punish leaks by squaring the number of connected squares.
    This will make LeakBot not want to play certain pieces,
    unless they are on the edge of the board or touching other players.
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Leak Bot {self.order}"

        self.square_scaler = -1.0  # reward playing big pieces
        self.leak_scaler = 0.2  # punish leaks
        self.leak_exponential = 1.3  # punish leaks hard

        return

    def calculate_costs(self, board_data, players):
        """Leak Bot tries to minimize leaks while playing big pieces.

        Returns a numpy.array of the costs associated with each move.
        """
        # compute the costs for each move
        costs = np.array([])

        # cycle through every potential move
        for move_key, move_dict in self.potential_moves.items():
            # get the new move board
            new_board_data = move_dict["board"]

            # get the piece
            piece_id = move_dict["piece_id"]
            piece = Piece(piece_id, self.color)

            # make list of all ordered pairs (x, y) of board spaces belonging to this player
            new_board_spots = self.get_board_spots(new_board_data, self.order)

            # hold the edges and corners for this player
            new_edge_board = np.zeros(shape=(self.size, self.size), dtype=int)
            new_corner_board = np.zeros(shape=(self.size, self.size), dtype=int)

            # for each spot belonging to this player, make new edge and corner board
            for xx, yy in new_board_spots:

                # edges
                for temp_x, temp_y in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    edge_x = temp_x + xx
                    edge_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= edge_x
                        and edge_x < self.size
                        and 0 <= edge_y
                        and edge_y < self.size
                    ):
                        if new_board_data[edge_x, edge_y] == 0:  # if square is empty
                            new_edge_board[edge_x, edge_y] = 1
                            new_corner_board[edge_x, edge_y] = 0

                # corners
                for temp_x, temp_y in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                    corner_x = temp_x + xx
                    corner_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= corner_x
                        and corner_x < self.size
                        and 0 <= corner_y
                        and corner_y < self.size
                    ):
                        # if square is empty
                        if new_board_data[corner_x, corner_y] == 0:
                            # if square not an edge
                            if new_edge_board[corner_x, corner_y] == 0:
                                new_corner_board[corner_x, corner_y] = 1

            # analyze the edge board, counting squares adjacent to each other
            number_of_islands = 0
            island_sizes = np.array([], dtype=int)
            used_edge_spots = np.array([])

            # first, get the edge board spots (x, y) coordinates
            new_edge_board_spots = self.get_board_spots(new_edge_board, 1)

            for xx, yy in new_edge_board_spots:
                current_edge_spot = np.array([[xx, yy]], dtype=int)
                # if nothing in used_edge_spots
                if not used_edge_spots.size:
                    used_edge_spots = np.array([[xx, yy]], dtype=int)
                else:
                    # check if current edge spot (xx, yy) in used_edge_spots
                    current_edge_spot = np.array([[xx, yy]], dtype=int)
                    has_edge_spot_been_used = (
                        (current_edge_spot == used_edge_spots).all(axis=1).any()
                    )

                    # if it has been used, move on
                    if has_edge_spot_been_used:
                        continue
                    else:
                        used_edge_spots = np.vstack(
                            (used_edge_spots, current_edge_spot)
                        )

                # recursively explore an island
                number_of_islands += 1
                island_size = 1
                island_size, used_edge_spots = self.recursive_island_search(
                    island_size,
                    current_edge_spot,
                    used_edge_spots,
                    new_edge_board_spots,
                )
                island_sizes = np.append(island_sizes, island_size)

            # calculate cost for this move
            temp_cost = self.square_scaler * piece.squares + self.leak_scaler * np.sum(
                island_sizes**self.leak_exponential
            )

            # store the information to check later
            move_dict["piece_squares"] = piece.squares
            move_dict["number_of_islands"] = number_of_islands
            move_dict["island_sizes"] = island_sizes

            costs = np.append(costs, temp_cost)

        return costs

    def recursive_island_search(self, island_size, current_spot, used_spots, all_spots):
        """Recursively searches for islands of edges.
        We have a grid representing edges full of ones and zeros.
        Basically, we want to find how many "islands" of ones there are,
        i.e. how many distinct sets of ones there are,
        and how big they are, i.e. 4 contiguous squares.

        We will do this by investigating each square once,
        here called current_spot,
        and looking around it in four directions.
        If current_spot has another edge next to it in all_spots,
        and has not already been looked at in used_spots,
        then we will add one to the current island size,
        and call recursive_island_search() on that edge as well.
        This will continue until the entire island has been explored.
        """
        xx = current_spot[0][0]
        yy = current_spot[0][1]
        for temp_x, temp_y in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            new_x = temp_x + xx
            new_y = temp_y + yy

            temp_spot = np.array([[new_x, new_y]], dtype=int)
            # if temp_spot (x, y) is in list of edge spots
            is_edge_spot = (temp_spot == all_spots).all(axis=1).any()
            is_used_edge_spot = (temp_spot == used_spots).all(axis=1).any()

            # if we've found an edge spot, and it's not already used
            if is_edge_spot and not is_used_edge_spot:
                # Add temp_spot to used_spots
                used_spots = np.vstack((used_spots, temp_spot))
                # increment size
                island_size += 1
                # run again with temp_spot as current_spot
                island_size, used_spots = self.recursive_island_search(
                    island_size, temp_spot, used_spots, all_spots
                )

        return island_size, used_spots

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Smol Bot is just the number of squares for a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = self.calculate_costs(board_data, players)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move from the lowest cost list
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        # print()
        # print(f"{self.name} potential move {move_key}/{total_potential_moves}")
        # print(f"cost = {costs[min_cost_indices[random_move_index]]}")
        # print(f"cost from piece size = {self.square_scaler * self.potential_moves[move_key]['piece_squares']}")
        # print(f"cost from distance = {self.closeness_scaler / self.potential_moves[move_key]['norm_distance']}")
        # print(f"cost from corners blocked = {self.opponent_corners_blocked_scaler * self.potential_moves[move_key]['number_opponent_corners_blocked']}")

        # print()
        # print(f"piece_squares = {self.potential_moves[move_key]['piece_squares']}")
        # print(f"distance = {self.potential_moves[move_key]['distance']}")
        # print(f"number_opponent_corners_blocked = {self.potential_moves[move_key]['number_opponent_corners_blocked']}" )

        return move_key


class Piece:
    """Class which defines each piece.
    Here we'll define one square to be the "center square" of the piece,
    then program the squares around it based on the id number
    The piece_coordinates define the relative coordinates about the center square.
    We should be able to flip and rotate each piece about the center square,
    which will change the piece_coordinates.

    There are a total of 21 pieces:
    12 pentanimoes (5 squares) [ids: 9-20]
    id: 9   10  11  12  13   14   15  16   17  18   19   20
        □□  □□  □□  □□  □□   □□□   □  □□□  □□□ □□□□ □□□□ □□□□□\n
        □   □□   □   □□  □□  □    □□□  □  □□    □   □\n
        □□  □    □□   □  □   □     □   □\n

    5 tetranimoes (4 squares) [ids: 4-8]
    id:  4   5  6   7   8
        □□□  □□ □□ □□□ □□□□\n
        □   □□  □□  □\n

    2 trinimoes (3 squares) [ids: 2-3]
    id:  2  3
        □□ □□□\n
        □\n

    1 duanimoes (2 squares) [id: 1]
    id:  1
         □□\n

    1 simple square [id: 0]
    id: 0
        □
    """

    def __init__(self, id, color):
        """
        Inputs:
        -------
        id: int
            number 0-20, defines the blokus piece
        """
        self.id = id
        self.color = color

        self.played = False  # has piece been played

        if id == 0:
            self.squares = 1
        elif id == 1:
            self.squares = 2
        elif id == 2 or id == 3:
            self.squares = 3
        elif 4 <= id and id <= 8:
            self.squares = 4
        elif 9 <= id and id <= 20:
            self.squares = 5
        else:
            print
            print("Not a valid piece id")
            print(f"id = {id} was given, id must be an int between [0,20]")
            sys.exit(1)

        # center square
        self.piece_name = "small square piece"
        self.piece_coordinates = np.array([[0, 0]], dtype=int)

        # 2 squares
        if id == 1:
            self.piece_name = "two square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))

        # 3 squares
        if id == 2:
            self.piece_name = "three corner piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
        if id == 3:
            self.piece_name = "three line piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))

        # 4 squares
        if id == 4:  # l-piece
            self.piece_name = "l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 5:  # z-piece
            self.piece_name = "z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 6:  # block piece
            self.piece_name = "block piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 7:  # t-piece
            self.piece_name = "t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
        if id == 8:  # 4 line piece
            self.piece_name = "four line piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))

        # 5 squares
        if id == 9:  # big C piece
            self.piece_name = "big c piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 10:  # block plus square piece
            self.piece_name = "block plus square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
        if id == 11:  # block plus square piece
            self.piece_name = "big z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 12:  # big w piece
            self.piece_name = "big w piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 13:  # l plus square piece
            self.piece_name = "l plus square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
        if id == 14:  # big l piece
            self.piece_name = "big l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 2]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-2, 0]))
        if id == 15:  # plus piece
            self.piece_name = "plus piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
        if id == 16:  # big l piece
            self.piece_name = "big t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -2]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
        if id == 17:  # long z piece
            self.piece_name = "long z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 18:  # long t piece
            self.piece_name = "long t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 19:  # long l piece
            self.piece_name = "long l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 20:  # long l piece
            self.piece_name = "long boi"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-2, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))

        return

    def set_piece_played(self):
        """If piece has been played, set self.played = True"""
        self.played = True
        return

    def set_piece_not_played(self):
        """If piece has not been played, set self.played = False.
        Useful mostly for the when the game is being reviewed
        """
        self.played = False
        return

    def rotate_piece(self):
        """Rotates the piece +90 degrees"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = old_y
            new_y = -old_x
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def rotate_piece_counterclockwise(self):
        """Rotates the piece -90 degrees"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = -old_y
            new_y = old_x
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def flip_piece(self):
        """Flip the piece so it is a mirror image of itself along x-axis"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = -old_x
            new_y = old_y
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def invert_piece(self):
        """Flip the piece so it is a mirror image of itself along y-axis"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = old_x
            new_y = -old_y
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def draw_piece(self, x_center, y_center, square_size):
        """Draw the piece in it's current orientation,
        centered at (x_center, y_center) in ortho units,
        with square_size in ortho units
        """
        # set player color
        rgb_color = hex_to_rgb(self.color)
        alpha = 1.0
        rgba_color = *rgb_color, alpha

        # square_size acts as a scale factor
        ss = square_size

        # cycle through piece coordinates, drawing each square
        for coordinate in self.piece_coordinates:
            temp_x = x_center + ss * coordinate[0]
            temp_y = y_center + ss * coordinate[1]

            point = temp_x, temp_y, 0.0

            cquad(point, ss, rgba_color)

        return


### PyOpenGL functions
def hex_to_rgb(hex_string):
    r_hex = hex_string[1:3]
    g_hex = hex_string[3:5]
    b_hex = hex_string[5:7]
    r_rgb = int(r_hex, 16) / 255.0
    g_rgb = int(g_hex, 16) / 255.0
    b_rgb = int(b_hex, 16) / 255.0
    return r_rgb, g_rgb, b_rgb


def hex_to_rgb_255(hex_string):
    r_hex = hex_string[1:3]
    g_hex = hex_string[3:5]
    b_hex = hex_string[5:7]
    r_rgb = int(r_hex, 16)
    g_rgb = int(g_hex, 16)
    b_rgb = int(b_hex, 16)
    return r_rgb, g_rgb, b_rgb


def line(point1, point2, color=(1, 1, 1)):
    glBegin(GL_LINES)
    glColor3f(*color)
    glVertex3fv(point1)
    glVertex3fv(point2)
    glEnd()
    return


def quad(points, color):
    glBegin(GL_QUADS)
    glColor3f(*color)
    for p in points:
        glVertex3fv(p)
    glEnd()
    return


def cquad(point, size, color):
    """Color in a square with the same color"""
    glBegin(GL_QUADS)
    if len(color) == 3:
        color = *color, 1.0
    glColor4f(*color)
    x, y, z = point
    s = size / 2.0
    glVertex3fv((x - s, y - s, z))
    glVertex3fv((x + s, y - s, z))
    glVertex3fv((x + s, y + s, z))
    glVertex3fv((x - s, y + s, z))
    glEnd()
    return


def dquad(point, size, color1, color2, stripe_number=4):
    """Color in a square with stripes of two colors"""
    glBegin(GL_QUADS)
    if len(color1) == 3:
        color1 = *color1, 1.0
    if len(color2) == 3:
        color2 = *color2, 1.0

    x, y, z = point
    s = size / 2.0

    left = x - s
    right = x + s
    down = y - s
    up = y + s

    stripe_width = size / stripe_number

    for ii in range(stripe_number):

        if ii % 2:
            color = color1
        else:
            color = color2

        glColor4f(*color)

        temp_left = left + ii * stripe_width
        temp_right = left + (ii + 1) * stripe_width

        glVertex3fv((temp_left, down, z))
        glVertex3fv((temp_right, down, z))
        glVertex3fv((temp_right, up, z))
        glVertex3fv((temp_left, up, z))

    glEnd()
    return


def draw_text(x, y, text, text_color=(255, 255, 255, 255), bg_color=(0, 0, 0, 125)):
    text_surface = font.render(text, True, text_color, bg_color)
    text_data = pygame.image.tostring(text_surface, "RGBA", True)
    glWindowPos2d(x, y)
    glDrawPixels(
        text_surface.get_width(),
        text_surface.get_height(),
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        text_data,
    )
    return


### Command line argument parser function
def parse_args():
    """Parse command line arguments.  All should be optional."""
    prog = "blokbot"
    description = """Polymino playing game"""
    epilog = """
    Examples:
    ---------
    1. Play four player game on a 20x20 board:
    python blokbot.py

    2. Play two player game on a 14x14 board:
    python blokbot.py --duo

    3. Play four player game where RandomBot controls Player 2 and Player 4:
    python blokbot.py -p2 random -p4 random

    4. Play four player game where SnugBot controls Player 2, 
       BigBot controls player 3, and Blockbot controls Player 4:
    python blokbot.py -p2 snug -p3 big -p4 block



    List of eligible bot names:
    1. random: RandomBot
    2. big: BigBot
    3. smol: SmolBot
    4. snug: SnugBot
    5. block: BlockBot
    """
    parser = argparse.ArgumentParser(
        prog=prog,
        description=description,
        epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "-p1",
        "--player1",
        type=str,
        help="str. Name of player 1.",
    )
    parser.add_argument(
        "-p2",
        "--player2",
        type=str,
        help="str. Name of player 2.",
    )
    parser.add_argument(
        "-p3",
        "--player3",
        type=str,
        help="str. Name of player 3.",
    )
    parser.add_argument(
        "-p4",
        "--player4",
        type=str,
        help="str. Name of player 4.",
    )
    parser.add_argument(
        "-d",
        "--duo",
        action="store_true",
        help="flag. If set, plays a two-player game.",
    )
    parser.add_argument(
        "-ns",
        "--nosave",
        action="store_true",
        help="flag. If set, do not save the game history and score at the end of the game.",
    )
    parser.add_argument(
        "-q",
        "--quickquit",
        action="store_true",
        help="flag. If set, quit the game immediately at the end of the game. (not recommended for humans)",
    )

    args = parser.parse_args()

    player1 = args.player1
    player2 = args.player2
    player3 = args.player3
    player4 = args.player4
    duo = args.duo
    nosave = args.nosave
    quickquit = args.quickquit

    if player1:
        player1 = player1.lower()
    if player2:
        player2 = player2.lower()
    if player3:
        player3 = player3.lower()
    if player4:
        player4 = player4.lower()

    print()
    print(f"player1 = {player1}")
    print(f"player2 = {player2}")
    print(f"player3 = {player3}")
    print(f"player4 = {player4}")
    print(f"duo = {duo}")
    print(f"nosave = {nosave}")
    print(f"quickquit = {quickquit}")

    return player1, player2, player3, player4, duo, nosave, quickquit


def main(player1, player2, player3, player4, duo, nosave, quickquit):
    """Main pygame and PyOpenGL initialization and loop"""
    # define global variables
    # global last_key
    global font

    # initialize pygame and setup an opengl display
    window_size = 1250, 800
    window_ratio = window_size[1] / window_size[0]
    pygame.init()
    screen = pygame.display.set_mode(window_size, OPENGL | DOUBLEBUF | RESIZABLE)
    glEnable(GL_DEPTH_TEST)  # use our zbuffer

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # setup the camera
    glMatrixMode(GL_PROJECTION)
    # gluPerspective(45.0,1000/1000,0.1,1000.0)  #setup lens

    # Define help font
    font = pygame.font.Font(None, 26)

    # setup ortho coordinate system
    left = -10
    right = 10
    down = window_ratio * left
    up = window_ratio * right
    near = -1
    far = 1
    glOrtho(left, right, down, up, near, far)
    # glOrtho(-10,110,-10,70,-1,1)

    # glTranslatef(0, 0, -100)          # move back
    # glRotatef(-20, 1, 0, 0)           # orbit higher

    # initialize a board
    # Blokus Duo
    if duo:
        player_number = 2
        board_size = 14
    else:
        # Blokus
        player_number = 4
        board_size = 20

    board = Board(
        player_number,
        player1,
        player2,
        player3,
        player4,
        board_size,
        left,
        right,
        down,
        up,
        window_size,
    )

    # initialize a timer in milliseconds
    nt = int(time.time() * 1000)

    going = True
    while going:
        # add 20 milliseconds to timer
        nt += 1000 // FPS_TARGET

        # if player is a robot, make the move
        if board.is_current_player_robot and not board.game_over:
            board.make_move()

        # check for events
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                going = False

            elif event.type == VIDEORESIZE:
                glLoadIdentity()
                new_window_size = event.w, event.h
                glViewport(0, 0, new_window_size[0], new_window_size[1])

                new_left = left * new_window_size[0] / window_size[0]
                new_right = right * new_window_size[0] / window_size[0]
                new_down = down * new_window_size[1] / window_size[1]
                new_up = up * new_window_size[1] / window_size[1]

                board.update_window_size(
                    new_left, new_right, new_down, new_up, new_window_size
                )

                # glOrtho(1 / left, 1 / right, 1 / down, 1 / up, 1 / near, 1 / far)
                glMatrixMode(GL_PROJECTION)
                glOrtho(new_left, new_right, new_down, new_up, near, far)

            elif event.type == KEYDOWN:
                last_key = event.key

                # if the game is over, only catch some events
                if board.game_over:
                    if last_key == K_SPACE:
                        board.toggle_game_review()
                    continue

                if last_key == K_UP:
                    board.increment_piece_index()

                if last_key == K_DOWN:
                    board.decrement_piece_index()

                if last_key == K_r:
                    board.rotate_piece()

                if last_key == K_e:
                    board.rotate_piece_counterclockwise()

                if last_key == K_f:
                    board.flip_piece()

                if last_key == K_g:
                    board.invert_piece()

                if last_key == K_p:
                    board.skip_turn()

                if last_key == K_h:
                    board.toggle_help()

                if last_key == K_x:
                    board.toggle_edge_help()

                if last_key == K_c:
                    board.toggle_corner_help()

                if last_key == K_o:
                    board.toggle_score()

                if last_key == K_l:
                    board.increment_player_color()

                if last_key == K_b:
                    board.toggle_potential_move_shown()

                if last_key == K_n:
                    board.decrement_potential_move()

                if last_key == K_m:
                    board.increment_potential_move()

                if last_key == K_COMMA:
                    for ii in range(10):
                        board.decrement_potential_move()

                if last_key == K_PERIOD:
                    for ii in range(10):
                        board.increment_potential_move()

            elif event.type == MOUSEWHEEL:
                if event.y > 0:
                    board.increment_piece_index()

                elif event.y < 0:
                    board.decrement_piece_index()

            # if the game is over, don't catch the mouse clicks
            if board.game_over:
                continue

            # get mouse position on window
            mouse_pos = pygame.mouse.get_pos()
            board.get_square_mouse_over(mouse_pos)

            board.highlight_hover()

            if board.x_index is not None and board.y_index is not None:

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    board.make_move()
                    # pygame.event.set_grab(not pygame.event.get_grab())

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
                    # board.increment_color_index()
                    pass
                    # pygame.mouse.set_visible(not pygame.mouse.get_visible())

        if board.game_over:
            if not board.are_winners_determined:
                board.determine_winners()

            if not nosave and not board.is_game_saved:
                board.save_game()

            if board.is_game_reviewing:
                board.review_game()

            if quickquit:
                going = False

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # game action
        board.draw_board()

        # update the full display Surface to the screen
        pygame.display.flip()

        # find how long it took for game action to be calculated and drawn
        ct = int(time.time() * 1000)

        # wait for the difference in expected versus actual time passed, unless it is less than one.
        pygame.time.wait(max(1, nt - ct))

    pygame.display.quit()
    pygame.quit()
    return


if __name__ == "__main__":
    player1, player2, player3, player4, duo, nosave, quickquit = parse_args()
    main(player1, player2, player3, player4, duo, nosave, quickquit)
