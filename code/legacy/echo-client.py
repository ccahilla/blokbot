import json
import pprint
import socket

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 8080
# HOST = "4690-71-83-239-169.ngrok.io"
# PORT = 4040  # The port used by the server


def binary_string_to_dict(binary_string, encoding="utf-8"):
    """Takes in a binary_string encoded with utf-8,
    and converts to a python dictionary
    """
    new_dict = json.loads(binary_string.decode(encoding))
    return new_dict


def dict_to_binary_string(input_dict, encoding="utf-8"):
    """Takes in a binary_string encoded with utf-8,
    and converts to a python dictionary
    """
    binary_string = json.dumps(input_dict).encode(encoding)
    return binary_string


# test_dict = {"my_data": np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]), "oh_yeah": 3}
test_dict = {"my_data": "yes data", "oh_yeah": 3, "heart": "boob"}

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    # s.sendall(b"Hello, world")
    test_json = dict_to_binary_string(test_dict)
    s.sendall(test_json)
    received_data = s.recv(1024)

received_dict = binary_string_to_dict(received_data)
print("received_dict")
pprint.pprint(received_dict)
