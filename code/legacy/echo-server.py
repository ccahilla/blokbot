import json
import socket

HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
PORT = 8080  # Port to listen on (non-privileged ports are > 1023)


def binary_string_to_dict(binary_string, encoding="utf-8"):
    """Takes in a binary_string encoded with utf-8,
    and converts to a python dictionary
    """
    new_dict = json.loads(binary_string.decode(encoding))
    return new_dict


def dict_to_binary_string(input_dict, encoding="utf-8"):
    """Takes in a binary_string encoded with utf-8,
    and converts to a python dictionary
    """
    binary_string = json.dumps(input_dict).encode(encoding)
    return binary_string


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print(f"Connected by {addr[0]}:{addr[1]}")
        test_json = b""
        while True:
            test_json = conn.recv(1024)

            print(f"test_json = {test_json}")
            if not test_json:
                break

            test_dict = binary_string_to_dict(test_json)

            test_dict["addition"] = "test addition"

            test_json = dict_to_binary_string(test_dict)

            conn.sendall(test_json)

    # test_dict = json.loads(test_json.decode("utf-8"))
    # print(f"test_dict:")
    # for key, value in test_dict.items():
    #     print(f"{key}: {value}")
    #     print(f"type(value) = {type(value)}")
