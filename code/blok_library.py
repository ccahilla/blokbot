"""blok_library.py

Library for some basic pygame and OpenGL functions.
Craig Cahillane
Oct 1, 2022
"""

import json

import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *


### client-server functions
# sending the game board as a json binary string
def binary_string_to_dict(binary_string, encoding="utf-8"):
    """Takes in a binary_string encoded with utf-8,
    and converts to a python dictionary
    """
    new_dict = json.loads(binary_string.decode(encoding))
    return new_dict


def dict_to_binary_string(input_dict, encoding="utf-8"):
    """Takes in a binary_string encoded with utf-8,
    and converts to a python dictionary
    """
    binary_string = json.dumps(input_dict).encode(encoding)
    return binary_string


### PyOpenGL functions
def hex_to_rgb(hex_string):
    r_hex = hex_string[1:3]
    g_hex = hex_string[3:5]
    b_hex = hex_string[5:7]
    r_rgb = int(r_hex, 16) / 255.0
    g_rgb = int(g_hex, 16) / 255.0
    b_rgb = int(b_hex, 16) / 255.0
    return r_rgb, g_rgb, b_rgb


def hex_to_rgb_255(hex_string):
    r_hex = hex_string[1:3]
    g_hex = hex_string[3:5]
    b_hex = hex_string[5:7]
    r_rgb = int(r_hex, 16)
    g_rgb = int(g_hex, 16)
    b_rgb = int(b_hex, 16)
    return r_rgb, g_rgb, b_rgb


def line(point1, point2, color=(1, 1, 1)):
    glBegin(GL_LINES)
    glColor3f(*color)
    glVertex3fv(point1)
    glVertex3fv(point2)
    glEnd()
    return


def quad(points, color):
    glBegin(GL_QUADS)
    glColor3f(*color)
    for p in points:
        glVertex3fv(p)
    glEnd()
    return


def cquad(point, size, color):
    """Color in a square with the same color"""
    glBegin(GL_QUADS)
    if len(color) == 3:
        color = *color, 1.0
    glColor4f(*color)
    x, y, z = point
    s = size / 2.0
    glVertex3fv((x - s, y - s, z))
    glVertex3fv((x + s, y - s, z))
    glVertex3fv((x + s, y + s, z))
    glVertex3fv((x - s, y + s, z))
    glEnd()
    return


def dquad(point, size, color1, color2, stripe_number=4):
    """Color in a square with stripes of two colors"""
    glBegin(GL_QUADS)
    if len(color1) == 3:
        color1 = *color1, 1.0
    if len(color2) == 3:
        color2 = *color2, 1.0

    x, y, z = point
    s = size / 2.0

    left = x - s
    right = x + s
    down = y - s
    up = y + s

    stripe_width = size / stripe_number

    for ii in range(stripe_number):
        if ii % 2:
            color = color1
        else:
            color = color2

        glColor4f(*color)

        temp_left = left + ii * stripe_width
        temp_right = left + (ii + 1) * stripe_width

        glVertex3fv((temp_left, down, z))
        glVertex3fv((temp_right, down, z))
        glVertex3fv((temp_right, up, z))
        glVertex3fv((temp_left, up, z))

    glEnd()
    return


def draw_text(
    x, y, text, font, text_color=(255, 255, 255, 255), bg_color=(0, 0, 0, 125)
):
    text_surface = font.render(text, True, text_color, bg_color)
    text_data = pygame.image.tostring(text_surface, "RGBA", True)
    glWindowPos2d(x, y)
    glDrawPixels(
        text_surface.get_width(),
        text_surface.get_height(),
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        text_data,
    )
    return
