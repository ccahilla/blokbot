"""Player.py

Defines a player of the game, human or robot.
Parent to BotPlayer class,
and all other robot player definitions one level below BotPlayer.
"""

import copy

import numpy as np

import blok_library
from Piece import Piece


class Player:
    """
    Defines a player of the game, human or robot.
    Parent to BotPlayer class,
    and all other robot player definitions one level below BotPlayer.
    """

    def __init__(self, order, name, color, color_index, size):
        # basic player variables
        self.order = order
        self.name = name
        self.color = color
        self.color_index = color_index
        self.size = size

        # score variables
        self.squares_left = 89  # want to minimize this
        self.score = 0

        # define which piece player is holding in their hand
        self.piece_index = 9

        # store every type of piece
        self.pieces = []
        self.pieces_left = 21
        for ii in range(self.pieces_left):
            temp_piece = Piece(ii, color)
            self.pieces.append(temp_piece)

        # hold last Piece played
        self.last_played_piece_id = None

        # store whether a piece has been played already
        self.pieces_played = np.zeros(self.pieces_left, dtype=int)

        # number of moves made
        self.moves = 0

        # store whether the player has played all pieces, or has no available moves
        self.finished = False

        # hold the edges and corners for each player
        self.edge_board = np.zeros(shape=(self.size, self.size), dtype=int)
        self.corner_board = np.zeros(shape=(self.size, self.size), dtype=int)

        # are potential moves calculated
        self.are_potential_moves_calculated = True

        # hold the potential moves for each player
        self.potential_move_index = 1
        self.potential_moves = {}

        # set player to not robot by default
        self.is_player_robot = False

        return

    # get functions
    def get_current_piece(self):
        """Gets the current piece object being held by the current player."""
        piece = self.pieces[self.piece_index]
        return piece

    # Piece management
    def increment_piece_index(self):
        """change the piece the current player is holding, by going to the next available index"""
        indicies = np.squeeze(np.argwhere(self.pieces_played == 0))
        if not indicies.size:  # if all pieces are played, so indicies is empty
            return
        elif indicies.size == 1:
            indicies = np.array([indicies], dtype=int)

        # check if indicies are higher than current piece indicies
        high_indicies = np.squeeze(np.argwhere(self.piece_index < indicies))

        # if no indicies are above the current piece index
        if not high_indicies.size:
            self.piece_index = indicies[0]
        elif high_indicies.size == 1:
            self.piece_index = indicies[high_indicies]
        else:
            self.piece_index = indicies[high_indicies[0]]

        return

    def decrement_piece_index(self):
        """change the piece the current player is holding, by choosing the next lowest piece available"""
        indicies = np.squeeze(np.argwhere(self.pieces_played == 0))
        if not indicies.size:  # if all pieces are played, so incidies is empty
            return

        # check if indicies are lower than current piece indicies
        low_indicies = np.squeeze(np.argwhere(self.piece_index > indicies))

        # if no indicies are above the current piece index
        if not low_indicies.size:
            self.piece_index = indicies[-1]
        elif low_indicies.size == 1:
            self.piece_index = indicies[low_indicies]
        else:
            self.piece_index = indicies[low_indicies[-1]]

        return

    def piece_played(self, id):
        """If a piece is played, we want to
        1) decrement pieces_left
        2) set Player.pieces_played[id] == 1
        3) set Piece.played = True
        """
        self.moves += 1
        self.pieces_left -= 1
        self.pieces_played[id] = 1
        current_piece = self.pieces[id]
        current_piece.set_piece_played()

        # store last piece played by the Player
        self.last_played_piece_id = id

        # Adjust score based on what piece was played
        self.score += current_piece.squares
        self.squares_left -= current_piece.squares

        # if a player is finished, set them to finished
        if self.pieces_left == 0:
            # print()
            # print(f"All pieces played for player {self.order}, congratulations!")
            # print()
            self.finished = True

        return

    # Move check functions
    def check_move_legality(self, x_index, y_index, piece, board_data):
        """checks the current proposed move is legal according to the rules,
        i.e. can be placed touching the corner of another same colored piece,
        then sets self.is_move_legal boolean value.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        temp_piece_coordinates = piece.piece_coordinates

        # check the board data to make sure all potential fill spots are empty
        is_move_possible = True
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index
            # ensure we're inside the board boundaries
            if (
                0 <= temp_x
                and temp_x < self.size
                and 0 <= temp_y
                and temp_y < self.size
            ):
                # if we aren't, then ensure the board spaces are empty, i.e. == 0
                if board_data[temp_x, temp_y]:
                    is_move_possible = False
                    break
            else:
                is_move_possible = False
                break

        is_move_legal = False
        if is_move_possible:
            # check if we hit a corner, and that we do not hit an edge
            edge_hit = False
            corner_hit = False

            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + x_index
                temp_y = coordinate[1] + y_index

                edge = self.edge_board[temp_x, temp_y]
                corner = self.corner_board[temp_x, temp_y]

                if edge:
                    edge_hit = True
                    break
                elif corner:
                    corner_hit = True

            if corner_hit and not edge_hit:
                is_move_legal = True

        return is_move_legal

    # Make move function
    def make_move(self, x_index, y_index, piece, board_data):
        """Places a piece of the current player's color.
        Move legality should already have been checked by Board.make_move()
        Also repopulates the Player edge_board and corner_board.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over

        Output:
        -------
        board_data: 2d numpy array of ints
            new board containing the human player's move
        """
        temp_piece_coordinates = piece.piece_coordinates

        # place piece on selected squares
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index
            board_data[temp_x, temp_y] = self.color_index

        # update the edge board and corner board
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index

            # edges
            for xx, yy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                edge_x = temp_x + xx
                edge_y = temp_y + yy

                # square not off edge of board
                if (
                    0 <= edge_x
                    and edge_x < self.size
                    and 0 <= edge_y
                    and edge_y < self.size
                ):
                    if board_data[edge_x, edge_y] == 0:  # if square is empty
                        self.edge_board[edge_x, edge_y] = 1
                        self.corner_board[edge_x, edge_y] = 0

            # corners
            for xx, yy in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                corner_x = temp_x + xx
                corner_y = temp_y + yy

                # square not off edge of board
                if (
                    0 <= corner_x
                    and corner_x < self.size
                    and 0 <= corner_y
                    and corner_y < self.size
                ):
                    # if square is empty
                    if board_data[corner_x, corner_y] == 0:
                        # if square not an edge
                        if self.edge_board[corner_x, corner_y] == 0:
                            self.corner_board[corner_x, corner_y] = 1

        # change pieces_played index to True, so it cannot be played again
        self.piece_played(piece.id)

        # increment the piece being held
        self.increment_piece_index()

        return board_data

    # Potential moves function
    def calculate_potential_moves(self, board_data):
        """Calculates the current players possible moves.
        Each piece has 8 possible orientations, some of which are degenerate.
        There are also mulitple possible displacements for each orientation,
        i.e. placement of the piece might be on a a couple different corners.
        """
        temp_potential_moves = {}
        moves = 0

        # get all corner coordinates and cycle through them
        xs, ys = self.corner_board.nonzero()

        for x_corner, y_corner in zip(xs, ys):

            # cycle through all remaining player pieces
            for piece in self.pieces:
                # skip pieces already placed
                if piece.played:
                    continue

                # copy the piece so as to not ruin the player's settings,
                # and to always have consistent start orientation
                cpiece = Piece(piece.id, piece.color)

                ## cycle through all orientations and displacements.
                # there are eight possible orientations for each piece
                # orientations will be stored by number of rotations and flips needed

                # two flips
                for ff in range(2):

                    # four rotations
                    for rr in range(4):

                        # apply rotations before flips
                        for ii in range(rr):
                            cpiece.rotate_piece()

                        for ii in range(ff):
                            cpiece.flip_piece()

                        # cycle through squares on the piece (5 maximum)
                        for x_piece, y_piece in cpiece.piece_coordinates:

                            # subtract the piece_coordinates to put that square on the corner
                            x_index = x_corner - x_piece
                            y_index = y_corner - y_piece

                            # try to place the piece on the corner
                            is_place_good = self.check_move_legality(
                                x_index, y_index, cpiece, board_data
                            )

                            # if the move is legal for that piece in that orientation
                            if is_place_good:

                                # form the new_board with the new piece placed
                                new_board = copy.deepcopy(board_data)
                                for coordinate in cpiece.piece_coordinates:
                                    temp_x = coordinate[0] + x_index
                                    temp_y = coordinate[1] + y_index
                                    new_board[temp_x, temp_y] = self.color_index

                                # check if the board is new
                                is_board_new = True
                                for move, move_dict in temp_potential_moves.items():
                                    # if piece is already in the dictionary
                                    if move_dict["piece_id"] == cpiece.id:
                                        if np.array_equal(
                                            move_dict["board"], new_board
                                        ):
                                            is_board_new = False
                                            break

                                # if board is new, add it to list of potential moves
                                if is_board_new:
                                    moves += 1
                                    temp_potential_moves[moves] = {}
                                    temp_potential_moves[moves]["board"] = new_board
                                    temp_potential_moves[moves]["x_corner"] = x_corner
                                    temp_potential_moves[moves]["y_corner"] = y_corner
                                    temp_potential_moves[moves]["piece_id"] = cpiece.id
                                    temp_potential_moves[moves]["flips"] = ff
                                    temp_potential_moves[moves]["rotations"] = rr
                                    temp_potential_moves[moves]["x_index"] = x_index
                                    temp_potential_moves[moves]["y_index"] = y_index

                        # undo rotations and flips
                        for ii in range(ff):
                            cpiece.flip_piece()

                        for ii in range(rr):
                            cpiece.rotate_piece_counterclockwise()

        # set player to finished if they have no moves
        total_potential_moves = len(temp_potential_moves)
        if total_potential_moves == 0:
            self.finished = True

        self.potential_moves = temp_potential_moves

        return

    def get_board_spots(self, board_data, value):
        """Gets all (x, y) pairs equal to value in board_data 2d array"""

        # make list of all ordered pairs (x, y) of board spaces belonging to each player
        board_spots = np.array([])
        for xx in range(self.size):
            for yy in range(self.size):
                if board_data[xx, yy] == value:
                    # if board_spots is None
                    if not board_spots.size:
                        board_spots = np.array([[xx, yy]], dtype=int)
                    else:
                        board_spots = np.vstack((board_spots, [xx, yy]))

        return board_spots

    # Draw functions
    def draw_remaining_pieces(self, x_center, y_center, square_size, player_index):
        """Draw the pieces remaining to the player on the side of the board,
        centered at (x_center, y_center) in ortho units.
        class Board will have to provide the center coordinates to the player.
        small_square_size is the size of the squares in the pieces in ortho units.
        """
        # square_size acts as a scale factor
        ss = square_size

        # factor which gets added to the y-coordinate with every additional piece
        y_addition = -5 * ss

        for ii, piece in enumerate(self.pieces):
            y_addition += 5 * ss
            if piece.played:
                continue

            temp_y = y_center + y_addition
            piece.draw_piece(x_center, temp_y, square_size)

            if ii == self.piece_index:
                box_size = 5 * square_size
                self.draw_box_around_selected_piece(
                    x_center, temp_y, box_size, player_index
                )

        return

    def draw_box_around_selected_piece(
        self, x_center, y_center, box_size, player_index
    ):
        """Draws a box around the selected piece for player"""
        x0 = x_center
        y0 = y_center
        bs2 = box_size / 2.0

        if self.order == player_index + 1:
            color = (1, 1, 0.2)
        else:
            color = (1, 1, 1)

        blok_library.line((x0 - bs2, y0 - bs2, 0), (x0 - bs2, y0 + bs2, 0), color)
        blok_library.line((x0 - bs2, y0 + bs2, 0), (x0 + bs2, y0 + bs2, 0), color)
        blok_library.line((x0 + bs2, y0 + bs2, 0), (x0 + bs2, y0 - bs2, 0), color)
        blok_library.line((x0 + bs2, y0 - bs2, 0), (x0 - bs2, y0 - bs2, 0), color)

        return
