"""ClientPlayer.py

Player who plays a game of blokbot remotely.
This player will connect to the HostPlayer's server 
at the start of the game.
They receive moves when other ClientPlayers take a turn,
and send a move when it is their turn.
"""

import pickle
import socket

import blok_library
from Piece import Piece
from Player import Player


class ClientPlayer(Player):
    """Player who plays a game of blokbot remotely.
    This player will connect to the HostPlayer's server
    at the start of the game.
    They receive moves when other ClientPlayers take a turn,
    and send a move when it is their turn.
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.is_player_robot = False

        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server = "192.168.1.10"
        self.port = 5555
        self.addr = (self.server, self.port)

        # connect to the server:port address for the first time
        self.connection = self.connect()

        return

    def get_connection(self):
        return self.connection

    def connect(self):
        try:
            self.client.connect(self.addr)
            return pickle.loads(self.client.recv(2048))
        except:
            pass

        return

    def send(self, data):
        try:
            self.client.send(pickle.dumps(data))
            return pickle.loads(self.client.recv(2048))
        except socket.error as e:
            print(e)

        return
