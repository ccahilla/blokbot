"""SnugBotPlayer.py

Bot who always plays the snuggest possible piece,
i.e. the piece that borders the most opponent squares.
"""

import numpy as np

from BotPlayer import BotPlayer
from Piece import Piece


class SnugBotPlayer(BotPlayer):
    """Bot who always plays the snuggest possible piece,
    i.e. the piece that borders the most opponent squares
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Snug Bot {self.order}"

        return

    def calculate_costs(self, board_data):
        """Calculates the cost of each move for Snug Bot.
        Each opponent square we border subtracts 1,
        and each square played subtracts 0.1.

        Returns a numpy.array of the costs associated with each move.
        """
        # compute the costs for each move
        costs = np.array([])

        # scalers to control the cost calculation
        square_scaler = -0.1
        opponent_edge_scaler = -1.0
        opponent_corner_scaler = -0.5

        for move_key, move_dict in self.potential_moves.items():
            # get the move information
            x_index = move_dict["x_index"]
            y_index = move_dict["y_index"]
            piece_id = move_dict["piece_id"]
            ff = move_dict["flips"]
            rr = move_dict["rotations"]

            # reconstruct the piece orientation
            piece = Piece(piece_id, self.color)

            for ii in range(rr):
                piece.rotate_piece()
            for ii in range(ff):
                piece.flip_piece()

            # calculate the number of potential new edges and corners
            number_opponent_edges = 0
            number_opponent_corners = 0
            number_off_board_edges = 0
            number_off_board_corners = 0
            for x_piece, y_piece in piece.piece_coordinates:
                temp_x = x_piece + x_index
                temp_y = y_piece + y_index

                # edges
                for xx, yy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    edge_x = temp_x + xx
                    edge_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= edge_x
                        and edge_x < self.size
                        and 0 <= edge_y
                        and edge_y < self.size
                    ):
                        # if square is not empty (should not have to check color)
                        if board_data[edge_x, edge_y] != 0:
                            number_opponent_edges += 1
                    else:
                        number_off_board_edges += 1

                # corners
                for xx, yy in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                    corner_x = temp_x + xx
                    corner_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= corner_x
                        and corner_x < self.size
                        and 0 <= corner_y
                        and corner_y < self.size
                    ):
                        # if square is not empty and not the same color as player
                        if (
                            board_data[corner_x, corner_y] != 0
                            and board_data[corner_x, corner_y] != self.color_index
                        ):
                            number_opponent_corners += 1

                    else:
                        number_off_board_corners += 1

            # calculate cost for this move
            temp_cost = (
                square_scaler * piece.squares
                + opponent_edge_scaler * number_opponent_edges
                + opponent_corner_scaler * number_opponent_corners
            )

            # store the information to check later
            move_dict["number_opponent_edges"] = number_opponent_edges
            move_dict["number_opponent_corners"] = number_opponent_corners
            move_dict["number_off_board_edges"] = number_off_board_edges
            move_dict["number_off_board_corners"] = number_off_board_corners

            costs = np.append(costs, temp_cost)

        return costs

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Snug Bot is the number of opponent squares near to a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = self.calculate_costs(board_data)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move from the lowest cost list
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        # print()
        # print(f"{self.name} potential move {move_key}/{total_potential_moves}")
        # print(f"cost = {costs[min_cost_indices[random_move_index]]}")
        # print(f"total_minimum_cost_moves = {total_minimum_cost_moves}")
        # print(
        #     f"number_opponent_edges = {self.potential_moves[move_key]['number_opponent_edges']}"
        # )
        # print(
        #     f"number_opponent_corners = {self.potential_moves[move_key]['number_opponent_corners']}"
        # )
        # print(
        #     f"number_off_board_edges = {self.potential_moves[move_key]['number_off_board_edges']}"
        # )
        # print(
        #     f"number_off_board_corners = {self.potential_moves[move_key]['number_off_board_corners']}"
        # )

        return move_key
