"""HostPlayer.py

Player who hosts a game of blokbot.
This player will open a server on their computer IP 
and wait for connections from the ClientPlayers.
When every ClientPlayer is connected,
this player's server will receive board data from each client
and sent it out to every other client.
"""

import pickle
import socket
import blok_library
from _thread import start_new_thread


from Piece import Piece
from Player import Player


class HostPlayer(HostPlayer):
    """Player who hosts a game of blokbot.
    This player will open a server on their computer IP
    and wait for connections from the ClientPlayers.
    When every ClientPlayer is connected,
    this player's server will receive board data from each client
    and sent it out to every other client.
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.is_player_robot = False

        self.server = "192.168.1.10"
        self.port = 5555

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            self.socket.bind((self.server, self.port))
        except socket.error as ee:
            print(ee)

        self.socket.listen(2)
        print("Waiting for connections, Server Started")

        return

    def threaded_client(conn, player):
        """threaded_client creates a connection to a ClientPlayer,
        so the host and client can communicate the board state between each other.
        """
        conn.send(pickle.dumps(players[player]))
        reply = ""
        while True:
            try:
                data = pickle.loads(conn.recv(2048))
                players[player] = data

                if not data:
                    print("Disconnected")
                    break
                else:
                    if player == 1:
                        reply = players[0]
                    else:
                        reply = players[1]

                    print("Received: ", data)
                    print("Sending : ", reply)

                conn.sendall(pickle.dumps(reply))
            except:
                break

        print("Lost connection")
        conn.close()
