"""BotPlayer.py

Robot players basic functions.
"""

from Piece import Piece
from Player import Player


class BotPlayer(Player):
    """Automatic player of blok game"""

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.is_player_robot = True
        return

    def make_robot_move(self, board_data, players):
        """Choose a random move from the self.potential_moves dictionary
        Inputs:
        board_data: 2d matrix
            current board information about placed pieces and empty squares
        players: list of Players
            each player playing the game in a list.
            corner_board and edge_board used for some robots decisions.
        """
        total_potential_moves = len(self.potential_moves)

        if total_potential_moves == 0:
            # if no moves, skip turn
            self.finished = True
            new_board = board_data
        else:
            # choose a move using general self.choose_move() function
            # choose_move should be different for every bot
            move_key = self.choose_move(board_data, players)

            # get the move information
            x_index = self.potential_moves[move_key]["x_index"]
            y_index = self.potential_moves[move_key]["y_index"]
            piece_id = self.potential_moves[move_key]["piece_id"]
            ff = self.potential_moves[move_key]["flips"]
            rr = self.potential_moves[move_key]["rotations"]

            # reconstruct the piece orientation
            piece = Piece(piece_id, self.color)

            for ii in range(rr):
                piece.rotate_piece()
            for ii in range(ff):
                piece.flip_piece()

            new_board = self.make_move(x_index, y_index, piece, board_data)

            # set the piece_index to the piece_id as well,
            # so other functions can know what was played
            self.piece_index = piece_id

        return new_board
