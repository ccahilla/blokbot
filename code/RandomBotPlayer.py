"""RandomBotPlayer.py

Bot who always plays a random available move
"""

import numpy as np

from BotPlayer import BotPlayer


class RandomBotPlayer(BotPlayer):
    """Bot who always plays a random available move"""

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Random Bot {self.order}"

        return

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary."""
        total_potential_moves = len(self.potential_moves)

        # choose a random move
        move_key = np.random.randint(total_potential_moves) + 1

        return move_key
