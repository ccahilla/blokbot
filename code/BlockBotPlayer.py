"""BlockBotPlayer.py

Bot who tries to take away corners from opponents,
i.e. tries to block you.
"""

import numpy as np

from BotPlayer import BotPlayer
from Piece import Piece


class BlockBotPlayer(BotPlayer):
    """Bot who tries to take away corners from opponents,
    i.e. tries to block you.
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"Block Bot {self.order}"

        self.square_scaler = -0.25  # reward playing big pieces
        self.closeness_scaler = (
            0.25  # slightly punish playing pieces close to your other pieces
        )
        self.opponent_corners_blocked_scaler = -1.0  # reward blocking opponents

        return

    def calculate_costs(self, board_data, players):
        """Block Bot tries to block the other players by playing pieces which
        take away the most amount of corners possible from opponents.

        Returns a numpy.array of the costs associated with each move.
        """
        # compute the costs for each move
        costs = np.array([])

        # make list of all ordered pairs (x, y) of board spaces belonging to each player
        board_spots = self.get_board_spots(board_data, self.order)

        # cycle through every potential move
        for move_key, move_dict in self.potential_moves.items():

            # get the move information
            x_index = move_dict["x_index"]
            y_index = move_dict["y_index"]
            piece_id = move_dict["piece_id"]
            ff = move_dict["flips"]
            rr = move_dict["rotations"]

            # reconstruct the piece orientation
            piece = Piece(piece_id, self.color)

            for ii in range(rr):
                piece.rotate_piece()
            for ii in range(ff):
                piece.flip_piece()

            # distance between squares and number of corners taken away
            distance = 0
            number_opponent_corners_blocked = 0

            # cycle through each player
            for player in players:

                # cycle through each piece coordinate
                for x_piece, y_piece in piece.piece_coordinates:
                    temp_x = x_piece + x_index
                    temp_y = y_piece + y_index

                    # if the player is yourself
                    if player.order == self.order:
                        ### calculate distance between current pieces and the potential move

                        # if self.score == 0, meaning no pieces have yet been played
                        if not self.score:
                            norm_distance = np.inf
                            continue

                        # for each spot belonging to this player, add to the distance
                        for xx, yy in board_spots:
                            temp_dist = np.abs(xx - temp_x) + np.abs(yy - temp_y)
                            distance += temp_dist

                        # normalize the distance by the number of squares
                        norm_distance = distance / self.score

                        # do not count toward the corners blocked total,
                        # move on to the next player
                        continue

                    temp_corner_board = player.corner_board
                    if temp_corner_board[temp_x, temp_y]:
                        number_opponent_corners_blocked += 1

            # calculate cost for this move
            temp_cost = (
                self.square_scaler * piece.squares
                + self.closeness_scaler / norm_distance
                + self.opponent_corners_blocked_scaler * number_opponent_corners_blocked
            )

            # store the information to check later
            move_dict["piece_squares"] = piece.squares
            move_dict["distance"] = distance
            move_dict["norm_distance"] = norm_distance
            move_dict[
                "number_opponent_corners_blocked"
            ] = number_opponent_corners_blocked

            costs = np.append(costs, temp_cost)

        return costs

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for Smol Bot is just the number of squares for a piece.
        Then it chooses a random move from those with the lowest cost.
        """
        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = self.calculate_costs(board_data, players)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move from the lowest cost list
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        # print()
        # print(f"{self.name} potential move {move_key}/{total_potential_moves}")
        # print(f"cost = {costs[min_cost_indices[random_move_index]]}")
        # print(f"cost from piece size = {self.square_scaler * self.potential_moves[move_key]['piece_squares']}")
        # print(f"cost from distance = {self.closeness_scaler / self.potential_moves[move_key]['norm_distance']}")
        # print(f"cost from corners blocked = {self.opponent_corners_blocked_scaler * self.potential_moves[move_key]['number_opponent_corners_blocked']}")

        # print()
        # print(f"piece_squares = {self.potential_moves[move_key]['piece_squares']}")
        # print(f"distance = {self.potential_moves[move_key]['distance']}")
        # print(f"number_opponent_corners_blocked = {self.potential_moves[move_key]['number_opponent_corners_blocked']}" )

        return move_key
