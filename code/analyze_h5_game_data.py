"""analyze_h5_game_data.py

Grabs the relevant game data from the .h5 game_history dictionaries,
and plots it in a convenient way
"""

import os
import time
import glob
import deepdish

import argparse
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams.update(
    {
        "figure.figsize": (12, 9),
        "text.usetex": False,
        # "mathtext.fontset": "cm",
        "lines.linewidth": 3,
        "lines.markersize": 10,
        "font.size": 16,
        "axes.grid": True,
        "grid.alpha": 0.5,
        "legend.loc": "best",
        "savefig.dpi": 80,
        "pdf.compression": 9,
    }
)


def parse_args():
    """Parse command line arguments.  All should be optional."""
    prog = "analyze_h5_game_data"
    description = """Analyze the automatic games played by blokbot"""
    epilog = """
    Examples:
    ---------
    1. Analyze games in the saved_games directory:
    python analyze_h5_game_data.py saved_games/

    """
    parser = argparse.ArgumentParser(
        prog=prog,
        description=description,
        epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "h5_dirs",
        nargs="+",
        help="directories of .h5 files to analyze.",
    )

    args = parser.parse_args()

    h5_dirs = args.h5_dirs

    print()
    print(f"h5_dirs = {h5_dirs}")

    return h5_dirs


def parse_files(file_list):
    """Takes in list of .h5 files containing game info"""

    player1_scores = np.array([])
    player2_scores = np.array([])
    player3_scores = np.array([])
    player4_scores = np.array([])

    data_dict = {}
    for ii, file0 in enumerate(file_list):
        # print(f"file0 = {file0}")
        info_dict = deepdish.io.load(file0, "/info")

        for key, value in info_dict.items():
            if ii == 0 and "player" in key:
                name = value["name"]

                data_dict[name] = {}
                data_dict[name]["wins"] = 0
                data_dict[name]["scores"] = np.array([])

            if "player" in key:
                name = value["name"]
                final_score = value["final_score"]
                data_dict[name]["scores"] = np.append(
                    data_dict[name]["scores"], final_score
                )

            if "winners" in key:
                for name in value:
                    data_dict[name]["wins"] += 1

    return data_dict


def plot_score_histogram(data_dict, fig_dir):
    """Plot a histogram of each type of player"""

    fig, (s1) = plt.subplots(1)
    for name, value in data_dict.items():
        scores = value["scores"]
        min_score = np.min(scores)
        max_score = 89
        bins = np.arange(min_score - 0.5, max_score + 1.5)

        avg_score = np.mean(scores)
        std_score = np.std(scores)

        label = f"{name} - Avg Score = {avg_score:.1f} +- {std_score:.1f}"
        color = next(s1._get_lines.prop_cycler)["color"]
        facecolor = *mpl.colors.to_rgb(color), 0.5
        s1.hist(
            scores,
            bins=bins,
            color=color,
            histtype="step",
            label=label,
        )
        s1.hist(
            scores,
            bins=bins,
            color=color,
            alpha=0.4,
            histtype="stepfilled",
        )

    s1.set_xlabel("Scores")
    s1.set_ylabel("Occurances")

    s1.legend(loc="upper left")

    plt.tight_layout()

    date_time_str = time.strftime("%Y_%m_%d_%H%M%S")
    fig_filename = f"score_histogram_{date_time_str}.pdf"
    full_fig_filename = os.path.join(fig_dir, fig_filename)
    plt.savefig(full_fig_filename)

    print()
    print("Score histogram created at:")
    print(full_fig_filename)

    return


def main(h5_dirs):
    """Analyze the data from the games"""

    # get the game files
    git_dir = os.path.abspath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")
    )
    print(f"git_dir = {git_dir}")

    file_list = []
    for h5_dir in h5_dirs:
        data_dir = os.path.join(git_dir, h5_dir)
        temp_file_list = glob.glob(data_dir + "*")
        file_list = file_list + temp_file_list

    # get the data from the files
    data_dict = parse_files(file_list)

    # print the data
    print()
    print("Name\t\tWins\tAvg Score +- Std Score")
    for name, value in data_dict.items():
        scores = value["scores"]
        avg_score = np.mean(scores)
        std_score = np.std(scores)

        print(f"{name}\t{value['wins']}\t{avg_score:.1f} +- {std_score:.1f}")

    # plot the data
    fig_dir = os.path.join(git_dir, "figures")
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)

    plot_score_histogram(data_dict, fig_dir)

    return


if __name__ == "__main__":
    h5_dirs = parse_args()
    main(h5_dirs)
