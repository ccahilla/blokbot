"""run_many_bloks.py

Run the blokbot.py code many times.
Potentially use multiprocessing.

Sukolb
"""

import argparse
import os

from main import game_loop


### Command line argument parser function
def parse_args():
    """Parse command line arguments.  All should be optional."""
    prog = "run_many_bloks"
    description = """Polymino playing game"""
    epilog = """
    Examples:
    ---------
    1. Play 100 automatic four player game on a 20x20 board:
    python run_many_bloks.py 100 -p1 random -p2 block -p3 big -p4 snug

    List of eligible bot names:
    1. random: RandomBot
    2. big: BigBot
    3. smol: SmolBot
    4. snug: SnugBot
    5. block: BlockBot
    """
    parser = argparse.ArgumentParser(
        prog=prog,
        description=description,
        epilog=epilog,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "number_of_games",
        type=int,
        help="int. Number of games to play.",
    )
    parser.add_argument(
        "-p1",
        "--player1",
        type=str,
        help="str. Name of player 1.",
    )
    parser.add_argument(
        "-p2",
        "--player2",
        type=str,
        help="str. Name of player 2.",
    )
    parser.add_argument(
        "-p3",
        "--player3",
        type=str,
        help="str. Name of player 3.",
    )
    parser.add_argument(
        "-p4",
        "--player4",
        type=str,
        help="str. Name of player 4.",
    )
    parser.add_argument(
        "-d",
        "--duo",
        action="store_true",
        help="flag. If set, plays a two-player game.",
    )
    parser.add_argument(
        "-ns",
        "--nosave",
        action="store_true",
        help="flag. If set, do not save the game history and score at the end of the game.",
    )

    args = parser.parse_args()

    number_of_games = args.number_of_games
    player1 = args.player1
    player2 = args.player2
    player3 = args.player3
    player4 = args.player4
    duo = args.duo
    nosave = args.nosave

    if player1:
        player1 = player1.lower()
    if player2:
        player2 = player2.lower()
    if player3:
        player3 = player3.lower()
    if player4:
        player4 = player4.lower()

    print()
    print(f"number_of_games = {number_of_games}")
    print(f"player1 = {player1}")
    print(f"player2 = {player2}")
    print(f"player3 = {player3}")
    print(f"player4 = {player4}")
    print(f"duo = {duo}")
    print(f"nosave = {nosave}")

    return number_of_games, player1, player2, player3, player4, duo, nosave


def run_many_bloks(number_of_games, player1, player2, player3, player4, duo, nosave):
    """Run the blokbot game many times over."""

    quickquit = True

    for ii in range(number_of_games):
        print(f"Playing game {ii + 1}/{number_of_games}")
        game_loop(player1, player2, player3, player4, duo, nosave, quickquit)

    return


if __name__ == "__main__":
    number_of_games, player1, player2, player3, player4, duo, nosave = parse_args()
    run_many_bloks(number_of_games, player1, player2, player3, player4, duo, nosave)
