"""MaxMoveBotPlayer.py

Bot who tries to maximize moves it can make.
This bot will look at every possible move available, 
calculate the moves it could make on the next turn,
and make the move that maximizes it's future moves.
"""
import copy
import time

import numpy as np

from BotPlayer import BotPlayer
from Piece import Piece


class MaxMoveBotPlayer(BotPlayer):
    """Bot who tries to maximize moves it can make.
    This bot will look at every possible move available, 
    calculate the moves it could make on the next turn,
    and make the move that maximizes it's future moves.
    """

    def __init__(self, order, name, color, color_index, size):
        # inherit all functions and variables from Player class
        super().__init__(order, name, color, color_index, size)

        self.name = f"MaxMove Bot {self.order}"

        self.square_scaler = -0.1  # reward playing big pieces
        self.max_move_scaler = -1.0  # reward high numbers of moves

        return

    def calculate_costs(self, board_data, players):
        """MaxMove Bot tries to maximize moves it can make.

        Returns a numpy.array of the costs associated with each move.
        """
        debug = False
        # compute the costs for each move
        costs = np.array([])

        # cycle through every potential move
        for move_key, move_dict in self.potential_moves.items():
            if debug:
                if move_key > 1:
                    continue

            # get the new move board
            new_board_data = move_dict["board"]

            if debug:
                print("new_board_data")
                print(f"{new_board_data}")

            # get the piece, fake its move
            piece_id = move_dict["piece_id"]
            new_piece = Piece(piece_id, self.color)
            new_piece.played = True

            # set up new_pieces list
            current_pieces = copy.deepcopy(self.pieces)
            del current_pieces[piece_id]
            new_pieces = current_pieces + [new_piece]

            if debug:
                print("new_pieces")
                for piece in new_pieces:
                    print(f"piece.id = {piece.id}, piece.played = {piece.played}")

            # make list of all ordered pairs (x, y) of board spaces belonging to this player
            new_board_spots = self.get_board_spots(new_board_data, self.order)

            # hold the edges and corners for this player
            new_edge_board = np.zeros(shape=(self.size, self.size), dtype=int)
            new_corner_board = np.zeros(shape=(self.size, self.size), dtype=int)

            # for each spot belonging to this player, make new edge and corner board
            for xx, yy in new_board_spots:

                # edges
                for temp_x, temp_y in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                    edge_x = temp_x + xx
                    edge_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= edge_x
                        and edge_x < self.size
                        and 0 <= edge_y
                        and edge_y < self.size
                    ):
                        if new_board_data[edge_x, edge_y] == 0:  # if square is empty
                            new_edge_board[edge_x, edge_y] = 1
                            new_corner_board[edge_x, edge_y] = 0

                # corners
                for temp_x, temp_y in [(-1, -1), (-1, 1), (1, -1), (1, 1)]:
                    corner_x = temp_x + xx
                    corner_y = temp_y + yy

                    # square not off edge of board
                    if (
                        0 <= corner_x
                        and corner_x < self.size
                        and 0 <= corner_y
                        and corner_y < self.size
                    ):
                        # if square is empty
                        if new_board_data[corner_x, corner_y] == 0:
                            # if square not an edge
                            if new_edge_board[corner_x, corner_y] == 0:
                                new_corner_board[corner_x, corner_y] = 1

            if debug:
                print("new_corner_board")
                print(f"{new_corner_board}")            

            future_moves_number = self.calculate_future_potential_moves(new_board_data, new_edge_board, new_corner_board, new_pieces)

            if debug:
                print("future_moves_number")
                print(future_moves_number)

            # calculate cost for this move
            temp_cost = self.square_scaler * new_piece.squares + self.max_move_scaler * future_moves_number

            # store the information to check later
            move_dict["piece_squares"] = new_piece.squares
            move_dict["future_moves_number"] = future_moves_number

            costs = np.append(costs, temp_cost)

        return costs


    def calculate_future_potential_moves(self, board_data, edge_board, corner_board, pieces):
        """Calculates the possible moves on the current possible moves for MaxMove Bot's cost function.

        Each piece has 8 possible orientations, some of which are degenerate.
        There are also mulitple possible displacements for each orientation,
        i.e. placement of the piece might be on a a couple different corners.
        """
        temp_potential_moves = {}
        moves = 0

        # get all corner coordinates and cycle through them
        xs, ys = corner_board.nonzero()

        for x_corner, y_corner in zip(xs, ys):

            # cycle through all remaining player pieces
            for piece in pieces:
                # skip pieces already placed
                if piece.played:
                    continue

                # copy the piece so as to not ruin the player's settings,
                # and to always have consistent start orientation
                cpiece = Piece(piece.id, piece.color)

                ## cycle through all orientations and displacements.
                # there are eight possible orientations for each piece
                # orientations will be stored by number of rotations and flips needed

                # two flips
                for ff in range(2):

                    # four rotations
                    for rr in range(4):

                        # apply rotations before flips
                        for ii in range(rr):
                            cpiece.rotate_piece()

                        for ii in range(ff):
                            cpiece.flip_piece()

                        # cycle through squares on the piece (5 maximum)
                        for x_piece, y_piece in cpiece.piece_coordinates:

                            # subtract the piece_coordinates to put that square on the corner
                            x_index = x_corner - x_piece
                            y_index = y_corner - y_piece

                            # try to place the piece on the corner
                            is_place_good = self.check_future_move_legality(
                                x_index, y_index, cpiece, board_data, edge_board, corner_board
                            )

                            # if the move is legal for that piece in that orientation
                            if is_place_good:

                                # form the new_board with the new piece placed
                                new_board = copy.deepcopy(board_data)
                                for coordinate in cpiece.piece_coordinates:
                                    temp_x = coordinate[0] + x_index
                                    temp_y = coordinate[1] + y_index
                                    new_board[temp_x, temp_y] = self.color_index

                                # check if the board is new
                                is_board_new = True
                                for move, move_dict in temp_potential_moves.items():
                                    # if piece is already in the dictionary
                                    if move_dict["piece_id"] == cpiece.id:
                                        if np.array_equal(
                                            move_dict["board"], new_board
                                        ):
                                            is_board_new = False
                                            break

                                # if board is new, add it to list of potential moves
                                if is_board_new:
                                    moves += 1
                                    temp_potential_moves[moves] = {}
                                    temp_potential_moves[moves]["board"] = new_board
                                    temp_potential_moves[moves]["x_corner"] = x_corner
                                    temp_potential_moves[moves]["y_corner"] = y_corner
                                    temp_potential_moves[moves]["piece_id"] = cpiece.id
                                    temp_potential_moves[moves]["flips"] = ff
                                    temp_potential_moves[moves]["rotations"] = rr
                                    temp_potential_moves[moves]["x_index"] = x_index
                                    temp_potential_moves[moves]["y_index"] = y_index

                        # undo rotations and flips
                        for ii in range(ff):
                            cpiece.flip_piece()

                        for ii in range(rr):
                            cpiece.rotate_piece_counterclockwise()

        return moves


    # Move check functions
    def check_future_move_legality(self, x_index, y_index, piece, board_data, edge_board, corner_board):
        """checks the current proposed move is legal according to the rules,
        i.e. can be placed touching the corner of another same colored piece,
        then sets self.is_move_legal boolean value.

        Inputs:
        -------
        x_index: int
            x-coordinate of the square mouse is over
        y_index: int
            y-coordinate of the square mouse is over
        """
        temp_piece_coordinates = piece.piece_coordinates

        # check the board data to make sure all potential fill spots are empty
        is_move_possible = True
        for coordinate in temp_piece_coordinates:
            temp_x = coordinate[0] + x_index
            temp_y = coordinate[1] + y_index
            # ensure we're inside the board boundaries
            if (
                0 <= temp_x
                and temp_x < self.size
                and 0 <= temp_y
                and temp_y < self.size
            ):
                # if we aren't, then ensure the board spaces are empty, i.e. == 0
                if board_data[temp_x, temp_y]:
                    is_move_possible = False
                    break
            else:
                is_move_possible = False
                break

        is_move_legal = False
        if is_move_possible:
            # check if we hit a corner, and that we do not hit an edge
            edge_hit = False
            corner_hit = False

            for coordinate in temp_piece_coordinates:
                temp_x = coordinate[0] + x_index
                temp_y = coordinate[1] + y_index

                edge = edge_board[temp_x, temp_y]
                corner = corner_board[temp_x, temp_y]

                if edge:
                    edge_hit = True
                    break
                elif corner:
                    corner_hit = True

            if corner_hit and not edge_hit:
                is_move_legal = True

        return is_move_legal

    def choose_move(self, board_data, players):
        """Chooses a move from the self.potential_moves dictionary.
        First calculates the cost of each move,
        which for MaxMove Bot is the number of future moves a current move will allow.
        Then it chooses a random move from those with the lowest cost.
        """
        start_time = time.time()

        total_potential_moves = len(self.potential_moves)

        # compute the costs for each move
        costs = self.calculate_costs(board_data, players)

        # find the lowest cost moves ( remember to add one )
        min_cost_indices = np.flatnonzero(costs == costs.min())

        # choose a random move from the lowest cost list
        total_minimum_cost_moves = len(min_cost_indices)
        random_move_index = np.random.randint(total_minimum_cost_moves)
        move_key = min_cost_indices[random_move_index] + 1

        print()
        print(f"{self.name} potential move {move_key}/{total_potential_moves}")
        print(f"cost = {costs[min_cost_indices[random_move_index]]}")
        print(f"cost from piece size = {self.square_scaler * self.potential_moves[move_key]['piece_squares']}")
        print(f"cost from potential moves = {self.max_move_scaler * self.potential_moves[move_key]['future_moves_number']}")

        print()
        print(f"piece_squares = {self.potential_moves[move_key]['piece_squares']}")
        print(f"potential moves = {self.potential_moves[move_key]['future_moves_number']}")

        print("\033[92m")
        print(f"Move done in {time.time() - start_time} seconds")
        print("\033[0m")
        return move_key
