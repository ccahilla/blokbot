"""Piece.py

Home of the Piece class definition.
"""
import sys

import numpy as np

import blok_library


class Piece:
    """Class which defines each piece.
    Here we'll define one square to be the "center square" of the piece,
    then program the squares around it based on the id number
    The piece_coordinates define the relative coordinates about the center square.
    We should be able to flip and rotate each piece about the center square,
    which will change the piece_coordinates.

    There are a total of 21 pieces:
    12 pentanimoes (5 squares) [ids: 9-20]
    id: 9   10  11  12  13   14   15  16   17  18   19   20
        □□  □□  □□  □□  □□   □□□   □  □□□  □□□ □□□□ □□□□ □□□□□\n
        □   □□   □   □□  □□  □    □□□  □  □□    □   □\n
        □□  □    □□   □  □   □     □   □\n

    5 tetranimoes (4 squares) [ids: 4-8]
    id:  4   5  6   7   8
        □□□  □□ □□ □□□ □□□□\n
        □   □□  □□  □\n

    2 trinimoes (3 squares) [ids: 2-3]
    id:  2  3
        □□ □□□\n
        □\n

    1 duanimoes (2 squares) [id: 1]
    id:  1
         □□\n

    1 simple square [id: 0]
    id: 0
        □
    """

    def __init__(self, id, color):
        """
        Inputs:
        -------
        id: int
            number 0-20, defines the blokus piece
        """
        self.id = id
        self.color = color

        self.played = False  # has piece been played

        if id == 0:
            self.squares = 1
        elif id == 1:
            self.squares = 2
        elif id == 2 or id == 3:
            self.squares = 3
        elif 4 <= id and id <= 8:
            self.squares = 4
        elif 9 <= id and id <= 20:
            self.squares = 5
        else:
            print
            print("Not a valid piece id")
            print(f"id = {id} was given, id must be an int between [0,20]")
            sys.exit(1)

        # center square
        self.piece_name = "small square piece"
        self.piece_coordinates = np.array([[0, 0]], dtype=int)

        # 2 squares
        if id == 1:
            self.piece_name = "two square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))

        # 3 squares
        if id == 2:
            self.piece_name = "three corner piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
        if id == 3:
            self.piece_name = "three line piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))

        # 4 squares
        if id == 4:  # l-piece
            self.piece_name = "l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 5:  # z-piece
            self.piece_name = "z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 6:  # block piece
            self.piece_name = "block piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
        if id == 7:  # t-piece
            self.piece_name = "t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
        if id == 8:  # 4 line piece
            self.piece_name = "four line piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))

        # 5 squares
        if id == 9:  # big C piece
            self.piece_name = "big c piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 10:  # block plus square piece
            self.piece_name = "block plus square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
        if id == 11:  # block plus square piece
            self.piece_name = "big z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 12:  # big w piece
            self.piece_name = "big w piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, -1]))
        if id == 13:  # l plus square piece
            self.piece_name = "l plus square piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
        if id == 14:  # big l piece
            self.piece_name = "big l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 2]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-2, 0]))
        if id == 15:  # plus piece
            self.piece_name = "plus piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, 1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
        if id == 16:  # big l piece
            self.piece_name = "big t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -2]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
        if id == 17:  # long z piece
            self.piece_name = "long z piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 18:  # long t piece
            self.piece_name = "long t piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [0, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 19:  # long l piece
            self.piece_name = "long l piece"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, -1]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))
        if id == 20:  # long l piece
            self.piece_name = "long boi"
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-2, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [-1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [1, 0]))
            self.piece_coordinates = np.vstack((self.piece_coordinates, [2, 0]))

        return

    def set_piece_played(self):
        """If piece has been played, set self.played = True"""
        self.played = True
        return

    def set_piece_not_played(self):
        """If piece has not been played, set self.played = False.
        Useful mostly for the when the game is being reviewed
        """
        self.played = False
        return

    def rotate_piece(self):
        """Rotates the piece +90 degrees"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = old_y
            new_y = -old_x
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def rotate_piece_counterclockwise(self):
        """Rotates the piece -90 degrees"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = -old_y
            new_y = old_x
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def flip_piece(self):
        """Flip the piece so it is a mirror image of itself along x-axis"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = -old_x
            new_y = old_y
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def invert_piece(self):
        """Flip the piece so it is a mirror image of itself along y-axis"""
        for ii, coordinate in enumerate(self.piece_coordinates):
            old_x, old_y = coordinate
            new_x = old_x
            new_y = -old_y
            self.piece_coordinates[ii] = [new_x, new_y]
        return

    def draw_piece(self, x_center, y_center, square_size):
        """Draw the piece in it's current orientation,
        centered at (x_center, y_center) in ortho units,
        with square_size in ortho units
        """
        # set player color
        rgb_color = blok_library.hex_to_rgb(self.color)
        alpha = 1.0
        rgba_color = *rgb_color, alpha

        # square_size acts as a scale factor
        ss = square_size

        # cycle through piece coordinates, drawing each square
        for coordinate in self.piece_coordinates:
            temp_x = x_center + ss * coordinate[0]
            temp_y = y_center + ss * coordinate[1]

            point = temp_x, temp_y, 0.0

            blok_library.cquad(point, ss, rgba_color)

        return
